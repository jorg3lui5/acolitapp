package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Trabajo;

public interface TrabajoControlador extends JpaRepository<Trabajo, Long>{	
		
	@Query("SELECT p "
			+ " FROM Trabajo p "
			+ " WHERE p.id = :id ")
	public Trabajo recuperarPorId(@Param("id") Long id);
		
}
