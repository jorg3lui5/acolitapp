package com.acolitapp.acolitappserve.enumerador;

public enum EstadoRespuestaEnum {
	
	OK("OK", "ESTADO SATISFACTORIO"),
	ERROR("ERROR", "ERROR EN EJECUCION");

	private String code;
	private String descripcion;

	private EstadoRespuestaEnum(String code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public String getCode() {
		return this.code;
	}
}