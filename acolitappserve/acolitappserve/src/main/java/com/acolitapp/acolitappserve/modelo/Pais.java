package com.acolitapp.acolitappserve.modelo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "pais")
public class Pais {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    @Size(max = 30)
    @Column(name = "nombre", length = 30)
    private String nombre;
//    @OneToMany(mappedBy = "pais", fetch = FetchType.LAZY)
//    private List<Nacionalidad> nacionalidadList;
//    @OneToMany(mappedBy = "pais", fetch = FetchType.LAZY)
//    private List<Ciudad> ciudadList;

    public Pais() {
    }

    public Pais(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

//    @XmlTransient
//    public List<Nacionalidad> getNacionalidadList() {
//        return nacionalidadList;
//    }
//
//    public void setNacionalidadList(List<Nacionalidad> nacionalidadList) {
//        this.nacionalidadList = nacionalidadList;
//    }
//
//    @XmlTransient
//    public List<Ciudad> getCiudadList() {
//        return ciudadList;
//    }
//
//    public void setCiudadList(List<Ciudad> ciudadList) {
//        this.ciudadList = ciudadList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pais)) {
            return false;
        }
        Pais other = (Pais) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aaa.Pais[ id=" + id + " ]";
    }
    

}