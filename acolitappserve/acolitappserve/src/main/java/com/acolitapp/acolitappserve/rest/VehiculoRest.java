package com.acolitapp.acolitappserve.rest;
import org.springframework.web.bind.annotation.RestController;

import com.acolitapp.acolitappserve.controlador.VehiculoControlador;
import com.acolitapp.acolitappserve.controlador.VehiculoControlador;
import com.acolitapp.acolitappserve.dto.ManejoString;
import com.acolitapp.acolitappserve.dto.RespuestaDTO;
import com.acolitapp.acolitappserve.enumerador.EstadoErrorEnum;
import com.acolitapp.acolitappserve.enumerador.EstadoRespuestaEnum;
import com.acolitapp.acolitappserve.modelo.Vehiculo;
import com.acolitapp.acolitappserve.modelo.Vehiculo;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/vehiculo")
@CrossOrigin(origins = "*")
public class VehiculoRest {
	
  public Logger logger = LoggerFactory.getLogger(VehiculoRest.class);

  @Autowired
  private VehiculoControlador vehiculoControlador;

  @RequestMapping(value = "/vehiculos", method = RequestMethod.GET)
  @ResponseBody
  public List<Vehiculo> listar() {
    return vehiculoControlador.findAll();
  }

  @GetMapping("/recuperarPorId/{id}")
  public RespuestaDTO recuperarPorId(@PathVariable(value = "id") Long id) {
	try {
		Vehiculo vehiculo = vehiculoControlador.recuperarPorId(id);
	
		return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), vehiculo);
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }

  @PostMapping("/crear")
  public Vehiculo crear(@RequestBody Vehiculo vehiculo) {
    return vehiculoControlador.save(vehiculo);
  }

  @PutMapping("/actualizar/{id}")
  public RespuestaDTO actualizar(@PathVariable(value = "id") Long id, @Valid @RequestBody Vehiculo objEntidad){
	try {
		if (objEntidad != null) {
			vehiculoControlador.save(objEntidad);
			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objEntidad);
		} else {
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "NO TIENE OBJETO A MODIFICAR.");
		}
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }

  @DeleteMapping("/eliminar/{id}")
  public RespuestaDTO eliminar(@PathVariable(value = "id") Long id) throws Exception {
	try {
		Vehiculo vehiculo = vehiculoControlador.recuperarPorId(id);
		vehiculoControlador.delete(vehiculo);
	
		return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), vehiculo);
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
}