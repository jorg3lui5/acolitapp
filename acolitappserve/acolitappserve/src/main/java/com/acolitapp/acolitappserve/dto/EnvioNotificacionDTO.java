package com.acolitapp.acolitappserve.dto;

import java.util.HashMap;
import java.util.List;

public class EnvioNotificacionDTO {

	private String titulo;
	private String mensaje;
	private List<DatoNotificacionDTO> datos;
	private String tema;
	private List<String> tokensDispositivos;
	private String prioridad;
	
	public EnvioNotificacionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EnvioNotificacionDTO(String titulo, String mensaje, List<DatoNotificacionDTO> datos, String tema,
			List<String> tokensDispositivos, String prioridad) {
		super();
		this.titulo = titulo;
		this.mensaje = mensaje;
		this.datos = datos;
		this.tema = tema;
		this.tokensDispositivos = tokensDispositivos;
		this.prioridad = prioridad;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public List<DatoNotificacionDTO> getDatos() {
		return datos;
	}

	public void setDatos(List<DatoNotificacionDTO> datos) {
		this.datos = datos;
	}

	public String getTema() {
		return tema;
	}

	public void setTema(String tema) {
		this.tema = tema;
	}

	public List<String> getTokensDispositivos() {
		return tokensDispositivos;
	}

	public void setTokensDispositivos(List<String> tokensDispositivos) {
		this.tokensDispositivos = tokensDispositivos;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}
	
	
	
	

}
