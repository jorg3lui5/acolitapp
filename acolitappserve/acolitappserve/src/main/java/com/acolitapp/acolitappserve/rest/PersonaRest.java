package com.acolitapp.acolitappserve.rest;
import org.springframework.web.bind.annotation.RestController;

import com.acolitapp.acolitappserve.controlador.DireccionControlador;
import com.acolitapp.acolitappserve.controlador.PersonaControlador;
import com.acolitapp.acolitappserve.controlador.VehiculoControlador;
import com.acolitapp.acolitappserve.controlador.VehiculoPersonaControlador;
import com.acolitapp.acolitappserve.controlador.PersonaControlador;
import com.acolitapp.acolitappserve.dto.ManejoString;
import com.acolitapp.acolitappserve.dto.RespuestaDTO;
import com.acolitapp.acolitappserve.enumerador.EstadoErrorEnum;
import com.acolitapp.acolitappserve.enumerador.EstadoRespuestaEnum;
import com.acolitapp.acolitappserve.modelo.Persona;
import com.acolitapp.acolitappserve.modelo.Persona;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/persona")
@CrossOrigin(origins = "*")
public class PersonaRest {
	
  public Logger logger = LoggerFactory.getLogger(PersonaRest.class);

  @Autowired
  private PersonaControlador controladorPrincipal;
  @Autowired
  private DireccionControlador direccionControlador;
  @Autowired
  private VehiculoPersonaControlador vehiculoPersonaControlador;
  @Autowired
  private VehiculoControlador vehiculoControlador;

  @GetMapping("/recuperarPorId/{id}")
  public RespuestaDTO recuperarPorId(@PathVariable(value = "id") Long id) {
	try {
		Persona objeto = controladorPrincipal.recuperarPorId(id);
		if(objeto!=null) {
			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objeto.limpiarRecursividad());
		}
		else{
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "EL DATO NO EXISTE");
		}
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
  
  @GetMapping("/recuperarPorUsuario/{nombreUsuario}")
  public RespuestaDTO recuperarPorUsuario(@PathVariable(value = "nombreUsuario") String nombreUsuario) {
	try {
		Persona objeto = controladorPrincipal.recuperarPorUsuario(nombreUsuario);
		if(objeto!=null) {
			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objeto.limpiarRecursividad());
		}
		else{
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "EL DATO NO EXISTE");
		}
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
  
  @GetMapping("/listarTodos")
  public RespuestaDTO listarTodos() {
	try {
		List<Persona> objetos = controladorPrincipal.findAll();
		if(objetos!=null && objetos.size()>0) {
			for(Persona persona: objetos) {
				persona.limpiarRecursividad();
			}
		}
		return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objetos);
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }

  @PostMapping("/crear")
  public RespuestaDTO crear(@RequestBody Persona objeto) {
		try {
			if (objeto != null) {
				controladorPrincipal.save(objeto);
			  return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objeto.limpiarRecursividad());
			} else {
				return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "NO TIENE OBJETO A CREAR.");
			}
		} catch (Exception e) {
			String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
			if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
				logger.error(errorMensaje, e);
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
		}
  }

  @PostMapping("/actualizar")
  public RespuestaDTO actualizar(@RequestBody Persona objEntidad){
	try {
		if (objEntidad != null && objEntidad.getId()!=null) {
			if(objEntidad.getDireccionList()!=null && !objEntidad.getDireccionList().isEmpty()) {
				objEntidad.getDireccionList().get(0).setPersona(objEntidad);
				direccionControlador.save(objEntidad.getDireccionList().get(0));
			}
			if(objEntidad.getVehiculoPersonaList()!=null && !objEntidad.getVehiculoPersonaList().isEmpty()) {
				objEntidad.getVehiculoPersonaList().get(0).setPersona(objEntidad);
				vehiculoControlador.save(objEntidad.getVehiculoPersonaList().get(0).getVehiculo());
				vehiculoPersonaControlador.save(objEntidad.getVehiculoPersonaList().get(0));
			}
			controladorPrincipal.save(objEntidad);
			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objEntidad.limpiarRecursividad());
		} else {
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "NO TIENE OBJETO A MODIFICAR.");
		}
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }

  @GetMapping("/eliminar/{id}")
  public RespuestaDTO eliminar(@PathVariable(value = "id") Long id) throws Exception {
	try {
		Persona objeto = controladorPrincipal.recuperarPorId(id);
		controladorPrincipal.delete(objeto);
	
		return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), "ELIMINADO CORRECTAMENTE");
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
}