/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acolitapp.acolitappserve.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "favor")

public class Favor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    @Size(max = 20)
    @Column(name = "titulo", length = 20)
    private String titulo;
    @Size(max = 150)
    @Column(name = "descripcion", length = 150)
    private String descripcion;
    @Size(max = 20)
    @Column(name = "estado", length = 20)
    private String estado;
    @Column(name = "fecha_solicita")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSolicita;
    @Column(name = "fecha_realiza")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRealiza;
    @OneToMany(mappedBy = "favor")
    @LazyCollection (LazyCollectionOption.FALSE)
    private List<Calificacion> calificacionList;
    @OneToMany(mappedBy = "favor")
    @LazyCollection (LazyCollectionOption.FALSE)
    private List<FiltroFavor> filtroFavorList;
    @OneToMany(mappedBy = "favor")
    @LazyCollection (LazyCollectionOption.FALSE)
    private List<UsuarioAyudaFavor> usuarioAyudaFavorList;
    @JoinColumn(name = "detalle_tipo_pago", referencedColumnName = "id")
    @ManyToOne()
    @LazyCollection (LazyCollectionOption.FALSE)
    private DetalleTipoPago detalleTipoPago;
    @JoinColumn(name = "direccion_favor", referencedColumnName = "id")
    @ManyToOne()
    @LazyCollection (LazyCollectionOption.FALSE)
    private Direccion direccionFavor;
    @JoinColumn(name = "usuario_realiza", referencedColumnName = "id")
    @ManyToOne()
    @LazyCollection (LazyCollectionOption.FALSE)
    private Usuario usuarioRealiza;
    @JoinColumn(name = "usuario_solicita", referencedColumnName = "id")
    @ManyToOne()
    @LazyCollection (LazyCollectionOption.FALSE)
    private Usuario usuarioSolicita;

    public Favor() {
    }

    public Favor(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaSolicita() {
        return fechaSolicita;
    }

    public void setFechaSolicita(Date fechaSolicita) {
        this.fechaSolicita = fechaSolicita;
    }

    public Date getFechaRealiza() {
        return fechaRealiza;
    }

    public void setFechaRealiza(Date fechaRealiza) {
        this.fechaRealiza = fechaRealiza;
    }

    @XmlTransient
    public List<Calificacion> getCalificacionList() {
        return calificacionList;
    }

    public void setCalificacionList(List<Calificacion> calificacionList) {
        this.calificacionList = calificacionList;
    }

    @XmlTransient
    public List<FiltroFavor> getFiltroFavorList() {
        return filtroFavorList;
    }

    public void setFiltroFavorList(List<FiltroFavor> filtroFavorList) {
        this.filtroFavorList = filtroFavorList;
    }

    @XmlTransient
    public List<UsuarioAyudaFavor> getUsuarioAyudaFavorList() {
        return usuarioAyudaFavorList;
    }

    public void setUsuarioAyudaFavorList(List<UsuarioAyudaFavor> usuarioAyudaFavorList) {
        this.usuarioAyudaFavorList = usuarioAyudaFavorList;
    }

    public DetalleTipoPago getDetalleTipoPago() {
        return detalleTipoPago;
    }

    public void setDetalleTipoPago(DetalleTipoPago detalleTipoPago) {
        this.detalleTipoPago = detalleTipoPago;
    }

    public Direccion getDireccionFavor() {
        return direccionFavor;
    }

    public void setDireccionFavor(Direccion direccionFavor) {
        this.direccionFavor = direccionFavor;
    }

    public Usuario getUsuarioRealiza() {
        return usuarioRealiza;
    }

    public void setUsuarioRealiza(Usuario usuarioRealiza) {
        this.usuarioRealiza = usuarioRealiza;
    }

    public Usuario getUsuarioSolicita() {
        return usuarioSolicita;
    }

    public void setUsuarioSolicita(Usuario usuarioSolicita) {
        this.usuarioSolicita = usuarioSolicita;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Favor)) {
            return false;
        }
        Favor other = (Favor) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aaa.Favor[ id=" + id + " ]";
    }

    public Favor limpiarRecursividad() {
    	if (this.getCalificacionList()!=null && !this.getCalificacionList().isEmpty()) {
    		for(Calificacion calificacion: this.getCalificacionList()) {
    			if(calificacion.getUsuario()!=null) {
    				calificacion.getUsuario().setPersona(null);
    			}
    		}
    	}
    	if(this.getDetalleTipoPago()!=null) {
    		this.getDetalleTipoPago().setFavorList(null);
    	}
    	if(this.getDireccionFavor()!=null) {
    		this.getDireccionFavor().setFavorList(null);
    		this.getDireccionFavor().setPersona(null);
    		this.getDireccionFavor().setTrabajoList(null);
    		if(this.getDireccionFavor().getUbicacion()!=null) {
    			this.getDireccionFavor().getUbicacion().setDireccionList(null);
    		}
    	}
    	if(this.getFiltroFavorList()!=null && !this.getFiltroFavorList().isEmpty()) {
    		for(FiltroFavor filtroFavor: this.getFiltroFavorList()) {
    			filtroFavor.setFavor(null);
    			if(filtroFavor.getFiltro()!=null) {
    				if(filtroFavor.getFiltro().getCategoriaFiltro()!=null) {
    					filtroFavor.getFiltro().getCategoriaFiltro().setFiltroList(null);    					
    				}
					filtroFavor.getFiltro().setFiltroFavorList(null);
					filtroFavor.getFiltro().setFiltroPersonaList(null);
					filtroFavor.getFiltro().setFiltroConfiguracionUsuarioList(null);
    			}
    		}
    	}
    	if(this.getUsuarioAyudaFavorList()!=null && !this.getUsuarioAyudaFavorList().isEmpty()) {
    		for (UsuarioAyudaFavor usuarioAyudaFavor:this.getUsuarioAyudaFavorList()) {
    			usuarioAyudaFavor.setFavor(null);
    			if(usuarioAyudaFavor.getUsuario()!=null) {
    				usuarioAyudaFavor.getUsuario().setPersona(null);
    			}
    		}
    	}
    	if(this.getUsuarioRealiza()!=null) {
    		this.getUsuarioRealiza().setPersona(null);
    	}
    	if(this.getUsuarioSolicita()!=null) {
    		this.getUsuarioSolicita().setPersona(null);
    	}
    	return this;
    }
}
