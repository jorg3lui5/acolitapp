package com.acolitapp.acolitappserve.rest;
import org.springframework.web.bind.annotation.RestController;

import com.acolitapp.acolitappserve.controlador.CalificacionControlador;
import com.acolitapp.acolitappserve.controlador.DetalleTipoPagoControlador;
import com.acolitapp.acolitappserve.controlador.FavorControlador;
import com.acolitapp.acolitappserve.controlador.UsuarioControlador;
import com.acolitapp.acolitappserve.controlador.FavorControlador;
import com.acolitapp.acolitappserve.dto.ManejoString;
import com.acolitapp.acolitappserve.dto.RespuestaDTO;
import com.acolitapp.acolitappserve.enumerador.EstadoErrorEnum;
import com.acolitapp.acolitappserve.enumerador.EstadoRespuestaEnum;
import com.acolitapp.acolitappserve.modelo.Calificacion;
import com.acolitapp.acolitappserve.modelo.Favor;
import com.acolitapp.acolitappserve.modelo.FiltroFavor;
import com.acolitapp.acolitappserve.modelo.UsuarioAyudaFavor;
import com.acolitapp.acolitappserve.modelo.Favor;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/favor")
@CrossOrigin(origins = "*")
public class FavorRest {
	
  public Logger logger = LoggerFactory.getLogger(FavorRest.class);

  @Autowired
  private FavorControlador controladorPrincipal;
  @Autowired
  private UsuarioControlador usuarioControlador;
  @Autowired
  private CalificacionControlador calificacionControlador;
  @Autowired
  private DetalleTipoPagoControlador detalleTipoPagoControlador;
  

  @GetMapping("/recuperarPorId/{id}")
  public RespuestaDTO recuperarPorId(@PathVariable(value = "id") Long id) {
	try {
		Favor objeto = controladorPrincipal.recuperarPorId(id);
		if(objeto!=null) {
			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objeto);
		}
		else{
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "EL DATO NO EXISTE");
		}
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
  
  @GetMapping("/listarTodos")
  public RespuestaDTO listarTodos() {
	try {
		List<Favor> objetos = controladorPrincipal.findAll();
		return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objetos);
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }

  @PostMapping("/crear")
  public RespuestaDTO crear(@RequestBody Favor objeto) {
		try {
			if (objeto != null) {
				if(objeto.getUsuarioSolicita()!=null) {
					objeto.setUsuarioSolicita(usuarioControlador.recuperarPorUsuario(objeto.getUsuarioSolicita().getUsuario()));
				}
		    	if (objeto.getCalificacionList()!=null && !objeto.getCalificacionList().isEmpty()) {
		    		for(Calificacion calificacion: objeto.getCalificacionList()) {
		    			calificacion.setFavor(objeto);
		    			calificacion.setUsuario(usuarioControlador.recuperarPorUsuario(calificacion.getUsuario().getUsuario()));
		    			calificacionControlador.save(calificacion);
		    		}
		    	}
		    	if(objeto.getDetalleTipoPago()!=null) {
		    		detalleTipoPagoControlador.save(objeto.getDetalleTipoPago());
		    	}
//		    	if(this.getDireccionFavor()!=null) {
//		    		this.getDireccionFavor().setFavorList(null);
//		    		this.getDireccionFavor().setPersona(null);
//		    		this.getDireccionFavor().setTrabajoList(null);
//		    		if(this.getDireccionFavor().getUbicacion()!=null) {
//		    			this.getDireccionFavor().getUbicacion().setDireccionList(null);
//		    		}
//		    	}
//		    	if(this.getFiltroFavorList()!=null && !this.getFiltroFavorList().isEmpty()) {
//		    		for(FiltroFavor filtroFavor: this.getFiltroFavorList()) {
//		    			filtroFavor.setFavor(null);
//		    			if(filtroFavor.getFiltro()!=null) {
//		    				if(filtroFavor.getFiltro().getCategoriaFiltro()!=null) {
//		    					filtroFavor.getFiltro().getCategoriaFiltro().setFiltroList(null);    					
//		    				}
//							filtroFavor.getFiltro().setFiltroFavorList(null);
//							filtroFavor.getFiltro().setFiltroPersonaList(null);
//							filtroFavor.getFiltro().setFiltroConfiguracionUsuarioList(null);
//		    			}
//		    		}
//		    	}
//		    	if(this.getUsuarioAyudaFavorList()!=null && !this.getUsuarioAyudaFavorList().isEmpty()) {
//		    		for (UsuarioAyudaFavor usuarioAyudaFavor:this.getUsuarioAyudaFavorList()) {
//		    			usuarioAyudaFavor.setFavor(null);
//		    			if(usuarioAyudaFavor.getUsuario()!=null) {
//		    				usuarioAyudaFavor.getUsuario().setPersona(null);
//		    			}
//		    		}
//		    	}
//		    	if(this.getUsuarioRealiza()!=null) {
//		    		this.getUsuarioRealiza().setPersona(null);
//		    	}
//		    	if(this.getUsuarioSolicita()!=null) {
//		    		this.getUsuarioSolicita().setPersona(null);
//		    	}
				controladorPrincipal.save(objeto);
			  return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objeto);
			} else {
				return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "NO TIENE OBJETO A CREAR.");
			}
		} catch (Exception e) {
			String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
			if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
				logger.error(errorMensaje, e);
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
		}
  }

  @PostMapping("/actualizar")
  public RespuestaDTO actualizar(@RequestBody Favor objEntidad){
	try {
		if (objEntidad != null && objEntidad.getId()!=null) {
			controladorPrincipal.save(objEntidad);
			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objEntidad);
		} else {
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "NO TIENE OBJETO A MODIFICAR.");
		}
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }

  @GetMapping("/eliminar/{id}")
  public RespuestaDTO eliminar(@PathVariable(value = "id") Long id) throws Exception {
	try {
		Favor objeto = controladorPrincipal.recuperarPorId(id);
		controladorPrincipal.delete(objeto);
	
		return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), "ELIMINADO CORRECTAMENTE");
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
}