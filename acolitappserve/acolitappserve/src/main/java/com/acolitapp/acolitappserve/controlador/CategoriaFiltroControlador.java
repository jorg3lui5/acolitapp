package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.CategoriaFiltro;

public interface CategoriaFiltroControlador extends JpaRepository<CategoriaFiltro, Long>{	
		
	@Query("SELECT p "
			+ " FROM CategoriaFiltro p "
			+ " WHERE p.id = :id ")
	public CategoriaFiltro recuperarPorId(@Param("id") Long id);
		
}
