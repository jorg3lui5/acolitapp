package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Profesion;

public interface ProfesionControlador extends JpaRepository<Profesion, Long>{	
		
	@Query("SELECT p "
			+ " FROM Profesion p "
			+ " WHERE p.id = :id ")
	public Profesion recuperarPorId(@Param("id") Long id);
		
}
