package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Favor;

public interface FavorControlador extends JpaRepository<Favor, Long>{	
		
	@Query("SELECT p "
			+ " FROM Favor p "
			+ " WHERE p.id = :id ")
	public Favor recuperarPorId(@Param("id") Long id);
		
}
