package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Nacionalidad;

public interface NacionalidadControlador extends JpaRepository<Nacionalidad, Long>{	
		
	@Query("SELECT p "
			+ " FROM Nacionalidad p "
			+ " WHERE p.id = :id ")
	public Nacionalidad recuperarPorId(@Param("id") Long id);
	
	@Query("SELECT p "
			+ " FROM Nacionalidad p "
			+ " WHERE p.pais.id = :id ")
	public Nacionalidad recuperarPorIdPais(@Param("id") Long id);
	
}
