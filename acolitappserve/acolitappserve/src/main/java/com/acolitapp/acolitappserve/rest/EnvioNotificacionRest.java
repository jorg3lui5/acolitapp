package com.acolitapp.acolitappserve.rest;
import org.springframework.web.bind.annotation.RestController;

import com.acolitapp.acolitappserve.controlador.EnvioNotificacionControlador;
import com.acolitapp.acolitappserve.controlador.PaisControlador;
import com.acolitapp.acolitappserve.dto.DatoNotificacionDTO;
import com.acolitapp.acolitappserve.dto.EnvioNotificacionDTO;
import com.acolitapp.acolitappserve.dto.ManejoString;
import com.acolitapp.acolitappserve.dto.RespuestaDTO;
import com.acolitapp.acolitappserve.enumerador.EstadoErrorEnum;
import com.acolitapp.acolitappserve.enumerador.EstadoRespuestaEnum;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;

@RestController
@RequestMapping("/envioNotificacion")
@CrossOrigin(origins = "*")
public class EnvioNotificacionRest {
	
  public Logger logger = LoggerFactory.getLogger(EnvioNotificacionRest.class);

  @Autowired
  private EnvioNotificacionControlador envioNotificacionControlador;

  @PostMapping(value = "/enviar")
  public RespuestaDTO enviar(@RequestBody EnvioNotificacionDTO envioNotificacionDTO) {
	try {
		if (envioNotificacionDTO != null) {

		    JSONObject notification = new JSONObject();
		    notification.put("title", envioNotificacionDTO.getTitulo());
		    notification.put("body", envioNotificacionDTO.getMensaje());
		    
		    JSONObject data = new JSONObject();
		    if(envioNotificacionDTO.getDatos()!=null && !envioNotificacionDTO.getDatos().isEmpty()) {
		    	for(DatoNotificacionDTO datoNotificacionDTO:envioNotificacionDTO.getDatos()) {
				    data.put(datoNotificacionDTO.getClave(), datoNotificacionDTO.getValor());
		    	}
		    }
		 
			JSONObject body = new JSONObject();
		    body.put("to", "");
//		    body.put("priority", envioNotificacionDTO.getPrioridad());
		    body.put("notification", notification);
		    body.put("data", data);
		    /**
			    {
			       "notification": {
			          "title": "JSA Notification",
			          "body": "Happy Message!"
			       },
			       "data": {
			          "Key-1": "JSA Data 1",
			          "Key-2": "JSA Data 2"
			       },
			       "to": "/topics/JavaSampleApproach",
			       "priority": "high"
			    }
			*/
		    HttpEntity<String> request = new HttpEntity<>(body.toString());
		    
		    CompletableFuture<String> pushNotification = envioNotificacionControlador.enviar(request);
		    CompletableFuture.allOf(pushNotification).join();
		    
		    String firebaseResponse = pushNotification.get();

			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), firebaseResponse);
		
		} 
		else {			
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "NO TIENE DATOS A NOTIFICAR");
		}
		
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
  
  @PostMapping(value = "/enviarTodos")
  public RespuestaDTO enviarTodos(@RequestBody EnvioNotificacionDTO envioNotificacionDTO) {
	try {
		if (envioNotificacionDTO != null) {

			envioNotificacionControlador.enviarTodos(envioNotificacionDTO);

			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), "mensaje enviado");
		
		} 
		else {			
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "NO TIENE DATOS A NOTIFICAR");
		}
		
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
  

  
}