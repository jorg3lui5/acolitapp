package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.FiltroFavor;

public interface FiltroFavorControlador extends JpaRepository<FiltroFavor, Long>{	
		
	@Query("SELECT p "
			+ " FROM FiltroFavor p "
			+ " WHERE p.id = :id ")
	public FiltroFavor recuperarPorId(@Param("id") Long id);
		
}
