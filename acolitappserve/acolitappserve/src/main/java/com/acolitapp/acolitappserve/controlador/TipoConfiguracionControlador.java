package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.TipoConfiguracion;

public interface TipoConfiguracionControlador extends JpaRepository<TipoConfiguracion, Long>{	
		
	@Query("SELECT p "
			+ " FROM TipoConfiguracion p "
			+ " WHERE p.id = :id ")
	public TipoConfiguracion recuperarPorId(@Param("id") Long id);
		
}
