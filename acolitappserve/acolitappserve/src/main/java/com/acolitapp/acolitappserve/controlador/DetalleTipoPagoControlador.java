package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.DetalleTipoPago;

public interface DetalleTipoPagoControlador extends JpaRepository<DetalleTipoPago, Long>{	
		
	@Query("SELECT p "
			+ " FROM DetalleTipoPago p "
			+ " WHERE p.id = :id ")
	public DetalleTipoPago recuperarPorId(@Param("id") Long id);
		
}
