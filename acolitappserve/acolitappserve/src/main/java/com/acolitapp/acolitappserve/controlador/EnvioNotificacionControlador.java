package com.acolitapp.acolitappserve.controlador;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.annotation.PostConstruct;

import org.hibernate.validator.internal.util.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.acolitapp.acolitappserve.dto.DatoNotificacionDTO;
import com.acolitapp.acolitappserve.dto.EnvioNotificacionDTO;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.ApnsConfig;
import com.google.firebase.messaging.Aps;
import com.google.firebase.messaging.ApsAlert;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.SendResponse;
import com.google.firebase.messaging.TopicManagementResponse;
import com.google.firebase.messaging.WebpushConfig;
import com.google.firebase.messaging.WebpushNotification;



@Service
public class EnvioNotificacionControlador {	

	@Value("${acolitapp.token}")
	private String tokenAcolitapp;
	  
	@Value("${firebase.api.url}")
	private String urlApiFirebase;
	
	@Value("${app.firebase-configuration-file}")
    private String firebaseConfigPath;
	
    
	@Async
	public CompletableFuture<String> enviar(HttpEntity<String> entity) {
		RestTemplate restTemplate = new RestTemplate();
	    
		/**
		https://fcm.googleapis.com/fcm/send
		Content-Type:application/json
		Authorization:key=FIREBASE_SERVER_KEY*/
	 
	    ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
	    interceptors.add(new EnvioNotificacionInterceptor("Authorization", "key=" + tokenAcolitapp));
	    interceptors.add(new EnvioNotificacionInterceptor("Content-Type", "application/json"));
	    restTemplate.setInterceptors(interceptors);
	 
	    String firebaseResponse = restTemplate.postForObject(urlApiFirebase, entity, String.class);
	 
	    return CompletableFuture.completedFuture(firebaseResponse);
	}
	
	public void enviarTodos(EnvioNotificacionDTO envioNotificacionDTO) throws FirebaseMessagingException, IOException {
		

        FileInputStream serviceAccount = new FileInputStream(firebaseConfigPath);

		FirebaseOptions options = new FirebaseOptions.Builder()
		  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
		  .setDatabaseUrl("https://acolitav1.firebaseio.com")
		  .build();
		FirebaseApp firebaseApp=null;
		
		boolean hasBeenInitialized=false;
		List<FirebaseApp> firebaseApps = FirebaseApp.getApps();
		for(FirebaseApp app : firebaseApps){
		    if(app.getName().equals(FirebaseApp.DEFAULT_APP_NAME)){
		        hasBeenInitialized=true;
		        firebaseApp = app;
		    }
		}

		if(!hasBeenInitialized) {
			firebaseApp = FirebaseApp.initializeApp(options);
		}
		
		List<String> registrationTokens = Arrays.asList(
	        "dynsuoX3bnA:APA91bHEoVLmvXuXCfnOUPYopbSXHH13jhHe6k2A0-oHTvjPySfLUMJW9De5OrPNWwRnNy7LrbukYNR0HaqeBgSeUB_dFh-TCnEe7N0fr6expP6qc426zBV8xCBbI229_P4scLloRbD_"
	    );
		
		MulticastMessage.Builder builder = MulticastMessage.builder()
				.setNotification(new Notification(envioNotificacionDTO.getTitulo(),envioNotificacionDTO.getMensaje()))
				.addAllTokens(registrationTokens);
		

		if(envioNotificacionDTO.getDatos()!=null && !envioNotificacionDTO.getDatos().isEmpty()) {
	    	for(DatoNotificacionDTO datoNotificacionDTO:envioNotificacionDTO.getDatos()) {
	    		builder.putData(datoNotificacionDTO.getClave(), datoNotificacionDTO.getValor());
	    	}
	    }
		MulticastMessage multicastMessage=builder.build();
		        
	    BatchResponse responseMulticast = FirebaseMessaging.getInstance().sendMulticast(multicastMessage);

		System.out.println(responseMulticast.getSuccessCount() + " messages were sent successfully");
		
	}
	
    public void initialize() {
        try {
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(new ClassPathResource(firebaseConfigPath).getInputStream())).build();
            if (FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
                System.out.println("Firebase application has been initialized");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
