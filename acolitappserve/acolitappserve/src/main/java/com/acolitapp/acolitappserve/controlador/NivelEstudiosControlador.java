package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.NivelEstudios;

public interface NivelEstudiosControlador extends JpaRepository<NivelEstudios, Long>{	
		
	@Query("SELECT p "
			+ " FROM NivelEstudios p "
			+ " WHERE p.id = :id ")
	public NivelEstudios recuperarPorId(@Param("id") Long id);
		
}
