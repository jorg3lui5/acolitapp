package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Filtro;

public interface FiltroControlador extends JpaRepository<Filtro, Long>{	
		
	@Query("SELECT p "
			+ " FROM Filtro p "
			+ " WHERE p.id = :id ")
	public Filtro recuperarPorId(@Param("id") Long id);
		
}
