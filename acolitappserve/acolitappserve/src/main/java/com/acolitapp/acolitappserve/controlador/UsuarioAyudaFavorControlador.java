package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.UsuarioAyudaFavor;

public interface UsuarioAyudaFavorControlador extends JpaRepository<UsuarioAyudaFavor, Long>{	
		
	@Query("SELECT p "
			+ " FROM UsuarioAyudaFavor p "
			+ " WHERE p.id = :id ")
	public UsuarioAyudaFavor recuperarPorId(@Param("id") Long id);
		
}
