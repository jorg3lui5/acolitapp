/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acolitapp.acolitappserve.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "usuario")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    @Size(max = 20)
    @Column(name = "usuario", length = 20)
    private String usuario;
    @Size(max = 20)
    @Column(name = "correo", length = 20)
    private String correo;
    @Size(max = 12)
    @Column(name = "contrasenia", length = 12)
    private String contrasenia;
    @Lob
    @Column(name = "foto")
    @Type(type="org.hibernate.type.BinaryType")
    private byte[] foto;
    @Column(name = "calificacion")
    private Integer calificacion;
    @Column(name = "estado")
    private Integer estado;
    @Size(max = 250)
    @Column(name = "token", length = 20)
    private String token;
//    @OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
//    private List<Calificacion> calificacionList;
//    @OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
//    private List<FiltroConfiguracionUsuario> filtroConfiguracionUsuarioList;
//    @OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
//    private List<UsuarioAyudaFavor> usuarioAyudaFavorList;
//    @OneToMany(mappedBy = "usuarioRealiza", fetch = FetchType.LAZY)
//    private List<Favor> favorList;
//    @OneToMany(mappedBy = "usuarioSolicita", fetch = FetchType.LAZY)
//    private List<Favor> favorList1;
    @JoinColumn(name = "persona", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
//    @JsonManagedReference
    private Persona persona;

    public Usuario() {
    }

    public Usuario(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

//    @XmlTransient
//    public List<Calificacion> getCalificacionList() {
//        return calificacionList;
//    }
//
//    public void setCalificacionList(List<Calificacion> calificacionList) {
//        this.calificacionList = calificacionList;
//    }
//
//    @XmlTransient
//    public List<FiltroConfiguracionUsuario> getFiltroConfiguracionUsuarioList() {
//        return filtroConfiguracionUsuarioList;
//    }
//
//    public void setFiltroConfiguracionUsuarioList(List<FiltroConfiguracionUsuario> filtroConfiguracionUsuarioList) {
//        this.filtroConfiguracionUsuarioList = filtroConfiguracionUsuarioList;
//    }
//
//    @XmlTransient
//    public List<UsuarioAyudaFavor> getUsuarioAyudaFavorList() {
//        return usuarioAyudaFavorList;
//    }
//
//    public void setUsuarioAyudaFavorList(List<UsuarioAyudaFavor> usuarioAyudaFavorList) {
//        this.usuarioAyudaFavorList = usuarioAyudaFavorList;
//    }
//
//    @XmlTransient
//    public List<Favor> getFavorList() {
//        return favorList;
//    }
//
//    public void setFavorList(List<Favor> favorList) {
//        this.favorList = favorList;
//    }
//
//    @XmlTransient
//    public List<Favor> getFavorList1() {
//        return favorList1;
//    }
//
//    public void setFavorList1(List<Favor> favorList1) {
//        this.favorList1 = favorList1;
//    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aaa.Usuario[ id=" + id + " ]";
    }
    
    public Usuario limpiarRecursividad() {
    	if (this.getPersona()!=null) {
    		//this.getPersona().setUsuarioList(null);
    	}
    	return this;
    }
    
}
