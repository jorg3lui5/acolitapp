package com.acolitapp.acolitappserve.dto;



public class DatoNotificacionDTO {

	private String clave;
	private String valor;
	
	public DatoNotificacionDTO() {
		super();
	}

	public DatoNotificacionDTO(String clave, String valor) {
		super();
		this.clave = clave;
		this.valor = valor;
	}
	
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}

	

}
