package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.TipoLicencia;

public interface TipoLicenciaControlador extends JpaRepository<TipoLicencia, Long>{	
		
	@Query("SELECT p "
			+ " FROM TipoLicencia p "
			+ " WHERE p.id = :id ")
	public TipoLicencia recuperarPorId(@Param("id") Long id);
		
}
