package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.VehiculoPersona;

public interface VehiculoPersonaControlador extends JpaRepository<VehiculoPersona, Long>{	
		
	@Query("SELECT p "
			+ " FROM VehiculoPersona p "
			+ " WHERE p.id = :id ")
	public VehiculoPersona recuperarPorId(@Param("id") Long id);
		
}
