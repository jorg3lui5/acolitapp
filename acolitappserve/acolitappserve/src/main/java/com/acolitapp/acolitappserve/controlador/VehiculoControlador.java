package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Vehiculo;

public interface VehiculoControlador extends JpaRepository<Vehiculo, Long>{	
		
	@Query("SELECT p "
			+ " FROM Vehiculo p "
			+ " WHERE p.id = :id ")
	public Vehiculo recuperarPorId(@Param("id") Long id);
		
}
