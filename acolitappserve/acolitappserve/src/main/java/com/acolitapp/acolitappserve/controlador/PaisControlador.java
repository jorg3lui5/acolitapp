package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Pais;

public interface PaisControlador extends JpaRepository<Pais, Long>{	
	
	public List<Pais> findById(String id);
	
	@Query("SELECT p "
			+ " FROM Pais p "
			+ " WHERE p.id = :id ")
	public Pais recuperarPorId(@Param("id") Long id);
		
}
