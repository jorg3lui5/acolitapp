package com.acolitapp.acolitappserve.dto;

import java.lang.annotation.Annotation;

import javax.ejb.ApplicationException;
import javax.persistence.Table;

@ApplicationException(rollback=true)
public class ExceptionPersonalizada extends Exception {
	private static final long serialVersionUID = 1L;

	public ExceptionPersonalizada() {
	
	}
	
	public ExceptionPersonalizada(String msg) {
		super(msg);
	}

	public ExceptionPersonalizada(Throwable ex) {
		super(ex);
	}

	public ExceptionPersonalizada(String msg, Throwable ex) {
		super(msg, ex);
	}

	public static String format(String formato, Object entidad) {
		Annotation[] anotaciones = entidad.getClass().getDeclaredAnnotations();
		String nombreTabla = entidad.getClass().getSimpleName().toUpperCase();
		for (Annotation an : anotaciones) {
			if (an.getClass().getName().equals(Table.class.getName())) {
				Table tb = (Table) an;
				nombreTabla = tb.name();
			}
		}
		return String.format(formato, new Object[] { nombreTabla, entidad.getClass().getCanonicalName() });
	}
}
