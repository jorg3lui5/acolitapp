package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.FiltroConfiguracionUsuario;

public interface FiltroConfiguracionUsuarioControlador extends JpaRepository<FiltroConfiguracionUsuario, Long>{	
		
	@Query("SELECT p "
			+ " FROM FiltroConfiguracionUsuario p "
			+ " WHERE p.id = :id ")
	public FiltroConfiguracionUsuario recuperarPorId(@Param("id") Long id);
		
}
