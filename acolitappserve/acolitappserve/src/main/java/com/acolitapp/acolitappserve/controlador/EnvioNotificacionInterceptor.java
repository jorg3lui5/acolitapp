package com.acolitapp.acolitappserve.controlador;

import java.io.IOException;
 
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;
 
public class EnvioNotificacionInterceptor implements ClientHttpRequestInterceptor {
 
  private final String claveCabecera;
  private final String valorCabecera;
 
  public EnvioNotificacionInterceptor(String claveCabecera, String valorCabecera) {
    this.claveCabecera = claveCabecera;
    this.valorCabecera = valorCabecera;
  }
 
  @Override
  public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
      throws IOException {
    HttpRequest wrapper = new HttpRequestWrapper(request);
    wrapper.getHeaders().set(claveCabecera, valorCabecera);
    return execution.execute(wrapper, body);
  }
}