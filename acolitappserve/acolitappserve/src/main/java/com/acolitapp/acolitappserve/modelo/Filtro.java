/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acolitapp.acolitappserve.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "filtro")
public class Filtro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    @Size(max = 20)
    @Column(name = "nombre", length = 20)
    private String nombre;
    @Size(max = 50)
    @Column(name = "descripcion", length = 50)
    private String descripcion;
    @JoinColumn(name = "categoria_filtro", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private CategoriaFiltro categoriaFiltro;
    @OneToMany(mappedBy = "filtro", fetch = FetchType.LAZY)
    private List<FiltroFavor> filtroFavorList;
    @OneToMany(mappedBy = "filtro", fetch = FetchType.LAZY)
    private List<FiltroConfiguracionUsuario> filtroConfiguracionUsuarioList;
    @OneToMany(mappedBy = "filtro", fetch = FetchType.LAZY)
    private List<FiltroPersona> filtroPersonaList;

    public Filtro() {
    }

    public Filtro(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public CategoriaFiltro getCategoriaFiltro() {
        return categoriaFiltro;
    }

    public void setCategoriaFiltro(CategoriaFiltro categoriaFiltro) {
        this.categoriaFiltro = categoriaFiltro;
    }

    @XmlTransient
    public List<FiltroFavor> getFiltroFavorList() {
        return filtroFavorList;
    }

    public void setFiltroFavorList(List<FiltroFavor> filtroFavorList) {
        this.filtroFavorList = filtroFavorList;
    }

    @XmlTransient
    public List<FiltroConfiguracionUsuario> getFiltroConfiguracionUsuarioList() {
        return filtroConfiguracionUsuarioList;
    }

    public void setFiltroConfiguracionUsuarioList(List<FiltroConfiguracionUsuario> filtroConfiguracionUsuarioList) {
        this.filtroConfiguracionUsuarioList = filtroConfiguracionUsuarioList;
    }

    @XmlTransient
    public List<FiltroPersona> getFiltroPersonaList() {
        return filtroPersonaList;
    }

    public void setFiltroPersonaList(List<FiltroPersona> filtroPersonaList) {
        this.filtroPersonaList = filtroPersonaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Filtro)) {
            return false;
        }
        Filtro other = (Filtro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aaa.Filtro[ id=" + id + " ]";
    }
    
}
