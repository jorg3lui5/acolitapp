package com.acolitapp.acolitappserve.rest;
import org.springframework.web.bind.annotation.RestController;

import com.acolitapp.acolitappserve.controlador.FiltroFavorControlador;
import com.acolitapp.acolitappserve.controlador.PaisControlador;
import com.acolitapp.acolitappserve.dto.ManejoString;
import com.acolitapp.acolitappserve.dto.RespuestaDTO;
import com.acolitapp.acolitappserve.enumerador.EstadoErrorEnum;
import com.acolitapp.acolitappserve.enumerador.EstadoRespuestaEnum;
import com.acolitapp.acolitappserve.modelo.FiltroFavor;
import com.acolitapp.acolitappserve.modelo.Pais;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/filtroFavor")
@CrossOrigin(origins = "*")
public class FiltroFavorRest {
	
  public Logger logger = LoggerFactory.getLogger(FiltroFavorRest.class);

  @Autowired
  private PaisControlador controladorPrincipal;

  @GetMapping("/recuperarPorId/{id}")
  public RespuestaDTO recuperarPorId(@PathVariable(value = "id") Long id) {
	try {
		Pais objeto = controladorPrincipal.recuperarPorId(id);
		if(objeto!=null) {
			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objeto);
		}
		else{
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "EL DATO NO EXISTE");
		}
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
  
  @GetMapping("/listarTodos")
  public RespuestaDTO listarTodos() {
	try {
		List<Pais> objetos = controladorPrincipal.findAll();
		return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objetos);
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }

  @PostMapping("/crear")
  public RespuestaDTO crear(@RequestBody Pais objeto) {
		try {
			if (objeto != null) {
				controladorPrincipal.save(objeto);
			  return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objeto);
			} else {
				return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "NO TIENE OBJETO A CREAR.");
			}
		} catch (Exception e) {
			String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
			if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
				logger.error(errorMensaje, e);
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
		}
  }

  @PostMapping("/actualizar")
  public RespuestaDTO actualizar(@RequestBody Pais objEntidad){
	try {
		if (objEntidad != null && objEntidad.getId()!=null) {
			controladorPrincipal.save(objEntidad);
			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objEntidad);
		} else {
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "NO TIENE OBJETO A MODIFICAR.");
		}
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }

  @GetMapping("/eliminar/{id}")
  public RespuestaDTO eliminar(@PathVariable(value = "id") Long id) throws Exception {
	try {
		Pais objeto = controladorPrincipal.recuperarPorId(id);
		controladorPrincipal.delete(objeto);
	
		return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), "ELIMINADO CORRECTAMENTE");
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
}