package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.FiltroPersona;

public interface FiltroPersonaControlador extends JpaRepository<FiltroPersona, Long>{	
		
	@Query("SELECT p "
			+ " FROM FiltroPersona p "
			+ " WHERE p.id = :id ")
	public FiltroPersona recuperarPorId(@Param("id") Long id);
		
}
