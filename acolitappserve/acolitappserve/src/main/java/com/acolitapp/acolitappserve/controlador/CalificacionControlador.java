package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Calificacion;

public interface CalificacionControlador extends JpaRepository<Calificacion, Long>{	
		
	@Query("SELECT p "
			+ " FROM Calificacion p "
			+ " WHERE p.id = :id ")
	public Calificacion recuperarPorId(@Param("id") Long id);
		
}
