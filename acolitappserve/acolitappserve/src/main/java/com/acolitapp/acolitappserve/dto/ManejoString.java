package com.acolitapp.acolitappserve.dto;

import com.acolitapp.acolitappserve.enumerador.EstadoErrorEnum;

public class ManejoString {

	
	public static String mensajeErrorFinal(String mensajeOriginal) {

		String mensajeFinal = null;
		String patron = EstadoErrorEnum.EJECUCION.getCode() + "|" + EstadoErrorEnum.VALIDACION.getCode();

		String[] pilaMensajes = mensajeOriginal.split(patron);
		for (int i = 0; i < pilaMensajes.length; i++) {
			if (pilaMensajes[i].contains("ERROR:")) {
				mensajeFinal = pilaMensajes[i].substring(0, pilaMensajes[i].indexOf("ERROR:"));
			} else {
				mensajeFinal = pilaMensajes[i];
			}
			if (mensajeFinal.trim().length() > 1)
				break;
		}

		return mensajeFinal;

	}
	
	
	/**
	 * @author jaime.perez 18/05/2017 
	 * Permite descomponer los mensajes y mostrar el usuario final
	 * @param mensajeOriginal
	 * @return
	 */
	public static String mensajeUsuarioFinal(String mensajeOriginal) {

		String mensajeFinal = null;
		String patron = EstadoErrorEnum.EJECUCION.getCode() + "|" + EstadoErrorEnum.VALIDACION.getCode();
		

		String[] pilaMensajes = mensajeOriginal.split(patron);
		for (int i = 0; i < pilaMensajes.length; i++) {
			if (pilaMensajes[i].contains("ERROR:")) {
				mensajeFinal = pilaMensajes[i].substring(0, pilaMensajes[i].indexOf("ERROR:"));
			} else {
				mensajeFinal = pilaMensajes[i];
			}
			if (mensajeFinal.trim().length() > 1)
				break;
		}
		
		if(pilaMensajes != null && pilaMensajes.length>0){
			
			if (mensajeOriginal.contains(EstadoErrorEnum.VALIDACION.getCode())){
				mensajeFinal = pilaMensajes[pilaMensajes.length-1];
				
				mensajeFinal = EstadoErrorEnum.VALIDACION.getCode() + " " + mensajeFinal;
				
			}else { //Se teien que validar que presenta 
				mensajeFinal =  " ERROR DE EJECUCION. COMUNIQUESE CON EL ADMINISTRADOR";
			}			
		}		

		return mensajeFinal;

	}
	
	
	
	/**
	 * Permite transformar un texto que tiene tildes o signos latinos, en caracteres simples, transformando en ISO-8859-1 
	 * @param texto
	 * @return
	 * @throws ExceptionPersonalizada
	 */
	public static String transformaStringUTF16(String texto) throws ExceptionPersonalizada{
		
		try {
			String s = texto;
			
			s = s.replaceAll("�", "n");
			s = s.replaceAll("�", "N");
			
			s = s.replaceAll("�", "a");
			s = s.replaceAll("�", "A");
			
			s = s.replaceAll("�", "e");
			s = s.replaceAll("�", "E");
			
			s = s.replaceAll("�", "i");
			s = s.replaceAll("�", "I");
			
			
			s = s.replaceAll("�", "O");
			s = s.replaceAll("�", "o");
			
			s = s.replaceAll("�", "u");
			s = s.replaceAll("�", "U");
			
			s = s.replaceAll("�", "u");
			s = s.replaceAll("�", "U");
			
			s = new String(s.getBytes("UTF-8"), "ISO-8859-1");
			
			return s;
		} catch (Exception e) {
			String mensajeError = "ERROR: " + EstadoErrorEnum.EJECUCION.getCode() + "  ManejoString.transformaStringUTF16 Detalle:... " + e.getMessage();
			
			throw new ExceptionPersonalizada(mensajeError,e);
		}
		
	}
	
	
	
	
	
}
