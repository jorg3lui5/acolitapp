package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.TipoPago;

public interface TipoPagoControlador extends JpaRepository<TipoPago, Long>{	
		
	@Query("SELECT p "
			+ " FROM TipoPago p "
			+ " WHERE p.id = :id ")
	public TipoPago recuperarPorId(@Param("id") Long id);
		
}
