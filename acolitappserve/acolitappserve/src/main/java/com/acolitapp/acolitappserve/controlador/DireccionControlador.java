package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Direccion;

public interface DireccionControlador extends JpaRepository<Direccion, Long>{	
		
	@Query("SELECT p "
			+ " FROM Direccion p "
			+ " WHERE p.id = :id ")
	public Direccion recuperarPorId(@Param("id") Long id);
		
}
