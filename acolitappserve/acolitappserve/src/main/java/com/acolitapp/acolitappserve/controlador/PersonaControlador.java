package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Persona;

public interface PersonaControlador extends JpaRepository<Persona, Long>{	
		
	@Query("SELECT p "
			+ " FROM Persona p "
			+ " WHERE p.id = :id ")
	public Persona recuperarPorId(@Param("id") Long id);
	
	@Query("SELECT p "
			+ " FROM Persona p "
			+ " WHERE p.identificacion = :id ")
	public Persona recuperarPorIdentificacion(@Param("id") String id);
	
	@Query("SELECT p.persona "
			+ " FROM Usuario p "
			+ " WHERE p.usuario = :id ")
	public Persona recuperarPorUsuario(@Param("id") String id);
}
