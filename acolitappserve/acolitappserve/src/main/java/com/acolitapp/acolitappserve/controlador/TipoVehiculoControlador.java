package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.TipoVehiculo;

public interface TipoVehiculoControlador extends JpaRepository<TipoVehiculo, Long>{	
		
	@Query("SELECT p "
			+ " FROM TipoVehiculo p "
			+ " WHERE p.id = :id ")
	public TipoVehiculo recuperarPorId(@Param("id") Long id);
		
}
