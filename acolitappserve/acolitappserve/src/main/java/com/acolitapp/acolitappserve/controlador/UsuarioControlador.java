package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Usuario;

public interface UsuarioControlador extends JpaRepository<Usuario, Long>{	
		
	@Query("SELECT p "
			+ " FROM Usuario p "
			+ " WHERE p.id = :id ")
	public Usuario recuperarPorId(@Param("id") Long id);
	
	@Query("SELECT p "
			+ " FROM Usuario p "
			+ " WHERE p.correo = :id ")
	public Usuario recuperarPorCorreo(@Param("id") String id);
	
	@Query("SELECT p "
			+ " FROM Usuario p "
			+ " WHERE p.usuario = :id ")
	public Usuario recuperarPorUsuario(@Param("id") String id);
	
	@Query("SELECT p "
			+ " FROM Usuario p "
			+ " WHERE p.usuario = :usuario "
			+ " AND p.contrasenia = :contrasenia")
	public Usuario loguear(@Param("usuario") String usuario,@Param("contrasenia") String contrasenia);
		
}
