package com.acolitapp.acolitappserve.rest;
import org.springframework.web.bind.annotation.RestController;

import com.acolitapp.acolitappserve.controlador.PersonaControlador;
import com.acolitapp.acolitappserve.controlador.UsuarioControlador;
import com.acolitapp.acolitappserve.dto.ManejoString;
import com.acolitapp.acolitappserve.dto.RespuestaDTO;
import com.acolitapp.acolitappserve.enumerador.EstadoErrorEnum;
import com.acolitapp.acolitappserve.enumerador.EstadoRespuestaEnum;
import com.acolitapp.acolitappserve.modelo.Persona;
import com.acolitapp.acolitappserve.modelo.Usuario;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/usuario")
@CrossOrigin(origins = "*")
public class UsuarioRest {
	
  public Logger logger = LoggerFactory.getLogger(UsuarioRest.class);

  @Autowired
  private UsuarioControlador usuarioControlador;
  @Autowired
  private PersonaControlador personaControlador;

  @GetMapping("/recuperarPorId/{id}")
  public RespuestaDTO recuperarPorId(@PathVariable(value = "id") Long id) {
	try {
		Usuario usuario = usuarioControlador.recuperarPorId(id);
		if(usuario!=null) {
			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), usuario.limpiarRecursividad());
		}
		else{
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "EL USUARIO NO EXISTE");

		}
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
  
  @GetMapping("/recuperarPorUsuario/{nombreUsuario}")
  public RespuestaDTO recuperarPorUsuario(@PathVariable(value = "nombreUsuario") String nombreUsuario) {
	try {
		Usuario usuario = usuarioControlador.recuperarPorUsuario(nombreUsuario);
		if(usuario!=null) {
			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), usuario.limpiarRecursividad());
		}
		else{
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "EL USUARIO NO EXISTE");

		}
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
  
  @GetMapping("/listarTodos")
  public RespuestaDTO listarTodos() {
	try {
		List<Usuario> objetos = usuarioControlador.findAll();
		if(objetos!=null && !objetos.isEmpty()) {
			for (Usuario objeto: objetos) {
				objeto.limpiarRecursividad();
			}
		}
		return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objetos);
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }

  @PostMapping("/crear")
  public RespuestaDTO crear(@RequestBody Usuario usuario) {
		try {
			if (usuario != null) {
				if(usuario.getId()==null) {
				  Persona p=personaControlador.recuperarPorIdentificacion(usuario.getPersona().getIdentificacion());
				  if(p!=null) {
					  return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "Ya existe un usuario con esa identificacion");
				  }
				  else {
					  Usuario u=usuarioControlador.recuperarPorUsuario(usuario.getUsuario());
					  if(u!=null) {
						  return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "Ya existe ese nombre de usuario");
					  }
					  else {
						  u=usuarioControlador.recuperarPorCorreo(usuario.getCorreo());
						  if(u!=null) {
							  return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "Ya existe un usuario con ese correo");
						  }
						  else {
							  personaControlador.save(usuario.getPersona());
							  usuarioControlador.save(usuario);
							  return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), usuario.limpiarRecursividad());
						  }
					  }
				  }
				}
				return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "NO SE PUDO CREAR EL USUARIO");

			} else {
				return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "NO TIENE OBJETO A CREAR.");
			}
		} catch (Exception e) {
			String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
			}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
			if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
				logger.error(errorMensaje, e);
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
		}
  }

  @PostMapping("/actualizar")
  public RespuestaDTO actualizar(@RequestBody Usuario objEntidad){
	try {
		if (objEntidad != null && objEntidad.getId()!=null) {
			usuarioControlador.save(objEntidad);
			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), objEntidad.limpiarRecursividad());
		} else {
			return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "NO TIENE OBJETO A MODIFICAR.");
		}
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }

  @GetMapping("/eliminar/{id}")
  public RespuestaDTO eliminar(@PathVariable(value = "id") Long id) throws Exception {
	try {
		Usuario usuario = usuarioControlador.recuperarPorId(id);
		usuarioControlador.delete(usuario);
	
		return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), "ELIMINADO CORRECTAMENTE");
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
  
  @CrossOrigin(origins = "*")
  @GetMapping("/loguear/{usuario}/{contrasenia}")
  public RespuestaDTO loguear(@PathVariable(value = "usuario") String usuario,@PathVariable(value = "contrasenia") String contrasenia) {
	try {
		Usuario usuarioLogueado = usuarioControlador.loguear(usuario, contrasenia);
		
		if(usuarioLogueado!=null) {
			return new RespuestaDTO(EstadoRespuestaEnum.OK.getCode(), usuarioLogueado.limpiarRecursividad());
		}
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), "USUARIO O CONTRASEÑA INCORRECTA");
	
	} catch (Exception e) {
		String errorMensaje = "Error: " + this.getClass().getSimpleName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName() + ":" + EstadoErrorEnum.EJECUCION.getCode() + " " + e.toString();
		if (!errorMensaje.contains(EstadoErrorEnum.VALIDACION.getCode()))
			logger.error(errorMensaje, e);
		return new RespuestaDTO(EstadoRespuestaEnum.ERROR.getCode(), ManejoString.mensajeUsuarioFinal(errorMensaje));
	}
  }
  

}