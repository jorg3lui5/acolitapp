package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Ocupacion;

public interface OcupacionControlador extends JpaRepository<Ocupacion, Long>{	
		
	@Query("SELECT p "
			+ " FROM Ocupacion p "
			+ " WHERE p.id = :id ")
	public Ocupacion recuperarPorId(@Param("id") Long id);
		
}
