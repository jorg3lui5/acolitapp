package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Ubicacion;

public interface UbicacionControlador extends JpaRepository<Ubicacion, Long>{	
		
	@Query("SELECT p "
			+ " FROM Ubicacion p "
			+ " WHERE p.id = :id ")
	public Ubicacion recuperarPorId(@Param("id") Long id);
		
}
