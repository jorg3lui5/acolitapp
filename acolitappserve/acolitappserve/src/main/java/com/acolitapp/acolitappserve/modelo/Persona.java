/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acolitapp.acolitappserve.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "persona")
public class Persona {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    @Size(max = 20)
    @Column(name = "identificacion", length = 20)
    private String identificacion;
    @Size(max = 50)
    @Column(name = "nombres_apellidos", length = 50)
    private String nombresApellidos;
    @Size(max = 10)
    @Column(name = "telefono", length = 10)
    private String telefono;
    @Size(max = 15)
    @Column(name = "licencia", length = 15)
    private String licencia;
    @Size(max = 120)
    @Column(name = "informacion_adicional", length = 250)
    private String informacionAdicional;
    @OneToMany(mappedBy = "persona")
    @LazyCollection (LazyCollectionOption.FALSE)
    private List<Trabajo> trabajoList;
    @OneToMany(mappedBy = "persona")
    @LazyCollection (LazyCollectionOption.FALSE)
    private List<FiltroPersona> filtroPersonaList;
    @JoinColumn(name = "nacionalidad", referencedColumnName = "id")
    @ManyToOne()
    @LazyCollection (LazyCollectionOption.FALSE)
    private Nacionalidad nacionalidad;
    @JoinColumn(name = "nivel_estudios", referencedColumnName = "id")
    @ManyToOne()
    @LazyCollection (LazyCollectionOption.FALSE)
    private NivelEstudios nivelEstudios;
    @JoinColumn(name = "ocupacion", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Ocupacion ocupacion;
    @JoinColumn(name = "profesion", referencedColumnName = "id")
    @ManyToOne()
    @LazyCollection (LazyCollectionOption.FALSE)
    private Profesion profesion;
    @JoinColumn(name = "tipo_licencia", referencedColumnName = "id")
    @ManyToOne()
    @LazyCollection (LazyCollectionOption.FALSE)
    private TipoLicencia tipoLicencia;
    @OneToMany(mappedBy = "persona")
    @LazyCollection (LazyCollectionOption.FALSE)
    private List<Direccion> direccionList;
    @OneToMany(mappedBy = "persona")
    @LazyCollection (LazyCollectionOption.FALSE)
    private List<VehiculoPersona> vehiculoPersonaList;
//    @OneToMany(mappedBy = "persona")
//    @LazyCollection (LazyCollectionOption.TRUE)
////    @JsonBackReference
//    private List<Usuario> usuarioList;
    

    public Persona() {
    }

    public Persona(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombresApellidos() {
        return nombresApellidos;
    }

    public void setNombresApellidos(String nombresApellidos) {
        this.nombresApellidos = nombresApellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getLicencia() {
        return licencia;
    }

    public void setLicencia(String licencia) {
        this.licencia = licencia;
    }

    public String getInformacionAdicional() {
        return informacionAdicional;
    }

    public void setInformacionAdicional(String informacionAdicional) {
        this.informacionAdicional = informacionAdicional;
    }

    @XmlTransient
    public List<Trabajo> getTrabajoList() {
        return trabajoList;
    }

    public void setTrabajoList(List<Trabajo> trabajoList) {
        this.trabajoList = trabajoList;
    }

    @XmlTransient
    public List<FiltroPersona> getFiltroPersonaList() {
        return filtroPersonaList;
    }

    public void setFiltroPersonaList(List<FiltroPersona> filtroPersonaList) {
        this.filtroPersonaList = filtroPersonaList;
    }

    public Nacionalidad getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(Nacionalidad nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public NivelEstudios getNivelEstudios() {
        return nivelEstudios;
    }

    public void setNivelEstudios(NivelEstudios nivelEstudios) {
        this.nivelEstudios = nivelEstudios;
    }

    public Ocupacion getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(Ocupacion ocupacion) {
        this.ocupacion = ocupacion;
    }

    public Profesion getProfesion() {
        return profesion;
    }

    public void setProfesion(Profesion profesion) {
        this.profesion = profesion;
    }

    public TipoLicencia getTipoLicencia() {
        return tipoLicencia;
    }

    public void setTipoLicencia(TipoLicencia tipoLicencia) {
        this.tipoLicencia = tipoLicencia;
    }

    @XmlTransient
    public List<Direccion> getDireccionList() {
        return direccionList;
    }

    public void setDireccionList(List<Direccion> direccionList) {
        this.direccionList = direccionList;
    }

    @XmlTransient
    public List<VehiculoPersona> getVehiculoPersonaList() {
        return vehiculoPersonaList;
    }

    public void setVehiculoPersonaList(List<VehiculoPersona> vehiculoPersonaList) {
        this.vehiculoPersonaList = vehiculoPersonaList;
    }

//    @XmlTransient
//    public List<Usuario> getUsuarioList() {
//        return usuarioList;
//    }
//
//    public void setUsuarioList(List<Usuario> usuarioList) {
//        this.usuarioList = usuarioList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persona)) {
            return false;
        }
        Persona other = (Persona) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "aaa.Persona[ id=" + id + " ]";
    }
    
    public Persona limpiarRecursividad() {
//    	
//    	if (this.getPersona()!=null) {
//    		this.getPersona().setUsuarioList(null);
//    	}
    	if(this.getDireccionList()!=null && !this.getDireccionList().isEmpty()) {
    		for (Direccion direccion: this.getDireccionList())
    		{
    			direccion.setPersona(null);
    		}
    	}
    	if(this.getVehiculoPersonaList()!=null && !this.getVehiculoPersonaList().isEmpty()) {
    		for(VehiculoPersona vehiculoPersona: this.getVehiculoPersonaList()) {
    			vehiculoPersona.setPersona(null);
    		}
    	}
    	return this;
    }
    

}
