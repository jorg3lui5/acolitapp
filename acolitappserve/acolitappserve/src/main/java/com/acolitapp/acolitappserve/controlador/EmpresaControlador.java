package com.acolitapp.acolitappserve.controlador;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.acolitapp.acolitappserve.modelo.Empresa;

public interface EmpresaControlador extends JpaRepository<Empresa, Long>{	
		
	@Query("SELECT p "
			+ " FROM Empresa p "
			+ " WHERE p.id = :id ")
	public Empresa recuperarPorId(@Param("id") Long id);
		
}
