import { Component, OnInit } from '@angular/core';
import { Constantes } from '../compartido/constantes';
import { DatosPersonalesPage } from '../datos-personales/datos-personales.page';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

    constantes: Constantes = new Constantes;
  datosPersonales: DatosPersonalesPage

  constructor(private route: Router) {

  }

  ngOnInit() {
  }

  abrirDatosPersonales() {
    this.route.navigate(['/datos-personales']);
  }

  abrirLogin() {
    this.route.navigate(['/login']);
  }
}
