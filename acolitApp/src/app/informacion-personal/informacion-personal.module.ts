import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InformacionPersonalPageRoutingModule } from './informacion-personal-routing.module';

import { InformacionPersonalPage } from './informacion-personal.page';
import { CabeceraPageModule } from '../cabecera/cabecera.module'
import { PiePaginaPageModule } from '../pie-pagina/pie-pagina.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InformacionPersonalPageRoutingModule,
    CabeceraPageModule,
    PiePaginaPageModule
  ],
  declarations: [
    InformacionPersonalPage
  ],
  exports: [
    InformacionPersonalPage
  ],
})
export class InformacionPersonalPageModule {}
