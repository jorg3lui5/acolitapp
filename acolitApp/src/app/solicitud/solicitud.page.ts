import { Component, OnInit } from '@angular/core';
import { Constantes } from '../compartido/constantes';

@Component({
  selector: 'app-solicitud',
  templateUrl: './solicitud.page.html',
  styleUrls: ['./solicitud.page.scss'],
})
export class SolicitudPage implements OnInit {

    constantes: Constantes = new Constantes;

  constructor() { }

  ngOnInit() {
  }

}
