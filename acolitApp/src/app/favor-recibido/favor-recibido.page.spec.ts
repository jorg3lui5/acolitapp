import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FavorRecibidoPage } from './favor-recibido.page';

describe('FavorRecibidoPage', () => {
  let component: FavorRecibidoPage;
  let fixture: ComponentFixture<FavorRecibidoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavorRecibidoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FavorRecibidoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
