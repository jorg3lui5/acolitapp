import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AjusteFavoresPage } from './ajuste-favores.page';

describe('AjusteFavoresPage', () => {
  let component: AjusteFavoresPage;
  let fixture: ComponentFixture<AjusteFavoresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjusteFavoresPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AjusteFavoresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
