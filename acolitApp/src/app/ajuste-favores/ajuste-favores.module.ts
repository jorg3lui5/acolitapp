import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AjusteFavoresPageRoutingModule } from './ajuste-favores-routing.module';

import { AjusteFavoresPage } from './ajuste-favores.page';
import { CabeceraPageModule } from '../cabecera/cabecera.module'
import { PiePaginaPageModule } from '../pie-pagina/pie-pagina.module'
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AjusteFavoresPageRoutingModule,
    CabeceraPageModule,
    PiePaginaPageModule
  ],
  declarations: [
    AjusteFavoresPage
  ],
  exports: [
    AjusteFavoresPage
  ],
})
export class AjusteFavoresPageModule {}
