import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EnvioNotificacionesService {

  constructor(public http: HttpClient) {

  }

  enviarNotificacion(){
    return this.http.get('https://jsonplaceholder.typicode.com/users');

  }
}
