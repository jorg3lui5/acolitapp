import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenuLateralPage } from './menu-lateral.page';

describe('MenuLateralPage', () => {
  let component: MenuLateralPage;
  let fixture: ComponentFixture<MenuLateralPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuLateralPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenuLateralPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
