import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FavoresRealizadosPage } from './favores-realizados.page';

describe('FavoresRealizadosPage', () => {
  let component: FavoresRealizadosPage;
  let fixture: ComponentFixture<FavoresRealizadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoresRealizadosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FavoresRealizadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
