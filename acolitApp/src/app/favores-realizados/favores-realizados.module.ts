import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { FavoresRealizadosPage } from './favores-realizados.page';
import { CabeceraPageModule } from '../cabecera/cabecera.module';
import { PiePaginaPageModule } from '../pie-pagina/pie-pagina.module';
import { MenuLateralPageModule } from '../menu-lateral/menu-lateral.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CabeceraPageModule,
    PiePaginaPageModule,
    MenuLateralPageModule,
    RouterModule.forChild([{ path: '', component: FavoresRealizadosPage }])

  ],
  declarations: [FavoresRealizadosPage]
})
export class FavoresRealizadosPageModule {}
