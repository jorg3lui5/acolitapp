import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FiltrosPageRoutingModule } from './filtros-routing.module';

import { FiltrosPage } from './filtros.page';
import { CabeceraPageModule } from '../cabecera/cabecera.module'
import { PiePaginaPageModule } from '../pie-pagina/pie-pagina.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FiltrosPageRoutingModule
  ],
  declarations: [
    FiltrosPage
  ],
  exports: [
    FiltrosPage
  ],
})
export class FiltrosPageModule {}
