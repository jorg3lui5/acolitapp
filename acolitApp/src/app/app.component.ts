import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Constantes } from './compartido/constantes';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
    constantes: Constantes = new Constantes;

  public appPages = [
    {
      title: this.constantes._cuenta,
      url: '/registro',
      icon: 'person'
    },
    {
      title: this.constantes._notificaciones,
      url: '/notificaciones',
      icon: 'notifications'
    },
    {
      title: this.constantes._favores,
      url: '/favores',
      icon: 'hand'
    },
    {
      title: this.constantes._cerrarSesion,
      url: '/login',
      icon: 'log-out'
    }
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
