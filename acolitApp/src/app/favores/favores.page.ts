import { Component, OnInit } from '@angular/core';
import { Constantes } from '../compartido/constantes';

@Component({
  selector: 'app-favores',
  templateUrl: './favores.page.html',
  styleUrls: ['./favores.page.scss'],
})
export class FavoresPage implements OnInit {

    constantes: Constantes = new Constantes;

  constructor() { }

  ngOnInit() {
  }

}
