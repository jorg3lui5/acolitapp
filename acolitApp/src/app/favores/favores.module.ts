import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FavoresPageRoutingModule } from './favores-routing.module';

import { FavoresPage } from './favores.page';
import { CabeceraPageModule } from '../cabecera/cabecera.module'
import { PiePaginaPageModule } from '../pie-pagina/pie-pagina.module'
import { MenuLateralPageModule } from '../menu-lateral/menu-lateral.module'


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FavoresPageRoutingModule,
    CabeceraPageModule,
    PiePaginaPageModule,
    MenuLateralPageModule
  ],
  declarations: [
    FavoresPage
  ],
  exports: [
    FavoresPage
  ],
})
export class FavoresPageModule {}
