import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PiePaginaPage } from './pie-pagina.page';

const routes: Routes = [
  {
    path: 'pie-pagina',
    component: PiePaginaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PiePaginaPageRoutingModule {}
