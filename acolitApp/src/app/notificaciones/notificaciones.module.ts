import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotificacionesPageRoutingModule } from './notificaciones-routing.module';

import { NotificacionesPage } from './notificaciones.page';
import { CabeceraPageModule } from '../cabecera/cabecera.module'
import { PiePaginaPageModule } from '../pie-pagina/pie-pagina.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotificacionesPageRoutingModule,
    CabeceraPageModule,
    PiePaginaPageModule
  ],
  declarations: [
    NotificacionesPage
  ],
  exports: [
    NotificacionesPage
  ],
})
export class NotificacionesPageModule {}
