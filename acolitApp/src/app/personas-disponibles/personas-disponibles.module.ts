import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PersonasDisponiblesPageRoutingModule } from './personas-disponibles-routing.module';

import { PersonasDisponiblesPage } from './personas-disponibles.page';
import { CabeceraPageModule } from '../cabecera/cabecera.module'
import { PiePaginaPageModule } from '../pie-pagina/pie-pagina.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PersonasDisponiblesPageRoutingModule,
    CabeceraPageModule,
    PiePaginaPageModule
  ],
  declarations: [
    PersonasDisponiblesPage
  ],
  exports: [
    PersonasDisponiblesPage
  ],
})
export class PersonasDisponiblesPageModule {}
