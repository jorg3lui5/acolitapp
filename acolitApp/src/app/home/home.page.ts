import { Component } from '@angular/core';
import { Constantes } from '../compartido/constantes';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

    constantes: Constantes = new Constantes;

  constructor() {}

}
