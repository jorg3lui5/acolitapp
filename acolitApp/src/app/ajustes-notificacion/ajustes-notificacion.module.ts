import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AjustesNotificacionPageRoutingModule } from './ajustes-notificacion-routing.module';

import { AjustesNotificacionPage } from './ajustes-notificacion.page';
import { CabeceraPageModule } from '../cabecera/cabecera.module'
import { PiePaginaPageModule } from '../pie-pagina/pie-pagina.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AjustesNotificacionPageRoutingModule,
    CabeceraPageModule,
    PiePaginaPageModule
  ],
  declarations: [
    AjustesNotificacionPage
  ],
  exports: [
    AjustesNotificacionPage
  ],
})
export class AjustesNotificacionPageModule {}
