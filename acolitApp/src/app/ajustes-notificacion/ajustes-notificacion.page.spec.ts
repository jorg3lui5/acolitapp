import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AjustesNotificacionPage } from './ajustes-notificacion.page';

describe('AjustesNotificacionPage', () => {
  let component: AjustesNotificacionPage;
  let fixture: ComponentFixture<AjustesNotificacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjustesNotificacionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AjustesNotificacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
