import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FavoresSolicitadosPage } from './favores-solicitados.page';

describe('FavoresSolicitadosPage', () => {
  let component: FavoresSolicitadosPage;
  let fixture: ComponentFixture<FavoresSolicitadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoresSolicitadosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FavoresSolicitadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
