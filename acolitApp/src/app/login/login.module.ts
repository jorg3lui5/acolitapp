import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginPageRoutingModule } from './login-routing.module';

import { LoginPage } from './login.page';
import { CabeceraPageModule } from '../cabecera/cabecera.module'
import { PiePaginaPageModule } from '../pie-pagina/pie-pagina.module'
import { FCM } from '@ionic-native/fcm/ngx';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginPageRoutingModule,
    CabeceraPageModule,
    PiePaginaPageModule,

  ],
  declarations: [
    LoginPage
  ],
  exports: [
    LoginPage
  ],
  providers: [
    FCM
  ],
})
export class LoginPageModule {}
