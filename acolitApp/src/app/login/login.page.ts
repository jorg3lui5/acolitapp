import { Component, OnInit } from '@angular/core';
import { Constantes } from '../compartido/constantes';
import { Router } from '@angular/router';
import { EnvioNotificacionesService } from '../services/envio-notificaciones/envio-notificaciones.service';
import { FCM } from '@ionic-native/fcm/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constantes: Constantes = new Constantes;
  usuarios:any;
  constructor(private fcm: FCM,private route: Router, public _envioNotificacionesService: EnvioNotificacionesService) { 
    
  }

  ngOnInit() {
    this.fcm.getToken().then(token => {
      console.log("TOKENN");
      console.log(token);
    });

    this._envioNotificacionesService.enviarNotificacion()
    .subscribe(
      (data)=> {
        this.usuarios=data;
      },
      (error)=>{
        console.log(error);
      }
    )
  }

  abrirFavores() {
    this.route.navigate(['/favores']);
  }

  abrirRegistro() {
    this.route.navigate(['/registro']);
  }


}
