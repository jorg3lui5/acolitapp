import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CabeceraPageRoutingModule } from './cabecera-routing.module';

import { CabeceraPage } from './cabecera.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CabeceraPageRoutingModule
  ],
  declarations: [
    CabeceraPage
  ]
  ,
  exports: [
    CabeceraPage
  ],
})
export class CabeceraPageModule {}
