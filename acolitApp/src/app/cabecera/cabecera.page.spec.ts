import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CabeceraPage } from './cabecera.page';

describe('CabeceraPage', () => {
  let component: CabeceraPage;
  let fixture: ComponentFixture<CabeceraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabeceraPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CabeceraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
