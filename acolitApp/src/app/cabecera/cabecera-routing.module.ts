import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CabeceraPage } from './cabecera.page';

const routes: Routes = [
  {
    path: 'cabecera',
    component: CabeceraPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CabeceraPageRoutingModule {}
