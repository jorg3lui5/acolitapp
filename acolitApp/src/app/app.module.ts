import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginPageModule } from './login/login.module'
import { HttpClientModule } from '@angular/common/http';
import { EnvioNotificacionesService } from './services/envio-notificaciones/envio-notificaciones.service';
import { FCM } from '@ionic-native/fcm/ngx';

@NgModule({
  declarations: [
    AppComponent,
  ],
  entryComponents: [
    
  ],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    LoginPageModule,
    HttpClientModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    EnvioNotificacionesService,
    FCM
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
