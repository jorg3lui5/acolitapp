import { Component, OnInit } from '@angular/core';
import { Constantes } from '../compartido/constantes';

@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.page.html',
  styleUrls: ['./datos-personales.page.scss'],
})
export class DatosPersonalesPage implements OnInit {

    constantes: Constantes = new Constantes;

  constructor() { }

  ngOnInit() {
  }

}
