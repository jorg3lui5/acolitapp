toc.dat                                                                                             0000600 0004000 0002000 00000371046 13575630675 0014473 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        PGDMP       4                    w         	   acolitapp    12.1    12.1 �   e           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false         f           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false         g           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false         h           1262    16393 	   acolitapp    DATABASE     g   CREATE DATABASE acolitapp WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';
    DROP DATABASE acolitapp;
                postgres    false                     2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                postgres    false         i           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   postgres    false    3         �            1259    16394    calificacion    TABLE     �   CREATE TABLE public.calificacion (
    id bigint NOT NULL,
    calificacion integer,
    id_usuario bigint NOT NULL,
    id_favor bigint NOT NULL
);
     DROP TABLE public.calificacion;
       public         heap    postgres    false    3         �            1259    16397    calificacion_id_favor_seq    SEQUENCE     �   CREATE SEQUENCE public.calificacion_id_favor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.calificacion_id_favor_seq;
       public          postgres    false    202    3         j           0    0    calificacion_id_favor_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.calificacion_id_favor_seq OWNED BY public.calificacion.id_favor;
          public          postgres    false    203         �            1259    16399    calificacion_id_seq    SEQUENCE     |   CREATE SEQUENCE public.calificacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.calificacion_id_seq;
       public          postgres    false    3    202         k           0    0    calificacion_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.calificacion_id_seq OWNED BY public.calificacion.id;
          public          postgres    false    204         �            1259    16401    calificacion_id_usuario_seq    SEQUENCE     �   CREATE SEQUENCE public.calificacion_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.calificacion_id_usuario_seq;
       public          postgres    false    202    3         l           0    0    calificacion_id_usuario_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.calificacion_id_usuario_seq OWNED BY public.calificacion.id_usuario;
          public          postgres    false    205         �            1259    16403    categoria_filtro    TABLE     c   CREATE TABLE public.categoria_filtro (
    id bigint NOT NULL,
    nombre character varying(30)
);
 $   DROP TABLE public.categoria_filtro;
       public         heap    postgres    false    3         �            1259    16406    categoria_filtro_id_seq    SEQUENCE     �   CREATE SEQUENCE public.categoria_filtro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.categoria_filtro_id_seq;
       public          postgres    false    3    206         m           0    0    categoria_filtro_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.categoria_filtro_id_seq OWNED BY public.categoria_filtro.id;
          public          postgres    false    207         �            1259    16408    ciudad    TABLE     v   CREATE TABLE public.ciudad (
    id bigint NOT NULL,
    nombre character varying(30),
    id_pais bigint NOT NULL
);
    DROP TABLE public.ciudad;
       public         heap    postgres    false    3         �            1259    16411    ciudad_id_pais_seq    SEQUENCE     {   CREATE SEQUENCE public.ciudad_id_pais_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.ciudad_id_pais_seq;
       public          postgres    false    3    208         n           0    0    ciudad_id_pais_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.ciudad_id_pais_seq OWNED BY public.ciudad.id_pais;
          public          postgres    false    209         �            1259    16413    ciudad_id_seq    SEQUENCE     v   CREATE SEQUENCE public.ciudad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.ciudad_id_seq;
       public          postgres    false    208    3         o           0    0    ciudad_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.ciudad_id_seq OWNED BY public.ciudad.id;
          public          postgres    false    210         �            1259    16415    detalle_tipo_pago    TABLE     �   CREATE TABLE public.detalle_tipo_pago (
    id bigint NOT NULL,
    id_tipo_pago bigint NOT NULL,
    valor double precision,
    descripcion character varying(50)
);
 %   DROP TABLE public.detalle_tipo_pago;
       public         heap    postgres    false    3         �            1259    16418    detalle_tipo_pago_id_seq    SEQUENCE     �   CREATE SEQUENCE public.detalle_tipo_pago_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.detalle_tipo_pago_id_seq;
       public          postgres    false    3    211         p           0    0    detalle_tipo_pago_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.detalle_tipo_pago_id_seq OWNED BY public.detalle_tipo_pago.id;
          public          postgres    false    212         �            1259    16420 "   detalle_tipo_pago_id_tipo_pago_seq    SEQUENCE     �   CREATE SEQUENCE public.detalle_tipo_pago_id_tipo_pago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.detalle_tipo_pago_id_tipo_pago_seq;
       public          postgres    false    211    3         q           0    0 "   detalle_tipo_pago_id_tipo_pago_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.detalle_tipo_pago_id_tipo_pago_seq OWNED BY public.detalle_tipo_pago.id_tipo_pago;
          public          postgres    false    213         �            1259    16422 	   direccion    TABLE     b  CREATE TABLE public.direccion (
    id bigint NOT NULL,
    calle_principal character varying(30),
    calle_secundaria character varying(30),
    barrio character varying(30),
    numero_casa character varying(20),
    referencia character varying(120),
    id_ciudad bigint NOT NULL,
    id_ubicacion bigint NOT NULL,
    id_persona bigint NOT NULL
);
    DROP TABLE public.direccion;
       public         heap    postgres    false    3         �            1259    16425    direccion_id_ciudad_seq    SEQUENCE     �   CREATE SEQUENCE public.direccion_id_ciudad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.direccion_id_ciudad_seq;
       public          postgres    false    214    3         r           0    0    direccion_id_ciudad_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.direccion_id_ciudad_seq OWNED BY public.direccion.id_ciudad;
          public          postgres    false    215         �            1259    16427    direccion_id_persona_seq    SEQUENCE     �   CREATE SEQUENCE public.direccion_id_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.direccion_id_persona_seq;
       public          postgres    false    3    214         s           0    0    direccion_id_persona_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.direccion_id_persona_seq OWNED BY public.direccion.id_persona;
          public          postgres    false    216         �            1259    16429    direccion_id_seq    SEQUENCE     y   CREATE SEQUENCE public.direccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.direccion_id_seq;
       public          postgres    false    3    214         t           0    0    direccion_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.direccion_id_seq OWNED BY public.direccion.id;
          public          postgres    false    217         �            1259    16431    direccion_id_ubicacion_seq    SEQUENCE     �   CREATE SEQUENCE public.direccion_id_ubicacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.direccion_id_ubicacion_seq;
       public          postgres    false    3    214         u           0    0    direccion_id_ubicacion_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.direccion_id_ubicacion_seq OWNED BY public.direccion.id_ubicacion;
          public          postgres    false    218         �            1259    16433    empresa    TABLE     Z   CREATE TABLE public.empresa (
    id bigint NOT NULL,
    nombre character varying(50)
);
    DROP TABLE public.empresa;
       public         heap    postgres    false    3         �            1259    16436    empresa_id_seq    SEQUENCE     w   CREATE SEQUENCE public.empresa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.empresa_id_seq;
       public          postgres    false    3    219         v           0    0    empresa_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.empresa_id_seq OWNED BY public.empresa.id;
          public          postgres    false    220         �            1259    16438    favor    TABLE     �  CREATE TABLE public.favor (
    id bigint NOT NULL,
    titulo character varying(20),
    descripcion character varying(150),
    estado character varying(20),
    fecha_solicita timestamp without time zone,
    fecha_realiza timestamp without time zone,
    id_direccion_favor bigint NOT NULL,
    id_detalle_tipo_pago bigint NOT NULL,
    id_usuario_solicita bigint NOT NULL,
    id_usuario_realiza bigint NOT NULL
);
    DROP TABLE public.favor;
       public         heap    postgres    false    3         �            1259    16441    favor_id_detalle_tipo_pago_seq    SEQUENCE     �   CREATE SEQUENCE public.favor_id_detalle_tipo_pago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.favor_id_detalle_tipo_pago_seq;
       public          postgres    false    221    3         w           0    0    favor_id_detalle_tipo_pago_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.favor_id_detalle_tipo_pago_seq OWNED BY public.favor.id_detalle_tipo_pago;
          public          postgres    false    222         �            1259    16443    favor_id_direccion_favor_seq    SEQUENCE     �   CREATE SEQUENCE public.favor_id_direccion_favor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.favor_id_direccion_favor_seq;
       public          postgres    false    3    221         x           0    0    favor_id_direccion_favor_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.favor_id_direccion_favor_seq OWNED BY public.favor.id_direccion_favor;
          public          postgres    false    223         �            1259    16445    favor_id_seq    SEQUENCE     u   CREATE SEQUENCE public.favor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.favor_id_seq;
       public          postgres    false    221    3         y           0    0    favor_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.favor_id_seq OWNED BY public.favor.id;
          public          postgres    false    224         �            1259    16447    favor_id_usuario_realiza_seq    SEQUENCE     �   CREATE SEQUENCE public.favor_id_usuario_realiza_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.favor_id_usuario_realiza_seq;
       public          postgres    false    3    221         z           0    0    favor_id_usuario_realiza_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.favor_id_usuario_realiza_seq OWNED BY public.favor.id_usuario_realiza;
          public          postgres    false    225         �            1259    16449    favor_id_usuario_solicita_seq    SEQUENCE     �   CREATE SEQUENCE public.favor_id_usuario_solicita_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.favor_id_usuario_solicita_seq;
       public          postgres    false    3    221         {           0    0    favor_id_usuario_solicita_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.favor_id_usuario_solicita_seq OWNED BY public.favor.id_usuario_solicita;
          public          postgres    false    226         �            1259    16451    filtro    TABLE     �   CREATE TABLE public.filtro (
    id bigint NOT NULL,
    nombre character varying(20),
    descripcion character varying(50),
    id_categoria_filtro bigint
);
    DROP TABLE public.filtro;
       public         heap    postgres    false    3         �            1259    16454    filtro_configuracion_usuario    TABLE     6  CREATE TABLE public.filtro_configuracion_usuario (
    id bigint NOT NULL,
    id_filtro bigint NOT NULL,
    id_tipo_configuracion bigint NOT NULL,
    id_usuario bigint NOT NULL,
    valor_string character varying(20),
    valor_int integer,
    id_tabla bigint NOT NULL,
    estado character varying(20)
);
 0   DROP TABLE public.filtro_configuracion_usuario;
       public         heap    postgres    false    3         �            1259    16457 *   filtro_configuracion_usuario_id_filtro_seq    SEQUENCE     �   CREATE SEQUENCE public.filtro_configuracion_usuario_id_filtro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.filtro_configuracion_usuario_id_filtro_seq;
       public          postgres    false    3    228         |           0    0 *   filtro_configuracion_usuario_id_filtro_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.filtro_configuracion_usuario_id_filtro_seq OWNED BY public.filtro_configuracion_usuario.id_filtro;
          public          postgres    false    229         �            1259    16459 #   filtro_configuracion_usuario_id_seq    SEQUENCE     �   CREATE SEQUENCE public.filtro_configuracion_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.filtro_configuracion_usuario_id_seq;
       public          postgres    false    228    3         }           0    0 #   filtro_configuracion_usuario_id_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.filtro_configuracion_usuario_id_seq OWNED BY public.filtro_configuracion_usuario.id;
          public          postgres    false    230         �            1259    16461 )   filtro_configuracion_usuario_id_tabla_seq    SEQUENCE     �   CREATE SEQUENCE public.filtro_configuracion_usuario_id_tabla_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 @   DROP SEQUENCE public.filtro_configuracion_usuario_id_tabla_seq;
       public          postgres    false    3    228         ~           0    0 )   filtro_configuracion_usuario_id_tabla_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public.filtro_configuracion_usuario_id_tabla_seq OWNED BY public.filtro_configuracion_usuario.id_tabla;
          public          postgres    false    231         �            1259    16463 6   filtro_configuracion_usuario_id_tipo_configuracion_seq    SEQUENCE     �   CREATE SEQUENCE public.filtro_configuracion_usuario_id_tipo_configuracion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 M   DROP SEQUENCE public.filtro_configuracion_usuario_id_tipo_configuracion_seq;
       public          postgres    false    3    228                    0    0 6   filtro_configuracion_usuario_id_tipo_configuracion_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.filtro_configuracion_usuario_id_tipo_configuracion_seq OWNED BY public.filtro_configuracion_usuario.id_tipo_configuracion;
          public          postgres    false    232         �            1259    16465 +   filtro_configuracion_usuario_id_usuario_seq    SEQUENCE     �   CREATE SEQUENCE public.filtro_configuracion_usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 B   DROP SEQUENCE public.filtro_configuracion_usuario_id_usuario_seq;
       public          postgres    false    228    3         �           0    0 +   filtro_configuracion_usuario_id_usuario_seq    SEQUENCE OWNED BY     {   ALTER SEQUENCE public.filtro_configuracion_usuario_id_usuario_seq OWNED BY public.filtro_configuracion_usuario.id_usuario;
          public          postgres    false    233         �            1259    16467    filtro_favor    TABLE     �   CREATE TABLE public.filtro_favor (
    id bigint NOT NULL,
    id_filtro bigint NOT NULL,
    id_favor bigint NOT NULL,
    valor_string character varying(20),
    valor_int integer,
    id_tabla bigint NOT NULL,
    estado character varying(20)
);
     DROP TABLE public.filtro_favor;
       public         heap    postgres    false    3         �            1259    16470    filtro_favor_id_favor_seq    SEQUENCE     �   CREATE SEQUENCE public.filtro_favor_id_favor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.filtro_favor_id_favor_seq;
       public          postgres    false    234    3         �           0    0    filtro_favor_id_favor_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.filtro_favor_id_favor_seq OWNED BY public.filtro_favor.id_favor;
          public          postgres    false    235         �            1259    16472    filtro_favor_id_filtro_seq    SEQUENCE     �   CREATE SEQUENCE public.filtro_favor_id_filtro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.filtro_favor_id_filtro_seq;
       public          postgres    false    3    234         �           0    0    filtro_favor_id_filtro_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.filtro_favor_id_filtro_seq OWNED BY public.filtro_favor.id_filtro;
          public          postgres    false    236         �            1259    16474    filtro_favor_id_seq    SEQUENCE     |   CREATE SEQUENCE public.filtro_favor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.filtro_favor_id_seq;
       public          postgres    false    234    3         �           0    0    filtro_favor_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.filtro_favor_id_seq OWNED BY public.filtro_favor.id;
          public          postgres    false    237         �            1259    16476    filtro_favor_id_tabla_seq    SEQUENCE     �   CREATE SEQUENCE public.filtro_favor_id_tabla_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.filtro_favor_id_tabla_seq;
       public          postgres    false    3    234         �           0    0    filtro_favor_id_tabla_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.filtro_favor_id_tabla_seq OWNED BY public.filtro_favor.id_tabla;
          public          postgres    false    238         �            1259    16478    filtro_id_seq    SEQUENCE     v   CREATE SEQUENCE public.filtro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.filtro_id_seq;
       public          postgres    false    227    3         �           0    0    filtro_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.filtro_id_seq OWNED BY public.filtro.id;
          public          postgres    false    239         �            1259    16480    filtro_persona    TABLE     �   CREATE TABLE public.filtro_persona (
    id bigint NOT NULL,
    id_filtro bigint NOT NULL,
    id_persona bigint NOT NULL,
    valor_string character varying(20),
    valor_int integer,
    id_tabla bigint NOT NULL,
    estado character varying(20)
);
 "   DROP TABLE public.filtro_persona;
       public         heap    postgres    false    3         �            1259    16483    filtro_persona_id_filtro_seq    SEQUENCE     �   CREATE SEQUENCE public.filtro_persona_id_filtro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.filtro_persona_id_filtro_seq;
       public          postgres    false    240    3         �           0    0    filtro_persona_id_filtro_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.filtro_persona_id_filtro_seq OWNED BY public.filtro_persona.id_filtro;
          public          postgres    false    241         �            1259    16485    filtro_persona_id_persona_seq    SEQUENCE     �   CREATE SEQUENCE public.filtro_persona_id_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.filtro_persona_id_persona_seq;
       public          postgres    false    3    240         �           0    0    filtro_persona_id_persona_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.filtro_persona_id_persona_seq OWNED BY public.filtro_persona.id_persona;
          public          postgres    false    242         �            1259    16487    filtro_persona_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.filtro_persona_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.filtro_persona_id_seq;
       public          postgres    false    240    3         �           0    0    filtro_persona_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.filtro_persona_id_seq OWNED BY public.filtro_persona.id;
          public          postgres    false    243         �            1259    16489    filtro_persona_id_tabla_seq    SEQUENCE     �   CREATE SEQUENCE public.filtro_persona_id_tabla_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.filtro_persona_id_tabla_seq;
       public          postgres    false    3    240         �           0    0    filtro_persona_id_tabla_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.filtro_persona_id_tabla_seq OWNED BY public.filtro_persona.id_tabla;
          public          postgres    false    244         �            1259    16491    nacionalidad    TABLE     |   CREATE TABLE public.nacionalidad (
    id bigint NOT NULL,
    nombre character varying(30),
    id_pais bigint NOT NULL
);
     DROP TABLE public.nacionalidad;
       public         heap    postgres    false    3         �            1259    16494    nacionalidad_id_pais_seq    SEQUENCE     �   CREATE SEQUENCE public.nacionalidad_id_pais_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.nacionalidad_id_pais_seq;
       public          postgres    false    3    245         �           0    0    nacionalidad_id_pais_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.nacionalidad_id_pais_seq OWNED BY public.nacionalidad.id_pais;
          public          postgres    false    246         �            1259    16496    nacionalidad_id_seq    SEQUENCE     |   CREATE SEQUENCE public.nacionalidad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.nacionalidad_id_seq;
       public          postgres    false    245    3         �           0    0    nacionalidad_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.nacionalidad_id_seq OWNED BY public.nacionalidad.id;
          public          postgres    false    247         �            1259    16498    nivel_estudios    TABLE     a   CREATE TABLE public.nivel_estudios (
    id bigint NOT NULL,
    nombre character varying(30)
);
 "   DROP TABLE public.nivel_estudios;
       public         heap    postgres    false    3         �            1259    16501    nivel_estudios_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.nivel_estudios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.nivel_estudios_id_seq;
       public          postgres    false    3    248         �           0    0    nivel_estudios_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.nivel_estudios_id_seq OWNED BY public.nivel_estudios.id;
          public          postgres    false    249         �            1259    16503 	   ocupacion    TABLE     \   CREATE TABLE public.ocupacion (
    id bigint NOT NULL,
    nombre character varying(30)
);
    DROP TABLE public.ocupacion;
       public         heap    postgres    false    3         �            1259    16506    ocupacion_id_seq    SEQUENCE     y   CREATE SEQUENCE public.ocupacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.ocupacion_id_seq;
       public          postgres    false    3    250         �           0    0    ocupacion_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.ocupacion_id_seq OWNED BY public.ocupacion.id;
          public          postgres    false    251         �            1259    16508    pais    TABLE     W   CREATE TABLE public.pais (
    id bigint NOT NULL,
    nombre character varying(30)
);
    DROP TABLE public.pais;
       public         heap    postgres    false    3         �            1259    16511    pais_id_seq    SEQUENCE     t   CREATE SEQUENCE public.pais_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.pais_id_seq;
       public          postgres    false    252    3         �           0    0    pais_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.pais_id_seq OWNED BY public.pais.id;
          public          postgres    false    253         �            1259    16513    persona    TABLE     �  CREATE TABLE public.persona (
    id bigint NOT NULL,
    identificacion character varying(20),
    nombres_apellidos character varying(50),
    telefono character varying(10),
    licencia character varying(15),
    id_tipo_licencia bigint,
    id_nacionalidad bigint,
    id_nivel_estudios bigint,
    id_profesion bigint,
    id_ocupacion bigint,
    informacion_adicional character varying(120)
);
    DROP TABLE public.persona;
       public         heap    postgres    false    3         �            1259    16516    persona_id_nacionalidad_seq    SEQUENCE     �   CREATE SEQUENCE public.persona_id_nacionalidad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.persona_id_nacionalidad_seq;
       public          postgres    false    254    3         �           0    0    persona_id_nacionalidad_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.persona_id_nacionalidad_seq OWNED BY public.persona.id_nacionalidad;
          public          postgres    false    255                     1259    16518    persona_id_nivel_estudios_seq    SEQUENCE     �   CREATE SEQUENCE public.persona_id_nivel_estudios_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.persona_id_nivel_estudios_seq;
       public          postgres    false    254    3         �           0    0    persona_id_nivel_estudios_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.persona_id_nivel_estudios_seq OWNED BY public.persona.id_nivel_estudios;
          public          postgres    false    256                    1259    16520    persona_id_ocupacion_seq    SEQUENCE     �   CREATE SEQUENCE public.persona_id_ocupacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.persona_id_ocupacion_seq;
       public          postgres    false    3    254         �           0    0    persona_id_ocupacion_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.persona_id_ocupacion_seq OWNED BY public.persona.id_ocupacion;
          public          postgres    false    257                    1259    16522    persona_id_profesion_seq    SEQUENCE     �   CREATE SEQUENCE public.persona_id_profesion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.persona_id_profesion_seq;
       public          postgres    false    254    3         �           0    0    persona_id_profesion_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.persona_id_profesion_seq OWNED BY public.persona.id_profesion;
          public          postgres    false    258                    1259    16524    persona_id_seq    SEQUENCE     w   CREATE SEQUENCE public.persona_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.persona_id_seq;
       public          postgres    false    3    254         �           0    0    persona_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.persona_id_seq OWNED BY public.persona.id;
          public          postgres    false    259                    1259    16526    persona_id_tipo_licencia_seq    SEQUENCE     �   CREATE SEQUENCE public.persona_id_tipo_licencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.persona_id_tipo_licencia_seq;
       public          postgres    false    3    254         �           0    0    persona_id_tipo_licencia_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.persona_id_tipo_licencia_seq OWNED BY public.persona.id_tipo_licencia;
          public          postgres    false    260                    1259    16528 	   profesion    TABLE     �   CREATE TABLE public.profesion (
    id bigint NOT NULL,
    nombre character varying(60),
    id_nivel_estudios bigint NOT NULL
);
    DROP TABLE public.profesion;
       public         heap    postgres    false    3                    1259    16531    profesion_id_nivel_estudios_seq    SEQUENCE     �   CREATE SEQUENCE public.profesion_id_nivel_estudios_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.profesion_id_nivel_estudios_seq;
       public          postgres    false    261    3         �           0    0    profesion_id_nivel_estudios_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.profesion_id_nivel_estudios_seq OWNED BY public.profesion.id_nivel_estudios;
          public          postgres    false    262                    1259    16533    profesion_id_seq    SEQUENCE     y   CREATE SEQUENCE public.profesion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.profesion_id_seq;
       public          postgres    false    3    261         �           0    0    profesion_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.profesion_id_seq OWNED BY public.profesion.id;
          public          postgres    false    263                    1259    16535    tipo_configuracion    TABLE     e   CREATE TABLE public.tipo_configuracion (
    id bigint NOT NULL,
    nombre character varying(30)
);
 &   DROP TABLE public.tipo_configuracion;
       public         heap    postgres    false    3         	           1259    16538    tipo_configuracion_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tipo_configuracion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.tipo_configuracion_id_seq;
       public          postgres    false    264    3         �           0    0    tipo_configuracion_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.tipo_configuracion_id_seq OWNED BY public.tipo_configuracion.id;
          public          postgres    false    265         
           1259    16540    tipo_licencia    TABLE     `   CREATE TABLE public.tipo_licencia (
    id bigint NOT NULL,
    nombre character varying(30)
);
 !   DROP TABLE public.tipo_licencia;
       public         heap    postgres    false    3                    1259    16543    tipo_licencia_id_seq    SEQUENCE     }   CREATE SEQUENCE public.tipo_licencia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.tipo_licencia_id_seq;
       public          postgres    false    266    3         �           0    0    tipo_licencia_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.tipo_licencia_id_seq OWNED BY public.tipo_licencia.id;
          public          postgres    false    267                    1259    16545 	   tipo_pago    TABLE     \   CREATE TABLE public.tipo_pago (
    id bigint NOT NULL,
    nombre character varying(30)
);
    DROP TABLE public.tipo_pago;
       public         heap    postgres    false    3                    1259    16548    tipo_pago_id_seq    SEQUENCE     y   CREATE SEQUENCE public.tipo_pago_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.tipo_pago_id_seq;
       public          postgres    false    3    268         �           0    0    tipo_pago_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.tipo_pago_id_seq OWNED BY public.tipo_pago.id;
          public          postgres    false    269                    1259    16550    tipo_vehiculo    TABLE     `   CREATE TABLE public.tipo_vehiculo (
    id bigint NOT NULL,
    nombre character varying(30)
);
 !   DROP TABLE public.tipo_vehiculo;
       public         heap    postgres    false    3                    1259    16553    tipo_vehiculo_id_seq    SEQUENCE     }   CREATE SEQUENCE public.tipo_vehiculo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.tipo_vehiculo_id_seq;
       public          postgres    false    3    270         �           0    0    tipo_vehiculo_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.tipo_vehiculo_id_seq OWNED BY public.tipo_vehiculo.id;
          public          postgres    false    271                    1259    16555    trabajo    TABLE     �   CREATE TABLE public.trabajo (
    id bigint NOT NULL,
    cargo character varying(50),
    id_persona bigint NOT NULL,
    id_ocupacion bigint NOT NULL,
    id_empresa bigint NOT NULL,
    id_direccion bigint NOT NULL
);
    DROP TABLE public.trabajo;
       public         heap    postgres    false    3                    1259    16558    trabajo_id_direccion_seq    SEQUENCE     �   CREATE SEQUENCE public.trabajo_id_direccion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.trabajo_id_direccion_seq;
       public          postgres    false    3    272         �           0    0    trabajo_id_direccion_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.trabajo_id_direccion_seq OWNED BY public.trabajo.id_direccion;
          public          postgres    false    273                    1259    16560    trabajo_id_empresa_seq    SEQUENCE        CREATE SEQUENCE public.trabajo_id_empresa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.trabajo_id_empresa_seq;
       public          postgres    false    3    272         �           0    0    trabajo_id_empresa_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.trabajo_id_empresa_seq OWNED BY public.trabajo.id_empresa;
          public          postgres    false    274                    1259    16562    trabajo_id_ocupacion_seq    SEQUENCE     �   CREATE SEQUENCE public.trabajo_id_ocupacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.trabajo_id_ocupacion_seq;
       public          postgres    false    3    272         �           0    0    trabajo_id_ocupacion_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.trabajo_id_ocupacion_seq OWNED BY public.trabajo.id_ocupacion;
          public          postgres    false    275                    1259    16564    trabajo_id_persona_seq    SEQUENCE        CREATE SEQUENCE public.trabajo_id_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.trabajo_id_persona_seq;
       public          postgres    false    3    272         �           0    0    trabajo_id_persona_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.trabajo_id_persona_seq OWNED BY public.trabajo.id_persona;
          public          postgres    false    276                    1259    16566    trabajo_id_seq    SEQUENCE     w   CREATE SEQUENCE public.trabajo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.trabajo_id_seq;
       public          postgres    false    272    3         �           0    0    trabajo_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.trabajo_id_seq OWNED BY public.trabajo.id;
          public          postgres    false    277                    1259    16568 	   ubicacion    TABLE     w   CREATE TABLE public.ubicacion (
    id bigint NOT NULL,
    latitud double precision,
    longitud double precision
);
    DROP TABLE public.ubicacion;
       public         heap    postgres    false    3                    1259    16571    ubicacion_id_seq    SEQUENCE     y   CREATE SEQUENCE public.ubicacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.ubicacion_id_seq;
       public          postgres    false    3    278         �           0    0    ubicacion_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.ubicacion_id_seq OWNED BY public.ubicacion.id;
          public          postgres    false    279                    1259    16573    usuario    TABLE       CREATE TABLE public.usuario (
    id bigint NOT NULL,
    usuario character varying(20),
    correo character varying(20),
    contrasenia character varying(12),
    foto bytea,
    calificacion integer,
    estado integer,
    id_persona bigint,
    token character varying(200)
);
    DROP TABLE public.usuario;
       public         heap    postgres    false    3                    1259    16579    usuario_ayuda_favor    TABLE     �   CREATE TABLE public.usuario_ayuda_favor (
    id bigint NOT NULL,
    id_usuario bigint NOT NULL,
    id_favor bigint NOT NULL,
    estado_aceptacion character varying(20)
);
 '   DROP TABLE public.usuario_ayuda_favor;
       public         heap    postgres    false    3                    1259    16582     usuario_ayuda_favor_id_favor_seq    SEQUENCE     �   CREATE SEQUENCE public.usuario_ayuda_favor_id_favor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.usuario_ayuda_favor_id_favor_seq;
       public          postgres    false    3    281         �           0    0     usuario_ayuda_favor_id_favor_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.usuario_ayuda_favor_id_favor_seq OWNED BY public.usuario_ayuda_favor.id_favor;
          public          postgres    false    282                    1259    16584    usuario_ayuda_favor_id_seq    SEQUENCE     �   CREATE SEQUENCE public.usuario_ayuda_favor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.usuario_ayuda_favor_id_seq;
       public          postgres    false    3    281         �           0    0    usuario_ayuda_favor_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.usuario_ayuda_favor_id_seq OWNED BY public.usuario_ayuda_favor.id;
          public          postgres    false    283                    1259    16586 "   usuario_ayuda_favor_id_usuario_seq    SEQUENCE     �   CREATE SEQUENCE public.usuario_ayuda_favor_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.usuario_ayuda_favor_id_usuario_seq;
       public          postgres    false    281    3         �           0    0 "   usuario_ayuda_favor_id_usuario_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.usuario_ayuda_favor_id_usuario_seq OWNED BY public.usuario_ayuda_favor.id_usuario;
          public          postgres    false    284                    1259    16588    usuario_id_persona_seq    SEQUENCE        CREATE SEQUENCE public.usuario_id_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.usuario_id_persona_seq;
       public          postgres    false    3    280         �           0    0    usuario_id_persona_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.usuario_id_persona_seq OWNED BY public.usuario.id_persona;
          public          postgres    false    285                    1259    16590    usuario_id_seq    SEQUENCE     w   CREATE SEQUENCE public.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.usuario_id_seq;
       public          postgres    false    280    3         �           0    0    usuario_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.usuario_id_seq OWNED BY public.usuario.id;
          public          postgres    false    286                    1259    16592    vehiculo    TABLE     �   CREATE TABLE public.vehiculo (
    id bigint NOT NULL,
    nombre character varying(30),
    id_tipo_vehiculo bigint NOT NULL
);
    DROP TABLE public.vehiculo;
       public         heap    postgres    false    3                     1259    16595    vehiculo_id_seq    SEQUENCE     x   CREATE SEQUENCE public.vehiculo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.vehiculo_id_seq;
       public          postgres    false    287    3         �           0    0    vehiculo_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.vehiculo_id_seq OWNED BY public.vehiculo.id;
          public          postgres    false    288         !           1259    16597    vehiculo_id_tipo_vehiculo_seq    SEQUENCE     �   CREATE SEQUENCE public.vehiculo_id_tipo_vehiculo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.vehiculo_id_tipo_vehiculo_seq;
       public          postgres    false    3    287         �           0    0    vehiculo_id_tipo_vehiculo_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.vehiculo_id_tipo_vehiculo_seq OWNED BY public.vehiculo.id_tipo_vehiculo;
          public          postgres    false    289         "           1259    16599    vehiculo_persona    TABLE     �   CREATE TABLE public.vehiculo_persona (
    id bigint NOT NULL,
    id_vehiculo bigint NOT NULL,
    id_persona bigint NOT NULL
);
 $   DROP TABLE public.vehiculo_persona;
       public         heap    postgres    false    3         #           1259    16602    vehiculo_persona_id_persona_seq    SEQUENCE     �   CREATE SEQUENCE public.vehiculo_persona_id_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.vehiculo_persona_id_persona_seq;
       public          postgres    false    290    3         �           0    0    vehiculo_persona_id_persona_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.vehiculo_persona_id_persona_seq OWNED BY public.vehiculo_persona.id_persona;
          public          postgres    false    291         $           1259    16604    vehiculo_persona_id_seq    SEQUENCE     �   CREATE SEQUENCE public.vehiculo_persona_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.vehiculo_persona_id_seq;
       public          postgres    false    290    3         �           0    0    vehiculo_persona_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.vehiculo_persona_id_seq OWNED BY public.vehiculo_persona.id;
          public          postgres    false    292         %           1259    16606     vehiculo_persona_id_vehiculo_seq    SEQUENCE     �   CREATE SEQUENCE public.vehiculo_persona_id_vehiculo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.vehiculo_persona_id_vehiculo_seq;
       public          postgres    false    3    290         �           0    0     vehiculo_persona_id_vehiculo_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.vehiculo_persona_id_vehiculo_seq OWNED BY public.vehiculo_persona.id_vehiculo;
          public          postgres    false    293         �           2604    17046    calificacion id    DEFAULT     r   ALTER TABLE ONLY public.calificacion ALTER COLUMN id SET DEFAULT nextval('public.calificacion_id_seq'::regclass);
 >   ALTER TABLE public.calificacion ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    204    202         �           2604    17047    calificacion id_usuario    DEFAULT     �   ALTER TABLE ONLY public.calificacion ALTER COLUMN id_usuario SET DEFAULT nextval('public.calificacion_id_usuario_seq'::regclass);
 F   ALTER TABLE public.calificacion ALTER COLUMN id_usuario DROP DEFAULT;
       public          postgres    false    205    202         �           2604    17048    calificacion id_favor    DEFAULT     ~   ALTER TABLE ONLY public.calificacion ALTER COLUMN id_favor SET DEFAULT nextval('public.calificacion_id_favor_seq'::regclass);
 D   ALTER TABLE public.calificacion ALTER COLUMN id_favor DROP DEFAULT;
       public          postgres    false    203    202         �           2604    17049    categoria_filtro id    DEFAULT     z   ALTER TABLE ONLY public.categoria_filtro ALTER COLUMN id SET DEFAULT nextval('public.categoria_filtro_id_seq'::regclass);
 B   ALTER TABLE public.categoria_filtro ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    206         �           2604    17050 	   ciudad id    DEFAULT     f   ALTER TABLE ONLY public.ciudad ALTER COLUMN id SET DEFAULT nextval('public.ciudad_id_seq'::regclass);
 8   ALTER TABLE public.ciudad ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    210    208         �           2604    17051    ciudad id_pais    DEFAULT     p   ALTER TABLE ONLY public.ciudad ALTER COLUMN id_pais SET DEFAULT nextval('public.ciudad_id_pais_seq'::regclass);
 =   ALTER TABLE public.ciudad ALTER COLUMN id_pais DROP DEFAULT;
       public          postgres    false    209    208         �           2604    17052    detalle_tipo_pago id    DEFAULT     |   ALTER TABLE ONLY public.detalle_tipo_pago ALTER COLUMN id SET DEFAULT nextval('public.detalle_tipo_pago_id_seq'::regclass);
 C   ALTER TABLE public.detalle_tipo_pago ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    212    211         �           2604    17053    detalle_tipo_pago id_tipo_pago    DEFAULT     �   ALTER TABLE ONLY public.detalle_tipo_pago ALTER COLUMN id_tipo_pago SET DEFAULT nextval('public.detalle_tipo_pago_id_tipo_pago_seq'::regclass);
 M   ALTER TABLE public.detalle_tipo_pago ALTER COLUMN id_tipo_pago DROP DEFAULT;
       public          postgres    false    213    211         �           2604    17054    direccion id    DEFAULT     l   ALTER TABLE ONLY public.direccion ALTER COLUMN id SET DEFAULT nextval('public.direccion_id_seq'::regclass);
 ;   ALTER TABLE public.direccion ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    217    214         �           2604    17055    direccion id_ciudad    DEFAULT     z   ALTER TABLE ONLY public.direccion ALTER COLUMN id_ciudad SET DEFAULT nextval('public.direccion_id_ciudad_seq'::regclass);
 B   ALTER TABLE public.direccion ALTER COLUMN id_ciudad DROP DEFAULT;
       public          postgres    false    215    214         �           2604    17056    direccion id_ubicacion    DEFAULT     �   ALTER TABLE ONLY public.direccion ALTER COLUMN id_ubicacion SET DEFAULT nextval('public.direccion_id_ubicacion_seq'::regclass);
 E   ALTER TABLE public.direccion ALTER COLUMN id_ubicacion DROP DEFAULT;
       public          postgres    false    218    214         �           2604    17057    direccion id_persona    DEFAULT     |   ALTER TABLE ONLY public.direccion ALTER COLUMN id_persona SET DEFAULT nextval('public.direccion_id_persona_seq'::regclass);
 C   ALTER TABLE public.direccion ALTER COLUMN id_persona DROP DEFAULT;
       public          postgres    false    216    214         �           2604    17058 
   empresa id    DEFAULT     h   ALTER TABLE ONLY public.empresa ALTER COLUMN id SET DEFAULT nextval('public.empresa_id_seq'::regclass);
 9   ALTER TABLE public.empresa ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    220    219         �           2604    17059    favor id    DEFAULT     d   ALTER TABLE ONLY public.favor ALTER COLUMN id SET DEFAULT nextval('public.favor_id_seq'::regclass);
 7   ALTER TABLE public.favor ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    224    221         �           2604    17060    favor id_direccion_favor    DEFAULT     �   ALTER TABLE ONLY public.favor ALTER COLUMN id_direccion_favor SET DEFAULT nextval('public.favor_id_direccion_favor_seq'::regclass);
 G   ALTER TABLE public.favor ALTER COLUMN id_direccion_favor DROP DEFAULT;
       public          postgres    false    223    221         �           2604    17061    favor id_detalle_tipo_pago    DEFAULT     �   ALTER TABLE ONLY public.favor ALTER COLUMN id_detalle_tipo_pago SET DEFAULT nextval('public.favor_id_detalle_tipo_pago_seq'::regclass);
 I   ALTER TABLE public.favor ALTER COLUMN id_detalle_tipo_pago DROP DEFAULT;
       public          postgres    false    222    221         �           2604    17062    favor id_usuario_solicita    DEFAULT     �   ALTER TABLE ONLY public.favor ALTER COLUMN id_usuario_solicita SET DEFAULT nextval('public.favor_id_usuario_solicita_seq'::regclass);
 H   ALTER TABLE public.favor ALTER COLUMN id_usuario_solicita DROP DEFAULT;
       public          postgres    false    226    221         �           2604    17063    favor id_usuario_realiza    DEFAULT     �   ALTER TABLE ONLY public.favor ALTER COLUMN id_usuario_realiza SET DEFAULT nextval('public.favor_id_usuario_realiza_seq'::regclass);
 G   ALTER TABLE public.favor ALTER COLUMN id_usuario_realiza DROP DEFAULT;
       public          postgres    false    225    221         �           2604    17064 	   filtro id    DEFAULT     f   ALTER TABLE ONLY public.filtro ALTER COLUMN id SET DEFAULT nextval('public.filtro_id_seq'::regclass);
 8   ALTER TABLE public.filtro ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    239    227         �           2604    17065    filtro_configuracion_usuario id    DEFAULT     �   ALTER TABLE ONLY public.filtro_configuracion_usuario ALTER COLUMN id SET DEFAULT nextval('public.filtro_configuracion_usuario_id_seq'::regclass);
 N   ALTER TABLE public.filtro_configuracion_usuario ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    230    228         �           2604    17066 &   filtro_configuracion_usuario id_filtro    DEFAULT     �   ALTER TABLE ONLY public.filtro_configuracion_usuario ALTER COLUMN id_filtro SET DEFAULT nextval('public.filtro_configuracion_usuario_id_filtro_seq'::regclass);
 U   ALTER TABLE public.filtro_configuracion_usuario ALTER COLUMN id_filtro DROP DEFAULT;
       public          postgres    false    229    228         �           2604    17067 2   filtro_configuracion_usuario id_tipo_configuracion    DEFAULT     �   ALTER TABLE ONLY public.filtro_configuracion_usuario ALTER COLUMN id_tipo_configuracion SET DEFAULT nextval('public.filtro_configuracion_usuario_id_tipo_configuracion_seq'::regclass);
 a   ALTER TABLE public.filtro_configuracion_usuario ALTER COLUMN id_tipo_configuracion DROP DEFAULT;
       public          postgres    false    232    228         �           2604    17068 '   filtro_configuracion_usuario id_usuario    DEFAULT     �   ALTER TABLE ONLY public.filtro_configuracion_usuario ALTER COLUMN id_usuario SET DEFAULT nextval('public.filtro_configuracion_usuario_id_usuario_seq'::regclass);
 V   ALTER TABLE public.filtro_configuracion_usuario ALTER COLUMN id_usuario DROP DEFAULT;
       public          postgres    false    233    228         �           2604    17069 %   filtro_configuracion_usuario id_tabla    DEFAULT     �   ALTER TABLE ONLY public.filtro_configuracion_usuario ALTER COLUMN id_tabla SET DEFAULT nextval('public.filtro_configuracion_usuario_id_tabla_seq'::regclass);
 T   ALTER TABLE public.filtro_configuracion_usuario ALTER COLUMN id_tabla DROP DEFAULT;
       public          postgres    false    231    228         �           2604    17070    filtro_favor id    DEFAULT     r   ALTER TABLE ONLY public.filtro_favor ALTER COLUMN id SET DEFAULT nextval('public.filtro_favor_id_seq'::regclass);
 >   ALTER TABLE public.filtro_favor ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    237    234         �           2604    17071    filtro_favor id_filtro    DEFAULT     �   ALTER TABLE ONLY public.filtro_favor ALTER COLUMN id_filtro SET DEFAULT nextval('public.filtro_favor_id_filtro_seq'::regclass);
 E   ALTER TABLE public.filtro_favor ALTER COLUMN id_filtro DROP DEFAULT;
       public          postgres    false    236    234         �           2604    17072    filtro_favor id_favor    DEFAULT     ~   ALTER TABLE ONLY public.filtro_favor ALTER COLUMN id_favor SET DEFAULT nextval('public.filtro_favor_id_favor_seq'::regclass);
 D   ALTER TABLE public.filtro_favor ALTER COLUMN id_favor DROP DEFAULT;
       public          postgres    false    235    234         �           2604    17073    filtro_favor id_tabla    DEFAULT     ~   ALTER TABLE ONLY public.filtro_favor ALTER COLUMN id_tabla SET DEFAULT nextval('public.filtro_favor_id_tabla_seq'::regclass);
 D   ALTER TABLE public.filtro_favor ALTER COLUMN id_tabla DROP DEFAULT;
       public          postgres    false    238    234         �           2604    17074    filtro_persona id    DEFAULT     v   ALTER TABLE ONLY public.filtro_persona ALTER COLUMN id SET DEFAULT nextval('public.filtro_persona_id_seq'::regclass);
 @   ALTER TABLE public.filtro_persona ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    243    240         �           2604    17075    filtro_persona id_filtro    DEFAULT     �   ALTER TABLE ONLY public.filtro_persona ALTER COLUMN id_filtro SET DEFAULT nextval('public.filtro_persona_id_filtro_seq'::regclass);
 G   ALTER TABLE public.filtro_persona ALTER COLUMN id_filtro DROP DEFAULT;
       public          postgres    false    241    240         �           2604    17076    filtro_persona id_persona    DEFAULT     �   ALTER TABLE ONLY public.filtro_persona ALTER COLUMN id_persona SET DEFAULT nextval('public.filtro_persona_id_persona_seq'::regclass);
 H   ALTER TABLE public.filtro_persona ALTER COLUMN id_persona DROP DEFAULT;
       public          postgres    false    242    240         �           2604    17077    filtro_persona id_tabla    DEFAULT     �   ALTER TABLE ONLY public.filtro_persona ALTER COLUMN id_tabla SET DEFAULT nextval('public.filtro_persona_id_tabla_seq'::regclass);
 F   ALTER TABLE public.filtro_persona ALTER COLUMN id_tabla DROP DEFAULT;
       public          postgres    false    244    240         �           2604    17078    nacionalidad id    DEFAULT     r   ALTER TABLE ONLY public.nacionalidad ALTER COLUMN id SET DEFAULT nextval('public.nacionalidad_id_seq'::regclass);
 >   ALTER TABLE public.nacionalidad ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    247    245         �           2604    17079    nacionalidad id_pais    DEFAULT     |   ALTER TABLE ONLY public.nacionalidad ALTER COLUMN id_pais SET DEFAULT nextval('public.nacionalidad_id_pais_seq'::regclass);
 C   ALTER TABLE public.nacionalidad ALTER COLUMN id_pais DROP DEFAULT;
       public          postgres    false    246    245         �           2604    17080    nivel_estudios id    DEFAULT     v   ALTER TABLE ONLY public.nivel_estudios ALTER COLUMN id SET DEFAULT nextval('public.nivel_estudios_id_seq'::regclass);
 @   ALTER TABLE public.nivel_estudios ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    249    248         �           2604    17081    ocupacion id    DEFAULT     l   ALTER TABLE ONLY public.ocupacion ALTER COLUMN id SET DEFAULT nextval('public.ocupacion_id_seq'::regclass);
 ;   ALTER TABLE public.ocupacion ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    251    250         �           2604    17082    pais id    DEFAULT     b   ALTER TABLE ONLY public.pais ALTER COLUMN id SET DEFAULT nextval('public.pais_id_seq'::regclass);
 6   ALTER TABLE public.pais ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    253    252         �           2604    17083 
   persona id    DEFAULT     h   ALTER TABLE ONLY public.persona ALTER COLUMN id SET DEFAULT nextval('public.persona_id_seq'::regclass);
 9   ALTER TABLE public.persona ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    259    254         �           2604    17084    persona id_tipo_licencia    DEFAULT     �   ALTER TABLE ONLY public.persona ALTER COLUMN id_tipo_licencia SET DEFAULT nextval('public.persona_id_tipo_licencia_seq'::regclass);
 G   ALTER TABLE public.persona ALTER COLUMN id_tipo_licencia DROP DEFAULT;
       public          postgres    false    260    254         �           2604    17085    persona id_nacionalidad    DEFAULT     �   ALTER TABLE ONLY public.persona ALTER COLUMN id_nacionalidad SET DEFAULT nextval('public.persona_id_nacionalidad_seq'::regclass);
 F   ALTER TABLE public.persona ALTER COLUMN id_nacionalidad DROP DEFAULT;
       public          postgres    false    255    254         �           2604    17086    persona id_nivel_estudios    DEFAULT     �   ALTER TABLE ONLY public.persona ALTER COLUMN id_nivel_estudios SET DEFAULT nextval('public.persona_id_nivel_estudios_seq'::regclass);
 H   ALTER TABLE public.persona ALTER COLUMN id_nivel_estudios DROP DEFAULT;
       public          postgres    false    256    254         �           2604    17087    persona id_profesion    DEFAULT     |   ALTER TABLE ONLY public.persona ALTER COLUMN id_profesion SET DEFAULT nextval('public.persona_id_profesion_seq'::regclass);
 C   ALTER TABLE public.persona ALTER COLUMN id_profesion DROP DEFAULT;
       public          postgres    false    258    254         �           2604    17088    persona id_ocupacion    DEFAULT     |   ALTER TABLE ONLY public.persona ALTER COLUMN id_ocupacion SET DEFAULT nextval('public.persona_id_ocupacion_seq'::regclass);
 C   ALTER TABLE public.persona ALTER COLUMN id_ocupacion DROP DEFAULT;
       public          postgres    false    257    254         �           2604    17089    profesion id    DEFAULT     l   ALTER TABLE ONLY public.profesion ALTER COLUMN id SET DEFAULT nextval('public.profesion_id_seq'::regclass);
 ;   ALTER TABLE public.profesion ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    263    261         �           2604    17090    profesion id_nivel_estudios    DEFAULT     �   ALTER TABLE ONLY public.profesion ALTER COLUMN id_nivel_estudios SET DEFAULT nextval('public.profesion_id_nivel_estudios_seq'::regclass);
 J   ALTER TABLE public.profesion ALTER COLUMN id_nivel_estudios DROP DEFAULT;
       public          postgres    false    262    261         �           2604    17091    tipo_configuracion id    DEFAULT     ~   ALTER TABLE ONLY public.tipo_configuracion ALTER COLUMN id SET DEFAULT nextval('public.tipo_configuracion_id_seq'::regclass);
 D   ALTER TABLE public.tipo_configuracion ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    265    264         �           2604    17092    tipo_licencia id    DEFAULT     t   ALTER TABLE ONLY public.tipo_licencia ALTER COLUMN id SET DEFAULT nextval('public.tipo_licencia_id_seq'::regclass);
 ?   ALTER TABLE public.tipo_licencia ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    267    266         �           2604    17093    tipo_pago id    DEFAULT     l   ALTER TABLE ONLY public.tipo_pago ALTER COLUMN id SET DEFAULT nextval('public.tipo_pago_id_seq'::regclass);
 ;   ALTER TABLE public.tipo_pago ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    269    268         �           2604    17094    tipo_vehiculo id    DEFAULT     t   ALTER TABLE ONLY public.tipo_vehiculo ALTER COLUMN id SET DEFAULT nextval('public.tipo_vehiculo_id_seq'::regclass);
 ?   ALTER TABLE public.tipo_vehiculo ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    271    270         �           2604    17095 
   trabajo id    DEFAULT     h   ALTER TABLE ONLY public.trabajo ALTER COLUMN id SET DEFAULT nextval('public.trabajo_id_seq'::regclass);
 9   ALTER TABLE public.trabajo ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    277    272         �           2604    17096    trabajo id_persona    DEFAULT     x   ALTER TABLE ONLY public.trabajo ALTER COLUMN id_persona SET DEFAULT nextval('public.trabajo_id_persona_seq'::regclass);
 A   ALTER TABLE public.trabajo ALTER COLUMN id_persona DROP DEFAULT;
       public          postgres    false    276    272         �           2604    17097    trabajo id_ocupacion    DEFAULT     |   ALTER TABLE ONLY public.trabajo ALTER COLUMN id_ocupacion SET DEFAULT nextval('public.trabajo_id_ocupacion_seq'::regclass);
 C   ALTER TABLE public.trabajo ALTER COLUMN id_ocupacion DROP DEFAULT;
       public          postgres    false    275    272         �           2604    17098    trabajo id_empresa    DEFAULT     x   ALTER TABLE ONLY public.trabajo ALTER COLUMN id_empresa SET DEFAULT nextval('public.trabajo_id_empresa_seq'::regclass);
 A   ALTER TABLE public.trabajo ALTER COLUMN id_empresa DROP DEFAULT;
       public          postgres    false    274    272         �           2604    17099    trabajo id_direccion    DEFAULT     |   ALTER TABLE ONLY public.trabajo ALTER COLUMN id_direccion SET DEFAULT nextval('public.trabajo_id_direccion_seq'::regclass);
 C   ALTER TABLE public.trabajo ALTER COLUMN id_direccion DROP DEFAULT;
       public          postgres    false    273    272         �           2604    17100    ubicacion id    DEFAULT     l   ALTER TABLE ONLY public.ubicacion ALTER COLUMN id SET DEFAULT nextval('public.ubicacion_id_seq'::regclass);
 ;   ALTER TABLE public.ubicacion ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    279    278         �           2604    17101 
   usuario id    DEFAULT     h   ALTER TABLE ONLY public.usuario ALTER COLUMN id SET DEFAULT nextval('public.usuario_id_seq'::regclass);
 9   ALTER TABLE public.usuario ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    286    280         �           2604    17102    usuario id_persona    DEFAULT     x   ALTER TABLE ONLY public.usuario ALTER COLUMN id_persona SET DEFAULT nextval('public.usuario_id_persona_seq'::regclass);
 A   ALTER TABLE public.usuario ALTER COLUMN id_persona DROP DEFAULT;
       public          postgres    false    285    280         �           2604    17103    usuario_ayuda_favor id    DEFAULT     �   ALTER TABLE ONLY public.usuario_ayuda_favor ALTER COLUMN id SET DEFAULT nextval('public.usuario_ayuda_favor_id_seq'::regclass);
 E   ALTER TABLE public.usuario_ayuda_favor ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    283    281         �           2604    17104    usuario_ayuda_favor id_usuario    DEFAULT     �   ALTER TABLE ONLY public.usuario_ayuda_favor ALTER COLUMN id_usuario SET DEFAULT nextval('public.usuario_ayuda_favor_id_usuario_seq'::regclass);
 M   ALTER TABLE public.usuario_ayuda_favor ALTER COLUMN id_usuario DROP DEFAULT;
       public          postgres    false    284    281         �           2604    17105    usuario_ayuda_favor id_favor    DEFAULT     �   ALTER TABLE ONLY public.usuario_ayuda_favor ALTER COLUMN id_favor SET DEFAULT nextval('public.usuario_ayuda_favor_id_favor_seq'::regclass);
 K   ALTER TABLE public.usuario_ayuda_favor ALTER COLUMN id_favor DROP DEFAULT;
       public          postgres    false    282    281         �           2604    17106    vehiculo id    DEFAULT     j   ALTER TABLE ONLY public.vehiculo ALTER COLUMN id SET DEFAULT nextval('public.vehiculo_id_seq'::regclass);
 :   ALTER TABLE public.vehiculo ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    288    287         �           2604    17107    vehiculo id_tipo_vehiculo    DEFAULT     �   ALTER TABLE ONLY public.vehiculo ALTER COLUMN id_tipo_vehiculo SET DEFAULT nextval('public.vehiculo_id_tipo_vehiculo_seq'::regclass);
 H   ALTER TABLE public.vehiculo ALTER COLUMN id_tipo_vehiculo DROP DEFAULT;
       public          postgres    false    289    287         �           2604    17108    vehiculo_persona id    DEFAULT     z   ALTER TABLE ONLY public.vehiculo_persona ALTER COLUMN id SET DEFAULT nextval('public.vehiculo_persona_id_seq'::regclass);
 B   ALTER TABLE public.vehiculo_persona ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    292    290         �           2604    17109    vehiculo_persona id_vehiculo    DEFAULT     �   ALTER TABLE ONLY public.vehiculo_persona ALTER COLUMN id_vehiculo SET DEFAULT nextval('public.vehiculo_persona_id_vehiculo_seq'::regclass);
 K   ALTER TABLE public.vehiculo_persona ALTER COLUMN id_vehiculo DROP DEFAULT;
       public          postgres    false    293    290         �           2604    17110    vehiculo_persona id_persona    DEFAULT     �   ALTER TABLE ONLY public.vehiculo_persona ALTER COLUMN id_persona SET DEFAULT nextval('public.vehiculo_persona_id_persona_seq'::regclass);
 J   ALTER TABLE public.vehiculo_persona ALTER COLUMN id_persona DROP DEFAULT;
       public          postgres    false    291    290                   0    16394    calificacion 
   TABLE DATA           N   COPY public.calificacion (id, calificacion, id_usuario, id_favor) FROM stdin;
    public          postgres    false    202       3591.dat           0    16403    categoria_filtro 
   TABLE DATA           6   COPY public.categoria_filtro (id, nombre) FROM stdin;
    public          postgres    false    206       3595.dat           0    16408    ciudad 
   TABLE DATA           5   COPY public.ciudad (id, nombre, id_pais) FROM stdin;
    public          postgres    false    208       3597.dat           0    16415    detalle_tipo_pago 
   TABLE DATA           Q   COPY public.detalle_tipo_pago (id, id_tipo_pago, valor, descripcion) FROM stdin;
    public          postgres    false    211       3600.dat           0    16422 	   direccion 
   TABLE DATA           �   COPY public.direccion (id, calle_principal, calle_secundaria, barrio, numero_casa, referencia, id_ciudad, id_ubicacion, id_persona) FROM stdin;
    public          postgres    false    214       3603.dat           0    16433    empresa 
   TABLE DATA           -   COPY public.empresa (id, nombre) FROM stdin;
    public          postgres    false    219       3608.dat           0    16438    favor 
   TABLE DATA           �   COPY public.favor (id, titulo, descripcion, estado, fecha_solicita, fecha_realiza, id_direccion_favor, id_detalle_tipo_pago, id_usuario_solicita, id_usuario_realiza) FROM stdin;
    public          postgres    false    221       3610.dat            0    16451    filtro 
   TABLE DATA           N   COPY public.filtro (id, nombre, descripcion, id_categoria_filtro) FROM stdin;
    public          postgres    false    227       3616.dat !          0    16454    filtro_configuracion_usuario 
   TABLE DATA           �   COPY public.filtro_configuracion_usuario (id, id_filtro, id_tipo_configuracion, id_usuario, valor_string, valor_int, id_tabla, estado) FROM stdin;
    public          postgres    false    228       3617.dat '          0    16467    filtro_favor 
   TABLE DATA           j   COPY public.filtro_favor (id, id_filtro, id_favor, valor_string, valor_int, id_tabla, estado) FROM stdin;
    public          postgres    false    234       3623.dat -          0    16480    filtro_persona 
   TABLE DATA           n   COPY public.filtro_persona (id, id_filtro, id_persona, valor_string, valor_int, id_tabla, estado) FROM stdin;
    public          postgres    false    240       3629.dat 2          0    16491    nacionalidad 
   TABLE DATA           ;   COPY public.nacionalidad (id, nombre, id_pais) FROM stdin;
    public          postgres    false    245       3634.dat 5          0    16498    nivel_estudios 
   TABLE DATA           4   COPY public.nivel_estudios (id, nombre) FROM stdin;
    public          postgres    false    248       3637.dat 7          0    16503 	   ocupacion 
   TABLE DATA           /   COPY public.ocupacion (id, nombre) FROM stdin;
    public          postgres    false    250       3639.dat 9          0    16508    pais 
   TABLE DATA           *   COPY public.pais (id, nombre) FROM stdin;
    public          postgres    false    252       3641.dat ;          0    16513    persona 
   TABLE DATA           �   COPY public.persona (id, identificacion, nombres_apellidos, telefono, licencia, id_tipo_licencia, id_nacionalidad, id_nivel_estudios, id_profesion, id_ocupacion, informacion_adicional) FROM stdin;
    public          postgres    false    254       3643.dat B          0    16528 	   profesion 
   TABLE DATA           B   COPY public.profesion (id, nombre, id_nivel_estudios) FROM stdin;
    public          postgres    false    261       3650.dat E          0    16535    tipo_configuracion 
   TABLE DATA           8   COPY public.tipo_configuracion (id, nombre) FROM stdin;
    public          postgres    false    264       3653.dat G          0    16540    tipo_licencia 
   TABLE DATA           3   COPY public.tipo_licencia (id, nombre) FROM stdin;
    public          postgres    false    266       3655.dat I          0    16545 	   tipo_pago 
   TABLE DATA           /   COPY public.tipo_pago (id, nombre) FROM stdin;
    public          postgres    false    268       3657.dat K          0    16550    tipo_vehiculo 
   TABLE DATA           3   COPY public.tipo_vehiculo (id, nombre) FROM stdin;
    public          postgres    false    270       3659.dat M          0    16555    trabajo 
   TABLE DATA           `   COPY public.trabajo (id, cargo, id_persona, id_ocupacion, id_empresa, id_direccion) FROM stdin;
    public          postgres    false    272       3661.dat S          0    16568 	   ubicacion 
   TABLE DATA           :   COPY public.ubicacion (id, latitud, longitud) FROM stdin;
    public          postgres    false    278       3667.dat U          0    16573    usuario 
   TABLE DATA           r   COPY public.usuario (id, usuario, correo, contrasenia, foto, calificacion, estado, id_persona, token) FROM stdin;
    public          postgres    false    280       3669.dat V          0    16579    usuario_ayuda_favor 
   TABLE DATA           Z   COPY public.usuario_ayuda_favor (id, id_usuario, id_favor, estado_aceptacion) FROM stdin;
    public          postgres    false    281       3670.dat \          0    16592    vehiculo 
   TABLE DATA           @   COPY public.vehiculo (id, nombre, id_tipo_vehiculo) FROM stdin;
    public          postgres    false    287       3676.dat _          0    16599    vehiculo_persona 
   TABLE DATA           G   COPY public.vehiculo_persona (id, id_vehiculo, id_persona) FROM stdin;
    public          postgres    false    290       3679.dat �           0    0    calificacion_id_favor_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.calificacion_id_favor_seq', 1, false);
          public          postgres    false    203         �           0    0    calificacion_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.calificacion_id_seq', 1, false);
          public          postgres    false    204         �           0    0    calificacion_id_usuario_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.calificacion_id_usuario_seq', 1, false);
          public          postgres    false    205         �           0    0    categoria_filtro_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.categoria_filtro_id_seq', 1, false);
          public          postgres    false    207         �           0    0    ciudad_id_pais_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.ciudad_id_pais_seq', 1, false);
          public          postgres    false    209         �           0    0    ciudad_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.ciudad_id_seq', 1, false);
          public          postgres    false    210         �           0    0    detalle_tipo_pago_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.detalle_tipo_pago_id_seq', 1, false);
          public          postgres    false    212         �           0    0 "   detalle_tipo_pago_id_tipo_pago_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.detalle_tipo_pago_id_tipo_pago_seq', 1, false);
          public          postgres    false    213         �           0    0    direccion_id_ciudad_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.direccion_id_ciudad_seq', 1, false);
          public          postgres    false    215         �           0    0    direccion_id_persona_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.direccion_id_persona_seq', 1, false);
          public          postgres    false    216         �           0    0    direccion_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.direccion_id_seq', 1, false);
          public          postgres    false    217         �           0    0    direccion_id_ubicacion_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.direccion_id_ubicacion_seq', 1, false);
          public          postgres    false    218         �           0    0    empresa_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.empresa_id_seq', 1, false);
          public          postgres    false    220         �           0    0    favor_id_detalle_tipo_pago_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.favor_id_detalle_tipo_pago_seq', 1, false);
          public          postgres    false    222         �           0    0    favor_id_direccion_favor_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.favor_id_direccion_favor_seq', 1, false);
          public          postgres    false    223         �           0    0    favor_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.favor_id_seq', 1, false);
          public          postgres    false    224         �           0    0    favor_id_usuario_realiza_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.favor_id_usuario_realiza_seq', 1, false);
          public          postgres    false    225         �           0    0    favor_id_usuario_solicita_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.favor_id_usuario_solicita_seq', 1, false);
          public          postgres    false    226         �           0    0 *   filtro_configuracion_usuario_id_filtro_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.filtro_configuracion_usuario_id_filtro_seq', 1, false);
          public          postgres    false    229         �           0    0 #   filtro_configuracion_usuario_id_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.filtro_configuracion_usuario_id_seq', 1, false);
          public          postgres    false    230         �           0    0 )   filtro_configuracion_usuario_id_tabla_seq    SEQUENCE SET     X   SELECT pg_catalog.setval('public.filtro_configuracion_usuario_id_tabla_seq', 1, false);
          public          postgres    false    231         �           0    0 6   filtro_configuracion_usuario_id_tipo_configuracion_seq    SEQUENCE SET     e   SELECT pg_catalog.setval('public.filtro_configuracion_usuario_id_tipo_configuracion_seq', 1, false);
          public          postgres    false    232         �           0    0 +   filtro_configuracion_usuario_id_usuario_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.filtro_configuracion_usuario_id_usuario_seq', 1, false);
          public          postgres    false    233         �           0    0    filtro_favor_id_favor_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.filtro_favor_id_favor_seq', 1, false);
          public          postgres    false    235         �           0    0    filtro_favor_id_filtro_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.filtro_favor_id_filtro_seq', 1, false);
          public          postgres    false    236         �           0    0    filtro_favor_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.filtro_favor_id_seq', 1, false);
          public          postgres    false    237         �           0    0    filtro_favor_id_tabla_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.filtro_favor_id_tabla_seq', 1, false);
          public          postgres    false    238         �           0    0    filtro_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.filtro_id_seq', 1, false);
          public          postgres    false    239         �           0    0    filtro_persona_id_filtro_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.filtro_persona_id_filtro_seq', 1, false);
          public          postgres    false    241         �           0    0    filtro_persona_id_persona_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.filtro_persona_id_persona_seq', 1, false);
          public          postgres    false    242         �           0    0    filtro_persona_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.filtro_persona_id_seq', 1, false);
          public          postgres    false    243         �           0    0    filtro_persona_id_tabla_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.filtro_persona_id_tabla_seq', 1, false);
          public          postgres    false    244         �           0    0    nacionalidad_id_pais_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.nacionalidad_id_pais_seq', 1, false);
          public          postgres    false    246         �           0    0    nacionalidad_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.nacionalidad_id_seq', 1, false);
          public          postgres    false    247         �           0    0    nivel_estudios_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.nivel_estudios_id_seq', 1, false);
          public          postgres    false    249         �           0    0    ocupacion_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.ocupacion_id_seq', 1, false);
          public          postgres    false    251         �           0    0    pais_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.pais_id_seq', 4, true);
          public          postgres    false    253         �           0    0    persona_id_nacionalidad_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.persona_id_nacionalidad_seq', 1, false);
          public          postgres    false    255         �           0    0    persona_id_nivel_estudios_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.persona_id_nivel_estudios_seq', 1, false);
          public          postgres    false    256         �           0    0    persona_id_ocupacion_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.persona_id_ocupacion_seq', 1, false);
          public          postgres    false    257         �           0    0    persona_id_profesion_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.persona_id_profesion_seq', 1, false);
          public          postgres    false    258         �           0    0    persona_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.persona_id_seq', 18, true);
          public          postgres    false    259         �           0    0    persona_id_tipo_licencia_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.persona_id_tipo_licencia_seq', 1, false);
          public          postgres    false    260         �           0    0    profesion_id_nivel_estudios_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.profesion_id_nivel_estudios_seq', 1, false);
          public          postgres    false    262         �           0    0    profesion_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.profesion_id_seq', 1, false);
          public          postgres    false    263         �           0    0    tipo_configuracion_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.tipo_configuracion_id_seq', 1, false);
          public          postgres    false    265         �           0    0    tipo_licencia_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.tipo_licencia_id_seq', 1, false);
          public          postgres    false    267         �           0    0    tipo_pago_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.tipo_pago_id_seq', 1, false);
          public          postgres    false    269         �           0    0    tipo_vehiculo_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.tipo_vehiculo_id_seq', 1, false);
          public          postgres    false    271         �           0    0    trabajo_id_direccion_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.trabajo_id_direccion_seq', 1, false);
          public          postgres    false    273         �           0    0    trabajo_id_empresa_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.trabajo_id_empresa_seq', 1, false);
          public          postgres    false    274         �           0    0    trabajo_id_ocupacion_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.trabajo_id_ocupacion_seq', 1, false);
          public          postgres    false    275         �           0    0    trabajo_id_persona_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.trabajo_id_persona_seq', 1, false);
          public          postgres    false    276         �           0    0    trabajo_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.trabajo_id_seq', 1, false);
          public          postgres    false    277         �           0    0    ubicacion_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.ubicacion_id_seq', 1, false);
          public          postgres    false    279         �           0    0     usuario_ayuda_favor_id_favor_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.usuario_ayuda_favor_id_favor_seq', 1, false);
          public          postgres    false    282         �           0    0    usuario_ayuda_favor_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.usuario_ayuda_favor_id_seq', 1, false);
          public          postgres    false    283         �           0    0 "   usuario_ayuda_favor_id_usuario_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.usuario_ayuda_favor_id_usuario_seq', 1, false);
          public          postgres    false    284         �           0    0    usuario_id_persona_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.usuario_id_persona_seq', 1, false);
          public          postgres    false    285         �           0    0    usuario_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.usuario_id_seq', 16, true);
          public          postgres    false    286         �           0    0    vehiculo_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.vehiculo_id_seq', 1, false);
          public          postgres    false    288         �           0    0    vehiculo_id_tipo_vehiculo_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.vehiculo_id_tipo_vehiculo_seq', 1, false);
          public          postgres    false    289         �           0    0    vehiculo_persona_id_persona_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.vehiculo_persona_id_persona_seq', 1, false);
          public          postgres    false    291         �           0    0    vehiculo_persona_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.vehiculo_persona_id_seq', 1, false);
          public          postgres    false    292         �           0    0     vehiculo_persona_id_vehiculo_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.vehiculo_persona_id_vehiculo_seq', 1, false);
          public          postgres    false    293         �           2606    16674    calificacion calificacion_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.calificacion
    ADD CONSTRAINT calificacion_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.calificacion DROP CONSTRAINT calificacion_pkey;
       public            postgres    false    202         �           2606    16676 ,   categoria_filtro categoria_filtro_nombre_key 
   CONSTRAINT     i   ALTER TABLE ONLY public.categoria_filtro
    ADD CONSTRAINT categoria_filtro_nombre_key UNIQUE (nombre);
 V   ALTER TABLE ONLY public.categoria_filtro DROP CONSTRAINT categoria_filtro_nombre_key;
       public            postgres    false    206         �           2606    16678 &   categoria_filtro categoria_filtro_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.categoria_filtro
    ADD CONSTRAINT categoria_filtro_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.categoria_filtro DROP CONSTRAINT categoria_filtro_pkey;
       public            postgres    false    206         �           2606    16680    ciudad ciudad_nombre_key 
   CONSTRAINT     U   ALTER TABLE ONLY public.ciudad
    ADD CONSTRAINT ciudad_nombre_key UNIQUE (nombre);
 B   ALTER TABLE ONLY public.ciudad DROP CONSTRAINT ciudad_nombre_key;
       public            postgres    false    208         �           2606    16682    ciudad ciudad_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.ciudad
    ADD CONSTRAINT ciudad_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.ciudad DROP CONSTRAINT ciudad_pkey;
       public            postgres    false    208         �           2606    16684    direccion direccion_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.direccion
    ADD CONSTRAINT direccion_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.direccion DROP CONSTRAINT direccion_pkey;
       public            postgres    false    214         �           2606    16686    empresa empresa_nombre_key 
   CONSTRAINT     W   ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT empresa_nombre_key UNIQUE (nombre);
 D   ALTER TABLE ONLY public.empresa DROP CONSTRAINT empresa_nombre_key;
       public            postgres    false    219         �           2606    16688    empresa empresa_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT empresa_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.empresa DROP CONSTRAINT empresa_pkey;
       public            postgres    false    219         �           2606    16690    favor favor_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.favor
    ADD CONSTRAINT favor_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.favor DROP CONSTRAINT favor_pkey;
       public            postgres    false    221                    2606    16692 >   filtro_configuracion_usuario filtro_configuracion_usuario_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.filtro_configuracion_usuario
    ADD CONSTRAINT filtro_configuracion_usuario_pkey PRIMARY KEY (id);
 h   ALTER TABLE ONLY public.filtro_configuracion_usuario DROP CONSTRAINT filtro_configuracion_usuario_pkey;
       public            postgres    false    228         	           2606    16694    filtro_favor filtro_favor_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.filtro_favor
    ADD CONSTRAINT filtro_favor_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.filtro_favor DROP CONSTRAINT filtro_favor_pkey;
       public            postgres    false    234                    2606    16696    filtro filtro_nombre_key 
   CONSTRAINT     U   ALTER TABLE ONLY public.filtro
    ADD CONSTRAINT filtro_nombre_key UNIQUE (nombre);
 B   ALTER TABLE ONLY public.filtro DROP CONSTRAINT filtro_nombre_key;
       public            postgres    false    227                    2606    16698 "   filtro_persona filtro_persona_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.filtro_persona
    ADD CONSTRAINT filtro_persona_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.filtro_persona DROP CONSTRAINT filtro_persona_pkey;
       public            postgres    false    240                    2606    16700    filtro filtro_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.filtro
    ADD CONSTRAINT filtro_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.filtro DROP CONSTRAINT filtro_pkey;
       public            postgres    false    227                    2606    16702 $   nacionalidad nacionalidad_nombre_key 
   CONSTRAINT     a   ALTER TABLE ONLY public.nacionalidad
    ADD CONSTRAINT nacionalidad_nombre_key UNIQUE (nombre);
 N   ALTER TABLE ONLY public.nacionalidad DROP CONSTRAINT nacionalidad_nombre_key;
       public            postgres    false    245                    2606    16704    nacionalidad nacionalidad_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.nacionalidad
    ADD CONSTRAINT nacionalidad_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.nacionalidad DROP CONSTRAINT nacionalidad_pkey;
       public            postgres    false    245                    2606    16706 (   nivel_estudios nivel_estudios_nombre_key 
   CONSTRAINT     e   ALTER TABLE ONLY public.nivel_estudios
    ADD CONSTRAINT nivel_estudios_nombre_key UNIQUE (nombre);
 R   ALTER TABLE ONLY public.nivel_estudios DROP CONSTRAINT nivel_estudios_nombre_key;
       public            postgres    false    248                    2606    16708 "   nivel_estudios nivel_estudios_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.nivel_estudios
    ADD CONSTRAINT nivel_estudios_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.nivel_estudios DROP CONSTRAINT nivel_estudios_pkey;
       public            postgres    false    248                    2606    16710    ocupacion ocupacion_nombre_key 
   CONSTRAINT     [   ALTER TABLE ONLY public.ocupacion
    ADD CONSTRAINT ocupacion_nombre_key UNIQUE (nombre);
 H   ALTER TABLE ONLY public.ocupacion DROP CONSTRAINT ocupacion_nombre_key;
       public            postgres    false    250                    2606    16712    ocupacion ocupacion_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ocupacion
    ADD CONSTRAINT ocupacion_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.ocupacion DROP CONSTRAINT ocupacion_pkey;
       public            postgres    false    250                    2606    16714    pais pais_nombre_key 
   CONSTRAINT     Q   ALTER TABLE ONLY public.pais
    ADD CONSTRAINT pais_nombre_key UNIQUE (nombre);
 >   ALTER TABLE ONLY public.pais DROP CONSTRAINT pais_nombre_key;
       public            postgres    false    252         !           2606    16716    pais pais_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.pais
    ADD CONSTRAINT pais_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.pais DROP CONSTRAINT pais_pkey;
       public            postgres    false    252         #           2606    17112 #   persona persona_identificación_key 
   CONSTRAINT     j   ALTER TABLE ONLY public.persona
    ADD CONSTRAINT "persona_identificación_key" UNIQUE (identificacion);
 O   ALTER TABLE ONLY public.persona DROP CONSTRAINT "persona_identificación_key";
       public            postgres    false    254         %           2606    16720    persona persona_licencia_key 
   CONSTRAINT     [   ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_licencia_key UNIQUE (licencia);
 F   ALTER TABLE ONLY public.persona DROP CONSTRAINT persona_licencia_key;
       public            postgres    false    254         '           2606    16722    persona persona_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.persona DROP CONSTRAINT persona_pkey;
       public            postgres    false    254         -           2606    16724    profesion profesion_nombre_key 
   CONSTRAINT     [   ALTER TABLE ONLY public.profesion
    ADD CONSTRAINT profesion_nombre_key UNIQUE (nombre);
 H   ALTER TABLE ONLY public.profesion DROP CONSTRAINT profesion_nombre_key;
       public            postgres    false    261         /           2606    16726    profesion profesion_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.profesion
    ADD CONSTRAINT profesion_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.profesion DROP CONSTRAINT profesion_pkey;
       public            postgres    false    261         3           2606    16728 0   tipo_configuracion tipo_configuracion_nombre_key 
   CONSTRAINT     m   ALTER TABLE ONLY public.tipo_configuracion
    ADD CONSTRAINT tipo_configuracion_nombre_key UNIQUE (nombre);
 Z   ALTER TABLE ONLY public.tipo_configuracion DROP CONSTRAINT tipo_configuracion_nombre_key;
       public            postgres    false    264         5           2606    16730 *   tipo_configuracion tipo_configuracion_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.tipo_configuracion
    ADD CONSTRAINT tipo_configuracion_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.tipo_configuracion DROP CONSTRAINT tipo_configuracion_pkey;
       public            postgres    false    264         9           2606    16732 &   tipo_licencia tipo_licencia_nombre_key 
   CONSTRAINT     c   ALTER TABLE ONLY public.tipo_licencia
    ADD CONSTRAINT tipo_licencia_nombre_key UNIQUE (nombre);
 P   ALTER TABLE ONLY public.tipo_licencia DROP CONSTRAINT tipo_licencia_nombre_key;
       public            postgres    false    266         ;           2606    16734     tipo_licencia tipo_licencia_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.tipo_licencia
    ADD CONSTRAINT tipo_licencia_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.tipo_licencia DROP CONSTRAINT tipo_licencia_pkey;
       public            postgres    false    266         �           2606    16736 &   detalle_tipo_pago tipo_pago_favor_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.detalle_tipo_pago
    ADD CONSTRAINT tipo_pago_favor_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.detalle_tipo_pago DROP CONSTRAINT tipo_pago_favor_pkey;
       public            postgres    false    211         ?           2606    16738    tipo_pago tipo_pago_nombre_key 
   CONSTRAINT     [   ALTER TABLE ONLY public.tipo_pago
    ADD CONSTRAINT tipo_pago_nombre_key UNIQUE (nombre);
 H   ALTER TABLE ONLY public.tipo_pago DROP CONSTRAINT tipo_pago_nombre_key;
       public            postgres    false    268         A           2606    16740    tipo_pago tipo_pago_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.tipo_pago
    ADD CONSTRAINT tipo_pago_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.tipo_pago DROP CONSTRAINT tipo_pago_pkey;
       public            postgres    false    268         E           2606    16742 &   tipo_vehiculo tipo_vehiculo_nombre_key 
   CONSTRAINT     c   ALTER TABLE ONLY public.tipo_vehiculo
    ADD CONSTRAINT tipo_vehiculo_nombre_key UNIQUE (nombre);
 P   ALTER TABLE ONLY public.tipo_vehiculo DROP CONSTRAINT tipo_vehiculo_nombre_key;
       public            postgres    false    270         G           2606    16744     tipo_vehiculo tipo_vehiculo_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.tipo_vehiculo
    ADD CONSTRAINT tipo_vehiculo_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.tipo_vehiculo DROP CONSTRAINT tipo_vehiculo_pkey;
       public            postgres    false    270         K           2606    16746    trabajo trabajo_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.trabajo
    ADD CONSTRAINT trabajo_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.trabajo DROP CONSTRAINT trabajo_pkey;
       public            postgres    false    272         M           2606    16748    ubicacion ubicacion_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ubicacion
    ADD CONSTRAINT ubicacion_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.ubicacion DROP CONSTRAINT ubicacion_pkey;
       public            postgres    false    278         �           2606    16750 #   empresa uk2fqlxbcs4h827hio1qam0dhd3 
   CONSTRAINT     `   ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT uk2fqlxbcs4h827hio1qam0dhd3 UNIQUE (nombre);
 M   ALTER TABLE ONLY public.empresa DROP CONSTRAINT uk2fqlxbcs4h827hio1qam0dhd3;
       public            postgres    false    219         C           2606    16752 %   tipo_pago uk2jn8asd6licmlekochs3wr069 
   CONSTRAINT     b   ALTER TABLE ONLY public.tipo_pago
    ADD CONSTRAINT uk2jn8asd6licmlekochs3wr069 UNIQUE (nombre);
 O   ALTER TABLE ONLY public.tipo_pago DROP CONSTRAINT uk2jn8asd6licmlekochs3wr069;
       public            postgres    false    268         O           2606    16754 "   usuario uk2mlfr087gb1ce55f2j87o74t 
   CONSTRAINT     _   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT uk2mlfr087gb1ce55f2j87o74t UNIQUE (correo);
 L   ALTER TABLE ONLY public.usuario DROP CONSTRAINT uk2mlfr087gb1ce55f2j87o74t;
       public            postgres    false    280         Q           2606    16756 #   usuario uk33gathdlc33wn52w45op1r397 
   CONSTRAINT     d   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT uk33gathdlc33wn52w45op1r397 UNIQUE (id_persona);
 M   ALTER TABLE ONLY public.usuario DROP CONSTRAINT uk33gathdlc33wn52w45op1r397;
       public            postgres    false    280         ^           2606    16758 $   vehiculo uk8k33brk0t0gtg3mvh7aup9asd 
   CONSTRAINT     a   ALTER TABLE ONLY public.vehiculo
    ADD CONSTRAINT uk8k33brk0t0gtg3mvh7aup9asd UNIQUE (nombre);
 N   ALTER TABLE ONLY public.vehiculo DROP CONSTRAINT uk8k33brk0t0gtg3mvh7aup9asd;
       public            postgres    false    287         1           2606    16760 %   profesion uk9g13ixelirooj3ohc6jmk4elh 
   CONSTRAINT     b   ALTER TABLE ONLY public.profesion
    ADD CONSTRAINT uk9g13ixelirooj3ohc6jmk4elh UNIQUE (nombre);
 O   ALTER TABLE ONLY public.profesion DROP CONSTRAINT uk9g13ixelirooj3ohc6jmk4elh;
       public            postgres    false    261         S           2606    16762 $   usuario uk_33gathdlc33wn52w45op1r397 
   CONSTRAINT     e   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT uk_33gathdlc33wn52w45op1r397 UNIQUE (id_persona);
 N   ALTER TABLE ONLY public.usuario DROP CONSTRAINT uk_33gathdlc33wn52w45op1r397;
       public            postgres    false    280                    2606    16764 (   nacionalidad ukai2qjhipbere5q56o9nn2k4oi 
   CONSTRAINT     e   ALTER TABLE ONLY public.nacionalidad
    ADD CONSTRAINT ukai2qjhipbere5q56o9nn2k4oi UNIQUE (nombre);
 R   ALTER TABLE ONLY public.nacionalidad DROP CONSTRAINT ukai2qjhipbere5q56o9nn2k4oi;
       public            postgres    false    245         I           2606    16766 )   tipo_vehiculo ukcvjjknqwylrwelqbudcg86aug 
   CONSTRAINT     f   ALTER TABLE ONLY public.tipo_vehiculo
    ADD CONSTRAINT ukcvjjknqwylrwelqbudcg86aug UNIQUE (nombre);
 S   ALTER TABLE ONLY public.tipo_vehiculo DROP CONSTRAINT ukcvjjknqwylrwelqbudcg86aug;
       public            postgres    false    270         =           2606    16768 )   tipo_licencia ukdk1ut3f3y7xlp92x4wkpmxeqw 
   CONSTRAINT     f   ALTER TABLE ONLY public.tipo_licencia
    ADD CONSTRAINT ukdk1ut3f3y7xlp92x4wkpmxeqw UNIQUE (nombre);
 S   ALTER TABLE ONLY public.tipo_licencia DROP CONSTRAINT ukdk1ut3f3y7xlp92x4wkpmxeqw;
       public            postgres    false    266         �           2606    16770 "   ciudad ukgq6hfkl2jwepoa130me3x7iak 
   CONSTRAINT     _   ALTER TABLE ONLY public.ciudad
    ADD CONSTRAINT ukgq6hfkl2jwepoa130me3x7iak UNIQUE (nombre);
 L   ALTER TABLE ONLY public.ciudad DROP CONSTRAINT ukgq6hfkl2jwepoa130me3x7iak;
       public            postgres    false    208                    2606    16772 "   filtro ukhtefuwbbsxlw16b20wufqwea0 
   CONSTRAINT     _   ALTER TABLE ONLY public.filtro
    ADD CONSTRAINT ukhtefuwbbsxlw16b20wufqwea0 UNIQUE (nombre);
 L   ALTER TABLE ONLY public.filtro DROP CONSTRAINT ukhtefuwbbsxlw16b20wufqwea0;
       public            postgres    false    227         U           2606    16774 #   usuario uki02kr8ui5pqddyd7pkm3v4jbt 
   CONSTRAINT     a   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT uki02kr8ui5pqddyd7pkm3v4jbt UNIQUE (usuario);
 M   ALTER TABLE ONLY public.usuario DROP CONSTRAINT uki02kr8ui5pqddyd7pkm3v4jbt;
       public            postgres    false    280                    2606    16776 %   ocupacion ukk5ah1k0qtb4pt5aokp0y53qc0 
   CONSTRAINT     b   ALTER TABLE ONLY public.ocupacion
    ADD CONSTRAINT ukk5ah1k0qtb4pt5aokp0y53qc0 UNIQUE (nombre);
 O   ALTER TABLE ONLY public.ocupacion DROP CONSTRAINT ukk5ah1k0qtb4pt5aokp0y53qc0;
       public            postgres    false    250         �           2606    16778 ,   categoria_filtro ukkfre22hnhgjf8fg6at2nu6vbi 
   CONSTRAINT     i   ALTER TABLE ONLY public.categoria_filtro
    ADD CONSTRAINT ukkfre22hnhgjf8fg6at2nu6vbi UNIQUE (nombre);
 V   ALTER TABLE ONLY public.categoria_filtro DROP CONSTRAINT ukkfre22hnhgjf8fg6at2nu6vbi;
       public            postgres    false    206         7           2606    16780 .   tipo_configuracion uklg2j2ii1muhc8gigjuey9lj95 
   CONSTRAINT     k   ALTER TABLE ONLY public.tipo_configuracion
    ADD CONSTRAINT uklg2j2ii1muhc8gigjuey9lj95 UNIQUE (nombre);
 X   ALTER TABLE ONLY public.tipo_configuracion DROP CONSTRAINT uklg2j2ii1muhc8gigjuey9lj95;
       public            postgres    false    264                    2606    16782 *   nivel_estudios ukome0q800iwkvkpqhgwboln8e2 
   CONSTRAINT     g   ALTER TABLE ONLY public.nivel_estudios
    ADD CONSTRAINT ukome0q800iwkvkpqhgwboln8e2 UNIQUE (nombre);
 T   ALTER TABLE ONLY public.nivel_estudios DROP CONSTRAINT ukome0q800iwkvkpqhgwboln8e2;
       public            postgres    false    248         )           2606    16784 #   persona ukox6qnkh1rllqubpew7pv0kblw 
   CONSTRAINT     b   ALTER TABLE ONLY public.persona
    ADD CONSTRAINT ukox6qnkh1rllqubpew7pv0kblw UNIQUE (licencia);
 M   ALTER TABLE ONLY public.persona DROP CONSTRAINT ukox6qnkh1rllqubpew7pv0kblw;
       public            postgres    false    254         +           2606    17114 #   persona uktr8fh0es1l6g7l2b688jykaqq 
   CONSTRAINT     h   ALTER TABLE ONLY public.persona
    ADD CONSTRAINT uktr8fh0es1l6g7l2b688jykaqq UNIQUE (identificacion);
 M   ALTER TABLE ONLY public.persona DROP CONSTRAINT uktr8fh0es1l6g7l2b688jykaqq;
       public            postgres    false    254         \           2606    16788 ,   usuario_ayuda_favor usuario_ayuda_favor_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.usuario_ayuda_favor
    ADD CONSTRAINT usuario_ayuda_favor_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.usuario_ayuda_favor DROP CONSTRAINT usuario_ayuda_favor_pkey;
       public            postgres    false    281         Y           2606    16790    usuario usuario_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pkey;
       public            postgres    false    280         `           2606    16792    vehiculo vehiculo_nombre_key 
   CONSTRAINT     Y   ALTER TABLE ONLY public.vehiculo
    ADD CONSTRAINT vehiculo_nombre_key UNIQUE (nombre);
 F   ALTER TABLE ONLY public.vehiculo DROP CONSTRAINT vehiculo_nombre_key;
       public            postgres    false    287         d           2606    16794 &   vehiculo_persona vehiculo_persona_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.vehiculo_persona
    ADD CONSTRAINT vehiculo_persona_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.vehiculo_persona DROP CONSTRAINT vehiculo_persona_pkey;
       public            postgres    false    290         b           2606    16796    vehiculo vehiculo_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.vehiculo
    ADD CONSTRAINT vehiculo_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.vehiculo DROP CONSTRAINT vehiculo_pkey;
       public            postgres    false    287         V           1259    16797    usuario_correo_uindex    INDEX     R   CREATE UNIQUE INDEX usuario_correo_uindex ON public.usuario USING btree (correo);
 )   DROP INDEX public.usuario_correo_uindex;
       public            postgres    false    280         W           1259    16798    usuario_id_persona_uindex    INDEX     Z   CREATE UNIQUE INDEX usuario_id_persona_uindex ON public.usuario USING btree (id_persona);
 -   DROP INDEX public.usuario_id_persona_uindex;
       public            postgres    false    280         Z           1259    16799    usuario_usuario_uindex    INDEX     T   CREATE UNIQUE INDEX usuario_usuario_uindex ON public.usuario USING btree (usuario);
 *   DROP INDEX public.usuario_usuario_uindex;
       public            postgres    false    280         x           2606    16800    nacionalidad _id_pais_fk    FK CONSTRAINT     v   ALTER TABLE ONLY public.nacionalidad
    ADD CONSTRAINT _id_pais_fk FOREIGN KEY (id_pais) REFERENCES public.pais(id);
 B   ALTER TABLE ONLY public.nacionalidad DROP CONSTRAINT _id_pais_fk;
       public          postgres    false    3361    252    245         e           2606    16805 %   calificacion calificacion_id_favor_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.calificacion
    ADD CONSTRAINT calificacion_id_favor_fk FOREIGN KEY (id_favor) REFERENCES public.favor(id);
 O   ALTER TABLE ONLY public.calificacion DROP CONSTRAINT calificacion_id_favor_fk;
       public          postgres    false    202    221    3327         f           2606    16810 '   calificacion calificacion_id_usuario_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.calificacion
    ADD CONSTRAINT calificacion_id_usuario_fk FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
 Q   ALTER TABLE ONLY public.calificacion DROP CONSTRAINT calificacion_id_usuario_fk;
       public          postgres    false    3417    202    280         g           2606    16815    ciudad ciudad_pais_id_fk    FK CONSTRAINT     v   ALTER TABLE ONLY public.ciudad
    ADD CONSTRAINT ciudad_pais_id_fk FOREIGN KEY (id_pais) REFERENCES public.pais(id);
 B   ALTER TABLE ONLY public.ciudad DROP CONSTRAINT ciudad_pais_id_fk;
       public          postgres    false    252    208    3361         h           2606    16820 3   detalle_tipo_pago detalle_tipo_pago_id_tipo_pago_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.detalle_tipo_pago
    ADD CONSTRAINT detalle_tipo_pago_id_tipo_pago_fk FOREIGN KEY (id_tipo_pago) REFERENCES public.tipo_pago(id);
 ]   ALTER TABLE ONLY public.detalle_tipo_pago DROP CONSTRAINT detalle_tipo_pago_id_tipo_pago_fk;
       public          postgres    false    268    211    3393         i           2606    16825 !   direccion direccion_persona_id_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.direccion
    ADD CONSTRAINT direccion_persona_id_fk FOREIGN KEY (id_persona) REFERENCES public.persona(id);
 K   ALTER TABLE ONLY public.direccion DROP CONSTRAINT direccion_persona_id_fk;
       public          postgres    false    254    214    3367         l           2606    16830 #   favor favor_id_detalle_tipo_pago_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.favor
    ADD CONSTRAINT favor_id_detalle_tipo_pago_fk FOREIGN KEY (id_detalle_tipo_pago) REFERENCES public.detalle_tipo_pago(id);
 M   ALTER TABLE ONLY public.favor DROP CONSTRAINT favor_id_detalle_tipo_pago_fk;
       public          postgres    false    211    221    3317         m           2606    16835    favor favor_id_direccion_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.favor
    ADD CONSTRAINT favor_id_direccion_fk FOREIGN KEY (id_direccion_favor) REFERENCES public.direccion(id);
 E   ALTER TABLE ONLY public.favor DROP CONSTRAINT favor_id_direccion_fk;
       public          postgres    false    214    221    3319         n           2606    16840 !   favor favor_id_usuario_realiza_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.favor
    ADD CONSTRAINT favor_id_usuario_realiza_fk FOREIGN KEY (id_usuario_realiza) REFERENCES public.usuario(id);
 K   ALTER TABLE ONLY public.favor DROP CONSTRAINT favor_id_usuario_realiza_fk;
       public          postgres    false    280    221    3417         o           2606    16845 "   favor favor_id_usuario_solicita_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.favor
    ADD CONSTRAINT favor_id_usuario_solicita_fk FOREIGN KEY (id_usuario_solicita) REFERENCES public.usuario(id);
 L   ALTER TABLE ONLY public.favor DROP CONSTRAINT favor_id_usuario_solicita_fk;
       public          postgres    false    280    221    3417         p           2606    16850 $   filtro filtro_categoria_filtro_id_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.filtro
    ADD CONSTRAINT filtro_categoria_filtro_id_fk FOREIGN KEY (id_categoria_filtro) REFERENCES public.categoria_filtro(id);
 N   ALTER TABLE ONLY public.filtro DROP CONSTRAINT filtro_categoria_filtro_id_fk;
       public          postgres    false    206    227    3307         q           2606    16855 F   filtro_configuracion_usuario filtro_configuracion_usuario_id_filtro_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.filtro_configuracion_usuario
    ADD CONSTRAINT filtro_configuracion_usuario_id_filtro_fk FOREIGN KEY (id_filtro) REFERENCES public.filtro(id);
 p   ALTER TABLE ONLY public.filtro_configuracion_usuario DROP CONSTRAINT filtro_configuracion_usuario_id_filtro_fk;
       public          postgres    false    228    3331    227         r           2606    16860 O   filtro_configuracion_usuario filtro_configuracion_usuario_id_tipo_configuracion    FK CONSTRAINT     �   ALTER TABLE ONLY public.filtro_configuracion_usuario
    ADD CONSTRAINT filtro_configuracion_usuario_id_tipo_configuracion FOREIGN KEY (id_tipo_configuracion) REFERENCES public.tipo_configuracion(id);
 y   ALTER TABLE ONLY public.filtro_configuracion_usuario DROP CONSTRAINT filtro_configuracion_usuario_id_tipo_configuracion;
       public          postgres    false    3381    264    228         s           2606    16865 D   filtro_configuracion_usuario filtro_configuracion_usuario_id_usuario    FK CONSTRAINT     �   ALTER TABLE ONLY public.filtro_configuracion_usuario
    ADD CONSTRAINT filtro_configuracion_usuario_id_usuario FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
 n   ALTER TABLE ONLY public.filtro_configuracion_usuario DROP CONSTRAINT filtro_configuracion_usuario_id_usuario;
       public          postgres    false    228    3417    280         t           2606    16870 "   filtro_favor filtro_favor_id_favor    FK CONSTRAINT     �   ALTER TABLE ONLY public.filtro_favor
    ADD CONSTRAINT filtro_favor_id_favor FOREIGN KEY (id_favor) REFERENCES public.favor(id);
 L   ALTER TABLE ONLY public.filtro_favor DROP CONSTRAINT filtro_favor_id_favor;
       public          postgres    false    3327    234    221         u           2606    16875 &   filtro_favor filtro_favor_id_filtro_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.filtro_favor
    ADD CONSTRAINT filtro_favor_id_filtro_fk FOREIGN KEY (id_filtro) REFERENCES public.filtro(id);
 P   ALTER TABLE ONLY public.filtro_favor DROP CONSTRAINT filtro_favor_id_filtro_fk;
       public          postgres    false    227    3331    234         v           2606    16880 *   filtro_persona filtro_persona_id_filtro_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.filtro_persona
    ADD CONSTRAINT filtro_persona_id_filtro_fk FOREIGN KEY (id_filtro) REFERENCES public.filtro(id);
 T   ALTER TABLE ONLY public.filtro_persona DROP CONSTRAINT filtro_persona_id_filtro_fk;
       public          postgres    false    3331    227    240         w           2606    16885 +   filtro_persona filtro_persona_id_persona_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.filtro_persona
    ADD CONSTRAINT filtro_persona_id_persona_fk FOREIGN KEY (id_persona) REFERENCES public.persona(id);
 U   ALTER TABLE ONLY public.filtro_persona DROP CONSTRAINT filtro_persona_id_persona_fk;
       public          postgres    false    254    240    3367         j           2606    16890    direccion id_ciudad_fk    FK CONSTRAINT     x   ALTER TABLE ONLY public.direccion
    ADD CONSTRAINT id_ciudad_fk FOREIGN KEY (id_ciudad) REFERENCES public.ciudad(id);
 @   ALTER TABLE ONLY public.direccion DROP CONSTRAINT id_ciudad_fk;
       public          postgres    false    208    3313    214         ~           2606    16895    profesion id_nivel_estudios_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.profesion
    ADD CONSTRAINT id_nivel_estudios_fk FOREIGN KEY (id_nivel_estudios) REFERENCES public.nivel_estudios(id);
 H   ALTER TABLE ONLY public.profesion DROP CONSTRAINT id_nivel_estudios_fk;
       public          postgres    false    248    3349    261         k           2606    16900    direccion id_ubicacion_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.direccion
    ADD CONSTRAINT id_ubicacion_fk FOREIGN KEY (id_ubicacion) REFERENCES public.ubicacion(id);
 C   ALTER TABLE ONLY public.direccion DROP CONSTRAINT id_ubicacion_fk;
       public          postgres    false    3405    278    214         y           2606    16905 "   persona persona_id_nacionalidad_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_id_nacionalidad_fk FOREIGN KEY (id_nacionalidad) REFERENCES public.nacionalidad(id);
 L   ALTER TABLE ONLY public.persona DROP CONSTRAINT persona_id_nacionalidad_fk;
       public          postgres    false    245    3343    254         z           2606    16910 $   persona persona_id_nivel_estudios_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_id_nivel_estudios_fk FOREIGN KEY (id_nivel_estudios) REFERENCES public.nivel_estudios(id);
 N   ALTER TABLE ONLY public.persona DROP CONSTRAINT persona_id_nivel_estudios_fk;
       public          postgres    false    248    3349    254         {           2606    16915    persona persona_id_ocupacion_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_id_ocupacion_fk FOREIGN KEY (id_ocupacion) REFERENCES public.ocupacion(id);
 I   ALTER TABLE ONLY public.persona DROP CONSTRAINT persona_id_ocupacion_fk;
       public          postgres    false    3355    250    254         |           2606    16920    persona persona_id_profesion_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_id_profesion_fk FOREIGN KEY (id_profesion) REFERENCES public.profesion(id);
 I   ALTER TABLE ONLY public.persona DROP CONSTRAINT persona_id_profesion_fk;
       public          postgres    false    3375    254    261         }           2606    16925 $   persona persona_id_tipo_lincencia_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_id_tipo_lincencia_fk FOREIGN KEY (id_tipo_licencia) REFERENCES public.tipo_licencia(id);
 N   ALTER TABLE ONLY public.persona DROP CONSTRAINT persona_id_tipo_lincencia_fk;
       public          postgres    false    266    254    3387                    2606    16930    trabajo trabajo_id_direccion    FK CONSTRAINT     �   ALTER TABLE ONLY public.trabajo
    ADD CONSTRAINT trabajo_id_direccion FOREIGN KEY (id_direccion) REFERENCES public.direccion(id);
 F   ALTER TABLE ONLY public.trabajo DROP CONSTRAINT trabajo_id_direccion;
       public          postgres    false    3319    272    214         �           2606    16935    trabajo trabajo_id_empresa_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.trabajo
    ADD CONSTRAINT trabajo_id_empresa_fk FOREIGN KEY (id_empresa) REFERENCES public.empresa(id);
 G   ALTER TABLE ONLY public.trabajo DROP CONSTRAINT trabajo_id_empresa_fk;
       public          postgres    false    3323    272    219         �           2606    16940    trabajo trabajo_id_ocupacion_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.trabajo
    ADD CONSTRAINT trabajo_id_ocupacion_fk FOREIGN KEY (id_ocupacion) REFERENCES public.ocupacion(id);
 I   ALTER TABLE ONLY public.trabajo DROP CONSTRAINT trabajo_id_ocupacion_fk;
       public          postgres    false    272    250    3355         �           2606    16945    trabajo trabajo_id_persona    FK CONSTRAINT     ~   ALTER TABLE ONLY public.trabajo
    ADD CONSTRAINT trabajo_id_persona FOREIGN KEY (id_persona) REFERENCES public.persona(id);
 D   ALTER TABLE ONLY public.trabajo DROP CONSTRAINT trabajo_id_persona;
       public          postgres    false    272    3367    254         �           2606    16950 3   usuario_ayuda_favor usuario_ayuda_favor_id_favor_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_ayuda_favor
    ADD CONSTRAINT usuario_ayuda_favor_id_favor_fk FOREIGN KEY (id_favor) REFERENCES public.favor(id);
 ]   ALTER TABLE ONLY public.usuario_ayuda_favor DROP CONSTRAINT usuario_ayuda_favor_id_favor_fk;
       public          postgres    false    281    221    3327         �           2606    16955 5   usuario_ayuda_favor usuario_ayuda_favor_id_usuario_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_ayuda_favor
    ADD CONSTRAINT usuario_ayuda_favor_id_usuario_fk FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);
 _   ALTER TABLE ONLY public.usuario_ayuda_favor DROP CONSTRAINT usuario_ayuda_favor_id_usuario_fk;
       public          postgres    false    280    281    3417         �           2606    16960    usuario usuario_persona_id_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_persona_id_fk FOREIGN KEY (id_persona) REFERENCES public.persona(id);
 G   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_persona_id_fk;
       public          postgres    false    280    254    3367         �           2606    16965 /   vehiculo_persona vehiculo_persona_id_persona_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.vehiculo_persona
    ADD CONSTRAINT vehiculo_persona_id_persona_fk FOREIGN KEY (id_persona) REFERENCES public.persona(id);
 Y   ALTER TABLE ONLY public.vehiculo_persona DROP CONSTRAINT vehiculo_persona_id_persona_fk;
       public          postgres    false    290    254    3367         �           2606    16970 0   vehiculo_persona vehiculo_persona_id_vehiculo_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.vehiculo_persona
    ADD CONSTRAINT vehiculo_persona_id_vehiculo_fk FOREIGN KEY (id_vehiculo) REFERENCES public.vehiculo(id);
 Z   ALTER TABLE ONLY public.vehiculo_persona DROP CONSTRAINT vehiculo_persona_id_vehiculo_fk;
       public          postgres    false    3426    290    287         �           2606    16975 %   vehiculo vehiculo_tipo_vehiculo_id_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.vehiculo
    ADD CONSTRAINT vehiculo_tipo_vehiculo_id_fk FOREIGN KEY (id_tipo_vehiculo) REFERENCES public.tipo_vehiculo(id);
 O   ALTER TABLE ONLY public.vehiculo DROP CONSTRAINT vehiculo_tipo_vehiculo_id_fk;
       public          postgres    false    287    3399    270                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  3591.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014267 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3595.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014273 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3597.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014275 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3600.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014256 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3603.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3608.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014266 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3610.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014257 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3616.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014265 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3617.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014266 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3623.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014263 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3629.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014271 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3634.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014265 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3637.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014270 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3639.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014272 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3641.dat                                                                                            0000600 0004000 0002000 00000000024 13575630675 0014264 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Peru
4	ccccc
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            3643.dat                                                                                            0000600 0004000 0002000 00000000454 13575630675 0014275 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        6	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
9	ssss	\N	\N	\N	\N	\N	\N	\N	\N	\N
12	2	\N	\N	\N	\N	\N	\N	\N	\N	\N
13	3	\N	\N	\N	\N	\N	\N	\N	\N	\N
14	33	\N	\N	\N	\N	\N	\N	\N	\N	\N
15	334	\N	\N	\N	\N	\N	\N	\N	\N	\N
16	44	\N	\N	\N	\N	\N	\N	\N	\N	\N
17	5	\N	\N	\N	\N	\N	\N	\N	\N	\N
18	66	\N	\N	\N	\N	\N	\N	\N	\N	\N
\.


                                                                                                                                                                                                                    3650.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014263 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3653.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014266 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3655.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014270 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3657.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014272 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3659.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014274 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3661.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014265 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3667.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014273 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3669.dat                                                                                            0000600 0004000 0002000 00000000366 13575630675 0014307 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        4	aa	aaa	aaa	\N	\N	\N	6	\N
7	ssss	ssss	ssss	\N	\N	\N	9	\N
10	2	2	2	\N	\N	\N	12	\N
11	3	3	3	\N	\N	\N	13	\N
12	33	33	33	\N	\N	\N	14	\N
13	334	334	334	\N	\N	\N	15	\N
14	44	44	44	\N	\N	\N	16	\N
15	5	5	5	\N	\N	\N	17	\N
16	66	66	66	\N	\N	\N	18	\N
\.


                                                                                                                                                                                                                                                                          3670.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014265 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3676.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014273 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3679.dat                                                                                            0000600 0004000 0002000 00000000005 13575630675 0014276 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           restore.sql                                                                                         0000600 0004000 0002000 00000270305 13575630675 0015414 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        --
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE acolitapp;
--
-- Name: acolitapp; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE acolitapp WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';


ALTER DATABASE acolitapp OWNER TO postgres;

\connect acolitapp

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: calificacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.calificacion (
    id bigint NOT NULL,
    calificacion integer,
    id_usuario bigint NOT NULL,
    id_favor bigint NOT NULL
);


ALTER TABLE public.calificacion OWNER TO postgres;

--
-- Name: calificacion_id_favor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.calificacion_id_favor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.calificacion_id_favor_seq OWNER TO postgres;

--
-- Name: calificacion_id_favor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.calificacion_id_favor_seq OWNED BY public.calificacion.id_favor;


--
-- Name: calificacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.calificacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.calificacion_id_seq OWNER TO postgres;

--
-- Name: calificacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.calificacion_id_seq OWNED BY public.calificacion.id;


--
-- Name: calificacion_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.calificacion_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.calificacion_id_usuario_seq OWNER TO postgres;

--
-- Name: calificacion_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.calificacion_id_usuario_seq OWNED BY public.calificacion.id_usuario;


--
-- Name: categoria_filtro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categoria_filtro (
    id bigint NOT NULL,
    nombre character varying(30)
);


ALTER TABLE public.categoria_filtro OWNER TO postgres;

--
-- Name: categoria_filtro_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categoria_filtro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categoria_filtro_id_seq OWNER TO postgres;

--
-- Name: categoria_filtro_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categoria_filtro_id_seq OWNED BY public.categoria_filtro.id;


--
-- Name: ciudad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ciudad (
    id bigint NOT NULL,
    nombre character varying(30),
    id_pais bigint NOT NULL
);


ALTER TABLE public.ciudad OWNER TO postgres;

--
-- Name: ciudad_id_pais_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ciudad_id_pais_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ciudad_id_pais_seq OWNER TO postgres;

--
-- Name: ciudad_id_pais_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ciudad_id_pais_seq OWNED BY public.ciudad.id_pais;


--
-- Name: ciudad_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ciudad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ciudad_id_seq OWNER TO postgres;

--
-- Name: ciudad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ciudad_id_seq OWNED BY public.ciudad.id;


--
-- Name: detalle_tipo_pago; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detalle_tipo_pago (
    id bigint NOT NULL,
    id_tipo_pago bigint NOT NULL,
    valor double precision,
    descripcion character varying(50)
);


ALTER TABLE public.detalle_tipo_pago OWNER TO postgres;

--
-- Name: detalle_tipo_pago_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detalle_tipo_pago_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detalle_tipo_pago_id_seq OWNER TO postgres;

--
-- Name: detalle_tipo_pago_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.detalle_tipo_pago_id_seq OWNED BY public.detalle_tipo_pago.id;


--
-- Name: detalle_tipo_pago_id_tipo_pago_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detalle_tipo_pago_id_tipo_pago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detalle_tipo_pago_id_tipo_pago_seq OWNER TO postgres;

--
-- Name: detalle_tipo_pago_id_tipo_pago_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.detalle_tipo_pago_id_tipo_pago_seq OWNED BY public.detalle_tipo_pago.id_tipo_pago;


--
-- Name: direccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.direccion (
    id bigint NOT NULL,
    calle_principal character varying(30),
    calle_secundaria character varying(30),
    barrio character varying(30),
    numero_casa character varying(20),
    referencia character varying(120),
    id_ciudad bigint NOT NULL,
    id_ubicacion bigint NOT NULL,
    id_persona bigint NOT NULL
);


ALTER TABLE public.direccion OWNER TO postgres;

--
-- Name: direccion_id_ciudad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.direccion_id_ciudad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.direccion_id_ciudad_seq OWNER TO postgres;

--
-- Name: direccion_id_ciudad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.direccion_id_ciudad_seq OWNED BY public.direccion.id_ciudad;


--
-- Name: direccion_id_persona_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.direccion_id_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.direccion_id_persona_seq OWNER TO postgres;

--
-- Name: direccion_id_persona_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.direccion_id_persona_seq OWNED BY public.direccion.id_persona;


--
-- Name: direccion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.direccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.direccion_id_seq OWNER TO postgres;

--
-- Name: direccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.direccion_id_seq OWNED BY public.direccion.id;


--
-- Name: direccion_id_ubicacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.direccion_id_ubicacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.direccion_id_ubicacion_seq OWNER TO postgres;

--
-- Name: direccion_id_ubicacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.direccion_id_ubicacion_seq OWNED BY public.direccion.id_ubicacion;


--
-- Name: empresa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.empresa (
    id bigint NOT NULL,
    nombre character varying(50)
);


ALTER TABLE public.empresa OWNER TO postgres;

--
-- Name: empresa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.empresa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.empresa_id_seq OWNER TO postgres;

--
-- Name: empresa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.empresa_id_seq OWNED BY public.empresa.id;


--
-- Name: favor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.favor (
    id bigint NOT NULL,
    titulo character varying(20),
    descripcion character varying(150),
    estado character varying(20),
    fecha_solicita timestamp without time zone,
    fecha_realiza timestamp without time zone,
    id_direccion_favor bigint NOT NULL,
    id_detalle_tipo_pago bigint NOT NULL,
    id_usuario_solicita bigint NOT NULL,
    id_usuario_realiza bigint NOT NULL
);


ALTER TABLE public.favor OWNER TO postgres;

--
-- Name: favor_id_detalle_tipo_pago_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.favor_id_detalle_tipo_pago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.favor_id_detalle_tipo_pago_seq OWNER TO postgres;

--
-- Name: favor_id_detalle_tipo_pago_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.favor_id_detalle_tipo_pago_seq OWNED BY public.favor.id_detalle_tipo_pago;


--
-- Name: favor_id_direccion_favor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.favor_id_direccion_favor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.favor_id_direccion_favor_seq OWNER TO postgres;

--
-- Name: favor_id_direccion_favor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.favor_id_direccion_favor_seq OWNED BY public.favor.id_direccion_favor;


--
-- Name: favor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.favor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.favor_id_seq OWNER TO postgres;

--
-- Name: favor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.favor_id_seq OWNED BY public.favor.id;


--
-- Name: favor_id_usuario_realiza_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.favor_id_usuario_realiza_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.favor_id_usuario_realiza_seq OWNER TO postgres;

--
-- Name: favor_id_usuario_realiza_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.favor_id_usuario_realiza_seq OWNED BY public.favor.id_usuario_realiza;


--
-- Name: favor_id_usuario_solicita_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.favor_id_usuario_solicita_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.favor_id_usuario_solicita_seq OWNER TO postgres;

--
-- Name: favor_id_usuario_solicita_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.favor_id_usuario_solicita_seq OWNED BY public.favor.id_usuario_solicita;


--
-- Name: filtro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.filtro (
    id bigint NOT NULL,
    nombre character varying(20),
    descripcion character varying(50),
    id_categoria_filtro bigint
);


ALTER TABLE public.filtro OWNER TO postgres;

--
-- Name: filtro_configuracion_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.filtro_configuracion_usuario (
    id bigint NOT NULL,
    id_filtro bigint NOT NULL,
    id_tipo_configuracion bigint NOT NULL,
    id_usuario bigint NOT NULL,
    valor_string character varying(20),
    valor_int integer,
    id_tabla bigint NOT NULL,
    estado character varying(20)
);


ALTER TABLE public.filtro_configuracion_usuario OWNER TO postgres;

--
-- Name: filtro_configuracion_usuario_id_filtro_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_configuracion_usuario_id_filtro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_configuracion_usuario_id_filtro_seq OWNER TO postgres;

--
-- Name: filtro_configuracion_usuario_id_filtro_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_configuracion_usuario_id_filtro_seq OWNED BY public.filtro_configuracion_usuario.id_filtro;


--
-- Name: filtro_configuracion_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_configuracion_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_configuracion_usuario_id_seq OWNER TO postgres;

--
-- Name: filtro_configuracion_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_configuracion_usuario_id_seq OWNED BY public.filtro_configuracion_usuario.id;


--
-- Name: filtro_configuracion_usuario_id_tabla_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_configuracion_usuario_id_tabla_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_configuracion_usuario_id_tabla_seq OWNER TO postgres;

--
-- Name: filtro_configuracion_usuario_id_tabla_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_configuracion_usuario_id_tabla_seq OWNED BY public.filtro_configuracion_usuario.id_tabla;


--
-- Name: filtro_configuracion_usuario_id_tipo_configuracion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_configuracion_usuario_id_tipo_configuracion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_configuracion_usuario_id_tipo_configuracion_seq OWNER TO postgres;

--
-- Name: filtro_configuracion_usuario_id_tipo_configuracion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_configuracion_usuario_id_tipo_configuracion_seq OWNED BY public.filtro_configuracion_usuario.id_tipo_configuracion;


--
-- Name: filtro_configuracion_usuario_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_configuracion_usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_configuracion_usuario_id_usuario_seq OWNER TO postgres;

--
-- Name: filtro_configuracion_usuario_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_configuracion_usuario_id_usuario_seq OWNED BY public.filtro_configuracion_usuario.id_usuario;


--
-- Name: filtro_favor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.filtro_favor (
    id bigint NOT NULL,
    id_filtro bigint NOT NULL,
    id_favor bigint NOT NULL,
    valor_string character varying(20),
    valor_int integer,
    id_tabla bigint NOT NULL,
    estado character varying(20)
);


ALTER TABLE public.filtro_favor OWNER TO postgres;

--
-- Name: filtro_favor_id_favor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_favor_id_favor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_favor_id_favor_seq OWNER TO postgres;

--
-- Name: filtro_favor_id_favor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_favor_id_favor_seq OWNED BY public.filtro_favor.id_favor;


--
-- Name: filtro_favor_id_filtro_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_favor_id_filtro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_favor_id_filtro_seq OWNER TO postgres;

--
-- Name: filtro_favor_id_filtro_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_favor_id_filtro_seq OWNED BY public.filtro_favor.id_filtro;


--
-- Name: filtro_favor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_favor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_favor_id_seq OWNER TO postgres;

--
-- Name: filtro_favor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_favor_id_seq OWNED BY public.filtro_favor.id;


--
-- Name: filtro_favor_id_tabla_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_favor_id_tabla_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_favor_id_tabla_seq OWNER TO postgres;

--
-- Name: filtro_favor_id_tabla_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_favor_id_tabla_seq OWNED BY public.filtro_favor.id_tabla;


--
-- Name: filtro_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_id_seq OWNER TO postgres;

--
-- Name: filtro_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_id_seq OWNED BY public.filtro.id;


--
-- Name: filtro_persona; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.filtro_persona (
    id bigint NOT NULL,
    id_filtro bigint NOT NULL,
    id_persona bigint NOT NULL,
    valor_string character varying(20),
    valor_int integer,
    id_tabla bigint NOT NULL,
    estado character varying(20)
);


ALTER TABLE public.filtro_persona OWNER TO postgres;

--
-- Name: filtro_persona_id_filtro_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_persona_id_filtro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_persona_id_filtro_seq OWNER TO postgres;

--
-- Name: filtro_persona_id_filtro_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_persona_id_filtro_seq OWNED BY public.filtro_persona.id_filtro;


--
-- Name: filtro_persona_id_persona_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_persona_id_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_persona_id_persona_seq OWNER TO postgres;

--
-- Name: filtro_persona_id_persona_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_persona_id_persona_seq OWNED BY public.filtro_persona.id_persona;


--
-- Name: filtro_persona_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_persona_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_persona_id_seq OWNER TO postgres;

--
-- Name: filtro_persona_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_persona_id_seq OWNED BY public.filtro_persona.id;


--
-- Name: filtro_persona_id_tabla_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filtro_persona_id_tabla_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filtro_persona_id_tabla_seq OWNER TO postgres;

--
-- Name: filtro_persona_id_tabla_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filtro_persona_id_tabla_seq OWNED BY public.filtro_persona.id_tabla;


--
-- Name: nacionalidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nacionalidad (
    id bigint NOT NULL,
    nombre character varying(30),
    id_pais bigint NOT NULL
);


ALTER TABLE public.nacionalidad OWNER TO postgres;

--
-- Name: nacionalidad_id_pais_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nacionalidad_id_pais_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nacionalidad_id_pais_seq OWNER TO postgres;

--
-- Name: nacionalidad_id_pais_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nacionalidad_id_pais_seq OWNED BY public.nacionalidad.id_pais;


--
-- Name: nacionalidad_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nacionalidad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nacionalidad_id_seq OWNER TO postgres;

--
-- Name: nacionalidad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nacionalidad_id_seq OWNED BY public.nacionalidad.id;


--
-- Name: nivel_estudios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nivel_estudios (
    id bigint NOT NULL,
    nombre character varying(30)
);


ALTER TABLE public.nivel_estudios OWNER TO postgres;

--
-- Name: nivel_estudios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nivel_estudios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nivel_estudios_id_seq OWNER TO postgres;

--
-- Name: nivel_estudios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nivel_estudios_id_seq OWNED BY public.nivel_estudios.id;


--
-- Name: ocupacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ocupacion (
    id bigint NOT NULL,
    nombre character varying(30)
);


ALTER TABLE public.ocupacion OWNER TO postgres;

--
-- Name: ocupacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ocupacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ocupacion_id_seq OWNER TO postgres;

--
-- Name: ocupacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ocupacion_id_seq OWNED BY public.ocupacion.id;


--
-- Name: pais; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pais (
    id bigint NOT NULL,
    nombre character varying(30)
);


ALTER TABLE public.pais OWNER TO postgres;

--
-- Name: pais_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pais_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pais_id_seq OWNER TO postgres;

--
-- Name: pais_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pais_id_seq OWNED BY public.pais.id;


--
-- Name: persona; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persona (
    id bigint NOT NULL,
    identificacion character varying(20),
    nombres_apellidos character varying(50),
    telefono character varying(10),
    licencia character varying(15),
    id_tipo_licencia bigint,
    id_nacionalidad bigint,
    id_nivel_estudios bigint,
    id_profesion bigint,
    id_ocupacion bigint,
    informacion_adicional character varying(120)
);


ALTER TABLE public.persona OWNER TO postgres;

--
-- Name: persona_id_nacionalidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_id_nacionalidad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_id_nacionalidad_seq OWNER TO postgres;

--
-- Name: persona_id_nacionalidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_id_nacionalidad_seq OWNED BY public.persona.id_nacionalidad;


--
-- Name: persona_id_nivel_estudios_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_id_nivel_estudios_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_id_nivel_estudios_seq OWNER TO postgres;

--
-- Name: persona_id_nivel_estudios_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_id_nivel_estudios_seq OWNED BY public.persona.id_nivel_estudios;


--
-- Name: persona_id_ocupacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_id_ocupacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_id_ocupacion_seq OWNER TO postgres;

--
-- Name: persona_id_ocupacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_id_ocupacion_seq OWNED BY public.persona.id_ocupacion;


--
-- Name: persona_id_profesion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_id_profesion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_id_profesion_seq OWNER TO postgres;

--
-- Name: persona_id_profesion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_id_profesion_seq OWNED BY public.persona.id_profesion;


--
-- Name: persona_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_id_seq OWNER TO postgres;

--
-- Name: persona_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_id_seq OWNED BY public.persona.id;


--
-- Name: persona_id_tipo_licencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_id_tipo_licencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_id_tipo_licencia_seq OWNER TO postgres;

--
-- Name: persona_id_tipo_licencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_id_tipo_licencia_seq OWNED BY public.persona.id_tipo_licencia;


--
-- Name: profesion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profesion (
    id bigint NOT NULL,
    nombre character varying(60),
    id_nivel_estudios bigint NOT NULL
);


ALTER TABLE public.profesion OWNER TO postgres;

--
-- Name: profesion_id_nivel_estudios_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.profesion_id_nivel_estudios_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profesion_id_nivel_estudios_seq OWNER TO postgres;

--
-- Name: profesion_id_nivel_estudios_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.profesion_id_nivel_estudios_seq OWNED BY public.profesion.id_nivel_estudios;


--
-- Name: profesion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.profesion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profesion_id_seq OWNER TO postgres;

--
-- Name: profesion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.profesion_id_seq OWNED BY public.profesion.id;


--
-- Name: tipo_configuracion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_configuracion (
    id bigint NOT NULL,
    nombre character varying(30)
);


ALTER TABLE public.tipo_configuracion OWNER TO postgres;

--
-- Name: tipo_configuracion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_configuracion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_configuracion_id_seq OWNER TO postgres;

--
-- Name: tipo_configuracion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_configuracion_id_seq OWNED BY public.tipo_configuracion.id;


--
-- Name: tipo_licencia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_licencia (
    id bigint NOT NULL,
    nombre character varying(30)
);


ALTER TABLE public.tipo_licencia OWNER TO postgres;

--
-- Name: tipo_licencia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_licencia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_licencia_id_seq OWNER TO postgres;

--
-- Name: tipo_licencia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_licencia_id_seq OWNED BY public.tipo_licencia.id;


--
-- Name: tipo_pago; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_pago (
    id bigint NOT NULL,
    nombre character varying(30)
);


ALTER TABLE public.tipo_pago OWNER TO postgres;

--
-- Name: tipo_pago_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_pago_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_pago_id_seq OWNER TO postgres;

--
-- Name: tipo_pago_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_pago_id_seq OWNED BY public.tipo_pago.id;


--
-- Name: tipo_vehiculo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_vehiculo (
    id bigint NOT NULL,
    nombre character varying(30)
);


ALTER TABLE public.tipo_vehiculo OWNER TO postgres;

--
-- Name: tipo_vehiculo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_vehiculo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_vehiculo_id_seq OWNER TO postgres;

--
-- Name: tipo_vehiculo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_vehiculo_id_seq OWNED BY public.tipo_vehiculo.id;


--
-- Name: trabajo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trabajo (
    id bigint NOT NULL,
    cargo character varying(50),
    id_persona bigint NOT NULL,
    id_ocupacion bigint NOT NULL,
    id_empresa bigint NOT NULL,
    id_direccion bigint NOT NULL
);


ALTER TABLE public.trabajo OWNER TO postgres;

--
-- Name: trabajo_id_direccion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trabajo_id_direccion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trabajo_id_direccion_seq OWNER TO postgres;

--
-- Name: trabajo_id_direccion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trabajo_id_direccion_seq OWNED BY public.trabajo.id_direccion;


--
-- Name: trabajo_id_empresa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trabajo_id_empresa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trabajo_id_empresa_seq OWNER TO postgres;

--
-- Name: trabajo_id_empresa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trabajo_id_empresa_seq OWNED BY public.trabajo.id_empresa;


--
-- Name: trabajo_id_ocupacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trabajo_id_ocupacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trabajo_id_ocupacion_seq OWNER TO postgres;

--
-- Name: trabajo_id_ocupacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trabajo_id_ocupacion_seq OWNED BY public.trabajo.id_ocupacion;


--
-- Name: trabajo_id_persona_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trabajo_id_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trabajo_id_persona_seq OWNER TO postgres;

--
-- Name: trabajo_id_persona_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trabajo_id_persona_seq OWNED BY public.trabajo.id_persona;


--
-- Name: trabajo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trabajo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trabajo_id_seq OWNER TO postgres;

--
-- Name: trabajo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trabajo_id_seq OWNED BY public.trabajo.id;


--
-- Name: ubicacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ubicacion (
    id bigint NOT NULL,
    latitud double precision,
    longitud double precision
);


ALTER TABLE public.ubicacion OWNER TO postgres;

--
-- Name: ubicacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ubicacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ubicacion_id_seq OWNER TO postgres;

--
-- Name: ubicacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ubicacion_id_seq OWNED BY public.ubicacion.id;


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id bigint NOT NULL,
    usuario character varying(20),
    correo character varying(20),
    contrasenia character varying(12),
    foto bytea,
    calificacion integer,
    estado integer,
    id_persona bigint,
    token character varying(200)
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- Name: usuario_ayuda_favor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario_ayuda_favor (
    id bigint NOT NULL,
    id_usuario bigint NOT NULL,
    id_favor bigint NOT NULL,
    estado_aceptacion character varying(20)
);


ALTER TABLE public.usuario_ayuda_favor OWNER TO postgres;

--
-- Name: usuario_ayuda_favor_id_favor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_ayuda_favor_id_favor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_ayuda_favor_id_favor_seq OWNER TO postgres;

--
-- Name: usuario_ayuda_favor_id_favor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_ayuda_favor_id_favor_seq OWNED BY public.usuario_ayuda_favor.id_favor;


--
-- Name: usuario_ayuda_favor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_ayuda_favor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_ayuda_favor_id_seq OWNER TO postgres;

--
-- Name: usuario_ayuda_favor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_ayuda_favor_id_seq OWNED BY public.usuario_ayuda_favor.id;


--
-- Name: usuario_ayuda_favor_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_ayuda_favor_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_ayuda_favor_id_usuario_seq OWNER TO postgres;

--
-- Name: usuario_ayuda_favor_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_ayuda_favor_id_usuario_seq OWNED BY public.usuario_ayuda_favor.id_usuario;


--
-- Name: usuario_id_persona_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_id_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_persona_seq OWNER TO postgres;

--
-- Name: usuario_id_persona_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_id_persona_seq OWNED BY public.usuario.id_persona;


--
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_seq OWNER TO postgres;

--
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_id_seq OWNED BY public.usuario.id;


--
-- Name: vehiculo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vehiculo (
    id bigint NOT NULL,
    nombre character varying(30),
    id_tipo_vehiculo bigint NOT NULL
);


ALTER TABLE public.vehiculo OWNER TO postgres;

--
-- Name: vehiculo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vehiculo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vehiculo_id_seq OWNER TO postgres;

--
-- Name: vehiculo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vehiculo_id_seq OWNED BY public.vehiculo.id;


--
-- Name: vehiculo_id_tipo_vehiculo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vehiculo_id_tipo_vehiculo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vehiculo_id_tipo_vehiculo_seq OWNER TO postgres;

--
-- Name: vehiculo_id_tipo_vehiculo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vehiculo_id_tipo_vehiculo_seq OWNED BY public.vehiculo.id_tipo_vehiculo;


--
-- Name: vehiculo_persona; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vehiculo_persona (
    id bigint NOT NULL,
    id_vehiculo bigint NOT NULL,
    id_persona bigint NOT NULL
);


ALTER TABLE public.vehiculo_persona OWNER TO postgres;

--
-- Name: vehiculo_persona_id_persona_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vehiculo_persona_id_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vehiculo_persona_id_persona_seq OWNER TO postgres;

--
-- Name: vehiculo_persona_id_persona_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vehiculo_persona_id_persona_seq OWNED BY public.vehiculo_persona.id_persona;


--
-- Name: vehiculo_persona_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vehiculo_persona_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vehiculo_persona_id_seq OWNER TO postgres;

--
-- Name: vehiculo_persona_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vehiculo_persona_id_seq OWNED BY public.vehiculo_persona.id;


--
-- Name: vehiculo_persona_id_vehiculo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vehiculo_persona_id_vehiculo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vehiculo_persona_id_vehiculo_seq OWNER TO postgres;

--
-- Name: vehiculo_persona_id_vehiculo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vehiculo_persona_id_vehiculo_seq OWNED BY public.vehiculo_persona.id_vehiculo;


--
-- Name: calificacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calificacion ALTER COLUMN id SET DEFAULT nextval('public.calificacion_id_seq'::regclass);


--
-- Name: calificacion id_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calificacion ALTER COLUMN id_usuario SET DEFAULT nextval('public.calificacion_id_usuario_seq'::regclass);


--
-- Name: calificacion id_favor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calificacion ALTER COLUMN id_favor SET DEFAULT nextval('public.calificacion_id_favor_seq'::regclass);


--
-- Name: categoria_filtro id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria_filtro ALTER COLUMN id SET DEFAULT nextval('public.categoria_filtro_id_seq'::regclass);


--
-- Name: ciudad id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ciudad ALTER COLUMN id SET DEFAULT nextval('public.ciudad_id_seq'::regclass);


--
-- Name: ciudad id_pais; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ciudad ALTER COLUMN id_pais SET DEFAULT nextval('public.ciudad_id_pais_seq'::regclass);


--
-- Name: detalle_tipo_pago id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_tipo_pago ALTER COLUMN id SET DEFAULT nextval('public.detalle_tipo_pago_id_seq'::regclass);


--
-- Name: detalle_tipo_pago id_tipo_pago; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_tipo_pago ALTER COLUMN id_tipo_pago SET DEFAULT nextval('public.detalle_tipo_pago_id_tipo_pago_seq'::regclass);


--
-- Name: direccion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.direccion ALTER COLUMN id SET DEFAULT nextval('public.direccion_id_seq'::regclass);


--
-- Name: direccion id_ciudad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.direccion ALTER COLUMN id_ciudad SET DEFAULT nextval('public.direccion_id_ciudad_seq'::regclass);


--
-- Name: direccion id_ubicacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.direccion ALTER COLUMN id_ubicacion SET DEFAULT nextval('public.direccion_id_ubicacion_seq'::regclass);


--
-- Name: direccion id_persona; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.direccion ALTER COLUMN id_persona SET DEFAULT nextval('public.direccion_id_persona_seq'::regclass);


--
-- Name: empresa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa ALTER COLUMN id SET DEFAULT nextval('public.empresa_id_seq'::regclass);


--
-- Name: favor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.favor ALTER COLUMN id SET DEFAULT nextval('public.favor_id_seq'::regclass);


--
-- Name: favor id_direccion_favor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.favor ALTER COLUMN id_direccion_favor SET DEFAULT nextval('public.favor_id_direccion_favor_seq'::regclass);


--
-- Name: favor id_detalle_tipo_pago; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.favor ALTER COLUMN id_detalle_tipo_pago SET DEFAULT nextval('public.favor_id_detalle_tipo_pago_seq'::regclass);


--
-- Name: favor id_usuario_solicita; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.favor ALTER COLUMN id_usuario_solicita SET DEFAULT nextval('public.favor_id_usuario_solicita_seq'::regclass);


--
-- Name: favor id_usuario_realiza; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.favor ALTER COLUMN id_usuario_realiza SET DEFAULT nextval('public.favor_id_usuario_realiza_seq'::regclass);


--
-- Name: filtro id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro ALTER COLUMN id SET DEFAULT nextval('public.filtro_id_seq'::regclass);


--
-- Name: filtro_configuracion_usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_configuracion_usuario ALTER COLUMN id SET DEFAULT nextval('public.filtro_configuracion_usuario_id_seq'::regclass);


--
-- Name: filtro_configuracion_usuario id_filtro; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_configuracion_usuario ALTER COLUMN id_filtro SET DEFAULT nextval('public.filtro_configuracion_usuario_id_filtro_seq'::regclass);


--
-- Name: filtro_configuracion_usuario id_tipo_configuracion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_configuracion_usuario ALTER COLUMN id_tipo_configuracion SET DEFAULT nextval('public.filtro_configuracion_usuario_id_tipo_configuracion_seq'::regclass);


--
-- Name: filtro_configuracion_usuario id_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_configuracion_usuario ALTER COLUMN id_usuario SET DEFAULT nextval('public.filtro_configuracion_usuario_id_usuario_seq'::regclass);


--
-- Name: filtro_configuracion_usuario id_tabla; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_configuracion_usuario ALTER COLUMN id_tabla SET DEFAULT nextval('public.filtro_configuracion_usuario_id_tabla_seq'::regclass);


--
-- Name: filtro_favor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_favor ALTER COLUMN id SET DEFAULT nextval('public.filtro_favor_id_seq'::regclass);


--
-- Name: filtro_favor id_filtro; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_favor ALTER COLUMN id_filtro SET DEFAULT nextval('public.filtro_favor_id_filtro_seq'::regclass);


--
-- Name: filtro_favor id_favor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_favor ALTER COLUMN id_favor SET DEFAULT nextval('public.filtro_favor_id_favor_seq'::regclass);


--
-- Name: filtro_favor id_tabla; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_favor ALTER COLUMN id_tabla SET DEFAULT nextval('public.filtro_favor_id_tabla_seq'::regclass);


--
-- Name: filtro_persona id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_persona ALTER COLUMN id SET DEFAULT nextval('public.filtro_persona_id_seq'::regclass);


--
-- Name: filtro_persona id_filtro; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_persona ALTER COLUMN id_filtro SET DEFAULT nextval('public.filtro_persona_id_filtro_seq'::regclass);


--
-- Name: filtro_persona id_persona; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_persona ALTER COLUMN id_persona SET DEFAULT nextval('public.filtro_persona_id_persona_seq'::regclass);


--
-- Name: filtro_persona id_tabla; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_persona ALTER COLUMN id_tabla SET DEFAULT nextval('public.filtro_persona_id_tabla_seq'::regclass);


--
-- Name: nacionalidad id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nacionalidad ALTER COLUMN id SET DEFAULT nextval('public.nacionalidad_id_seq'::regclass);


--
-- Name: nacionalidad id_pais; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nacionalidad ALTER COLUMN id_pais SET DEFAULT nextval('public.nacionalidad_id_pais_seq'::regclass);


--
-- Name: nivel_estudios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel_estudios ALTER COLUMN id SET DEFAULT nextval('public.nivel_estudios_id_seq'::regclass);


--
-- Name: ocupacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ocupacion ALTER COLUMN id SET DEFAULT nextval('public.ocupacion_id_seq'::regclass);


--
-- Name: pais id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pais ALTER COLUMN id SET DEFAULT nextval('public.pais_id_seq'::regclass);


--
-- Name: persona id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona ALTER COLUMN id SET DEFAULT nextval('public.persona_id_seq'::regclass);


--
-- Name: persona id_tipo_licencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona ALTER COLUMN id_tipo_licencia SET DEFAULT nextval('public.persona_id_tipo_licencia_seq'::regclass);


--
-- Name: persona id_nacionalidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona ALTER COLUMN id_nacionalidad SET DEFAULT nextval('public.persona_id_nacionalidad_seq'::regclass);


--
-- Name: persona id_nivel_estudios; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona ALTER COLUMN id_nivel_estudios SET DEFAULT nextval('public.persona_id_nivel_estudios_seq'::regclass);


--
-- Name: persona id_profesion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona ALTER COLUMN id_profesion SET DEFAULT nextval('public.persona_id_profesion_seq'::regclass);


--
-- Name: persona id_ocupacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona ALTER COLUMN id_ocupacion SET DEFAULT nextval('public.persona_id_ocupacion_seq'::regclass);


--
-- Name: profesion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profesion ALTER COLUMN id SET DEFAULT nextval('public.profesion_id_seq'::regclass);


--
-- Name: profesion id_nivel_estudios; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profesion ALTER COLUMN id_nivel_estudios SET DEFAULT nextval('public.profesion_id_nivel_estudios_seq'::regclass);


--
-- Name: tipo_configuracion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_configuracion ALTER COLUMN id SET DEFAULT nextval('public.tipo_configuracion_id_seq'::regclass);


--
-- Name: tipo_licencia id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_licencia ALTER COLUMN id SET DEFAULT nextval('public.tipo_licencia_id_seq'::regclass);


--
-- Name: tipo_pago id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_pago ALTER COLUMN id SET DEFAULT nextval('public.tipo_pago_id_seq'::regclass);


--
-- Name: tipo_vehiculo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_vehiculo ALTER COLUMN id SET DEFAULT nextval('public.tipo_vehiculo_id_seq'::regclass);


--
-- Name: trabajo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo ALTER COLUMN id SET DEFAULT nextval('public.trabajo_id_seq'::regclass);


--
-- Name: trabajo id_persona; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo ALTER COLUMN id_persona SET DEFAULT nextval('public.trabajo_id_persona_seq'::regclass);


--
-- Name: trabajo id_ocupacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo ALTER COLUMN id_ocupacion SET DEFAULT nextval('public.trabajo_id_ocupacion_seq'::regclass);


--
-- Name: trabajo id_empresa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo ALTER COLUMN id_empresa SET DEFAULT nextval('public.trabajo_id_empresa_seq'::regclass);


--
-- Name: trabajo id_direccion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo ALTER COLUMN id_direccion SET DEFAULT nextval('public.trabajo_id_direccion_seq'::regclass);


--
-- Name: ubicacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion ALTER COLUMN id SET DEFAULT nextval('public.ubicacion_id_seq'::regclass);


--
-- Name: usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario ALTER COLUMN id SET DEFAULT nextval('public.usuario_id_seq'::regclass);


--
-- Name: usuario id_persona; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario ALTER COLUMN id_persona SET DEFAULT nextval('public.usuario_id_persona_seq'::regclass);


--
-- Name: usuario_ayuda_favor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_ayuda_favor ALTER COLUMN id SET DEFAULT nextval('public.usuario_ayuda_favor_id_seq'::regclass);


--
-- Name: usuario_ayuda_favor id_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_ayuda_favor ALTER COLUMN id_usuario SET DEFAULT nextval('public.usuario_ayuda_favor_id_usuario_seq'::regclass);


--
-- Name: usuario_ayuda_favor id_favor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_ayuda_favor ALTER COLUMN id_favor SET DEFAULT nextval('public.usuario_ayuda_favor_id_favor_seq'::regclass);


--
-- Name: vehiculo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehiculo ALTER COLUMN id SET DEFAULT nextval('public.vehiculo_id_seq'::regclass);


--
-- Name: vehiculo id_tipo_vehiculo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehiculo ALTER COLUMN id_tipo_vehiculo SET DEFAULT nextval('public.vehiculo_id_tipo_vehiculo_seq'::regclass);


--
-- Name: vehiculo_persona id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehiculo_persona ALTER COLUMN id SET DEFAULT nextval('public.vehiculo_persona_id_seq'::regclass);


--
-- Name: vehiculo_persona id_vehiculo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehiculo_persona ALTER COLUMN id_vehiculo SET DEFAULT nextval('public.vehiculo_persona_id_vehiculo_seq'::regclass);


--
-- Name: vehiculo_persona id_persona; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehiculo_persona ALTER COLUMN id_persona SET DEFAULT nextval('public.vehiculo_persona_id_persona_seq'::regclass);


--
-- Data for Name: calificacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.calificacion (id, calificacion, id_usuario, id_favor) FROM stdin;
\.
COPY public.calificacion (id, calificacion, id_usuario, id_favor) FROM '$$PATH$$/3591.dat';

--
-- Data for Name: categoria_filtro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categoria_filtro (id, nombre) FROM stdin;
\.
COPY public.categoria_filtro (id, nombre) FROM '$$PATH$$/3595.dat';

--
-- Data for Name: ciudad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ciudad (id, nombre, id_pais) FROM stdin;
\.
COPY public.ciudad (id, nombre, id_pais) FROM '$$PATH$$/3597.dat';

--
-- Data for Name: detalle_tipo_pago; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detalle_tipo_pago (id, id_tipo_pago, valor, descripcion) FROM stdin;
\.
COPY public.detalle_tipo_pago (id, id_tipo_pago, valor, descripcion) FROM '$$PATH$$/3600.dat';

--
-- Data for Name: direccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.direccion (id, calle_principal, calle_secundaria, barrio, numero_casa, referencia, id_ciudad, id_ubicacion, id_persona) FROM stdin;
\.
COPY public.direccion (id, calle_principal, calle_secundaria, barrio, numero_casa, referencia, id_ciudad, id_ubicacion, id_persona) FROM '$$PATH$$/3603.dat';

--
-- Data for Name: empresa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.empresa (id, nombre) FROM stdin;
\.
COPY public.empresa (id, nombre) FROM '$$PATH$$/3608.dat';

--
-- Data for Name: favor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.favor (id, titulo, descripcion, estado, fecha_solicita, fecha_realiza, id_direccion_favor, id_detalle_tipo_pago, id_usuario_solicita, id_usuario_realiza) FROM stdin;
\.
COPY public.favor (id, titulo, descripcion, estado, fecha_solicita, fecha_realiza, id_direccion_favor, id_detalle_tipo_pago, id_usuario_solicita, id_usuario_realiza) FROM '$$PATH$$/3610.dat';

--
-- Data for Name: filtro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.filtro (id, nombre, descripcion, id_categoria_filtro) FROM stdin;
\.
COPY public.filtro (id, nombre, descripcion, id_categoria_filtro) FROM '$$PATH$$/3616.dat';

--
-- Data for Name: filtro_configuracion_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.filtro_configuracion_usuario (id, id_filtro, id_tipo_configuracion, id_usuario, valor_string, valor_int, id_tabla, estado) FROM stdin;
\.
COPY public.filtro_configuracion_usuario (id, id_filtro, id_tipo_configuracion, id_usuario, valor_string, valor_int, id_tabla, estado) FROM '$$PATH$$/3617.dat';

--
-- Data for Name: filtro_favor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.filtro_favor (id, id_filtro, id_favor, valor_string, valor_int, id_tabla, estado) FROM stdin;
\.
COPY public.filtro_favor (id, id_filtro, id_favor, valor_string, valor_int, id_tabla, estado) FROM '$$PATH$$/3623.dat';

--
-- Data for Name: filtro_persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.filtro_persona (id, id_filtro, id_persona, valor_string, valor_int, id_tabla, estado) FROM stdin;
\.
COPY public.filtro_persona (id, id_filtro, id_persona, valor_string, valor_int, id_tabla, estado) FROM '$$PATH$$/3629.dat';

--
-- Data for Name: nacionalidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nacionalidad (id, nombre, id_pais) FROM stdin;
\.
COPY public.nacionalidad (id, nombre, id_pais) FROM '$$PATH$$/3634.dat';

--
-- Data for Name: nivel_estudios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nivel_estudios (id, nombre) FROM stdin;
\.
COPY public.nivel_estudios (id, nombre) FROM '$$PATH$$/3637.dat';

--
-- Data for Name: ocupacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ocupacion (id, nombre) FROM stdin;
\.
COPY public.ocupacion (id, nombre) FROM '$$PATH$$/3639.dat';

--
-- Data for Name: pais; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pais (id, nombre) FROM stdin;
\.
COPY public.pais (id, nombre) FROM '$$PATH$$/3641.dat';

--
-- Data for Name: persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.persona (id, identificacion, nombres_apellidos, telefono, licencia, id_tipo_licencia, id_nacionalidad, id_nivel_estudios, id_profesion, id_ocupacion, informacion_adicional) FROM stdin;
\.
COPY public.persona (id, identificacion, nombres_apellidos, telefono, licencia, id_tipo_licencia, id_nacionalidad, id_nivel_estudios, id_profesion, id_ocupacion, informacion_adicional) FROM '$$PATH$$/3643.dat';

--
-- Data for Name: profesion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.profesion (id, nombre, id_nivel_estudios) FROM stdin;
\.
COPY public.profesion (id, nombre, id_nivel_estudios) FROM '$$PATH$$/3650.dat';

--
-- Data for Name: tipo_configuracion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_configuracion (id, nombre) FROM stdin;
\.
COPY public.tipo_configuracion (id, nombre) FROM '$$PATH$$/3653.dat';

--
-- Data for Name: tipo_licencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_licencia (id, nombre) FROM stdin;
\.
COPY public.tipo_licencia (id, nombre) FROM '$$PATH$$/3655.dat';

--
-- Data for Name: tipo_pago; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_pago (id, nombre) FROM stdin;
\.
COPY public.tipo_pago (id, nombre) FROM '$$PATH$$/3657.dat';

--
-- Data for Name: tipo_vehiculo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_vehiculo (id, nombre) FROM stdin;
\.
COPY public.tipo_vehiculo (id, nombre) FROM '$$PATH$$/3659.dat';

--
-- Data for Name: trabajo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.trabajo (id, cargo, id_persona, id_ocupacion, id_empresa, id_direccion) FROM stdin;
\.
COPY public.trabajo (id, cargo, id_persona, id_ocupacion, id_empresa, id_direccion) FROM '$$PATH$$/3661.dat';

--
-- Data for Name: ubicacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ubicacion (id, latitud, longitud) FROM stdin;
\.
COPY public.ubicacion (id, latitud, longitud) FROM '$$PATH$$/3667.dat';

--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (id, usuario, correo, contrasenia, foto, calificacion, estado, id_persona, token) FROM stdin;
\.
COPY public.usuario (id, usuario, correo, contrasenia, foto, calificacion, estado, id_persona, token) FROM '$$PATH$$/3669.dat';

--
-- Data for Name: usuario_ayuda_favor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario_ayuda_favor (id, id_usuario, id_favor, estado_aceptacion) FROM stdin;
\.
COPY public.usuario_ayuda_favor (id, id_usuario, id_favor, estado_aceptacion) FROM '$$PATH$$/3670.dat';

--
-- Data for Name: vehiculo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vehiculo (id, nombre, id_tipo_vehiculo) FROM stdin;
\.
COPY public.vehiculo (id, nombre, id_tipo_vehiculo) FROM '$$PATH$$/3676.dat';

--
-- Data for Name: vehiculo_persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vehiculo_persona (id, id_vehiculo, id_persona) FROM stdin;
\.
COPY public.vehiculo_persona (id, id_vehiculo, id_persona) FROM '$$PATH$$/3679.dat';

--
-- Name: calificacion_id_favor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.calificacion_id_favor_seq', 1, false);


--
-- Name: calificacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.calificacion_id_seq', 1, false);


--
-- Name: calificacion_id_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.calificacion_id_usuario_seq', 1, false);


--
-- Name: categoria_filtro_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categoria_filtro_id_seq', 1, false);


--
-- Name: ciudad_id_pais_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ciudad_id_pais_seq', 1, false);


--
-- Name: ciudad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ciudad_id_seq', 1, false);


--
-- Name: detalle_tipo_pago_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detalle_tipo_pago_id_seq', 1, false);


--
-- Name: detalle_tipo_pago_id_tipo_pago_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detalle_tipo_pago_id_tipo_pago_seq', 1, false);


--
-- Name: direccion_id_ciudad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.direccion_id_ciudad_seq', 1, false);


--
-- Name: direccion_id_persona_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.direccion_id_persona_seq', 1, false);


--
-- Name: direccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.direccion_id_seq', 1, false);


--
-- Name: direccion_id_ubicacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.direccion_id_ubicacion_seq', 1, false);


--
-- Name: empresa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.empresa_id_seq', 1, false);


--
-- Name: favor_id_detalle_tipo_pago_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.favor_id_detalle_tipo_pago_seq', 1, false);


--
-- Name: favor_id_direccion_favor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.favor_id_direccion_favor_seq', 1, false);


--
-- Name: favor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.favor_id_seq', 1, false);


--
-- Name: favor_id_usuario_realiza_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.favor_id_usuario_realiza_seq', 1, false);


--
-- Name: favor_id_usuario_solicita_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.favor_id_usuario_solicita_seq', 1, false);


--
-- Name: filtro_configuracion_usuario_id_filtro_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_configuracion_usuario_id_filtro_seq', 1, false);


--
-- Name: filtro_configuracion_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_configuracion_usuario_id_seq', 1, false);


--
-- Name: filtro_configuracion_usuario_id_tabla_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_configuracion_usuario_id_tabla_seq', 1, false);


--
-- Name: filtro_configuracion_usuario_id_tipo_configuracion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_configuracion_usuario_id_tipo_configuracion_seq', 1, false);


--
-- Name: filtro_configuracion_usuario_id_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_configuracion_usuario_id_usuario_seq', 1, false);


--
-- Name: filtro_favor_id_favor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_favor_id_favor_seq', 1, false);


--
-- Name: filtro_favor_id_filtro_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_favor_id_filtro_seq', 1, false);


--
-- Name: filtro_favor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_favor_id_seq', 1, false);


--
-- Name: filtro_favor_id_tabla_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_favor_id_tabla_seq', 1, false);


--
-- Name: filtro_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_id_seq', 1, false);


--
-- Name: filtro_persona_id_filtro_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_persona_id_filtro_seq', 1, false);


--
-- Name: filtro_persona_id_persona_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_persona_id_persona_seq', 1, false);


--
-- Name: filtro_persona_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_persona_id_seq', 1, false);


--
-- Name: filtro_persona_id_tabla_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filtro_persona_id_tabla_seq', 1, false);


--
-- Name: nacionalidad_id_pais_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nacionalidad_id_pais_seq', 1, false);


--
-- Name: nacionalidad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nacionalidad_id_seq', 1, false);


--
-- Name: nivel_estudios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nivel_estudios_id_seq', 1, false);


--
-- Name: ocupacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ocupacion_id_seq', 1, false);


--
-- Name: pais_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pais_id_seq', 4, true);


--
-- Name: persona_id_nacionalidad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_id_nacionalidad_seq', 1, false);


--
-- Name: persona_id_nivel_estudios_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_id_nivel_estudios_seq', 1, false);


--
-- Name: persona_id_ocupacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_id_ocupacion_seq', 1, false);


--
-- Name: persona_id_profesion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_id_profesion_seq', 1, false);


--
-- Name: persona_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_id_seq', 18, true);


--
-- Name: persona_id_tipo_licencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_id_tipo_licencia_seq', 1, false);


--
-- Name: profesion_id_nivel_estudios_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.profesion_id_nivel_estudios_seq', 1, false);


--
-- Name: profesion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.profesion_id_seq', 1, false);


--
-- Name: tipo_configuracion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_configuracion_id_seq', 1, false);


--
-- Name: tipo_licencia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_licencia_id_seq', 1, false);


--
-- Name: tipo_pago_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_pago_id_seq', 1, false);


--
-- Name: tipo_vehiculo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_vehiculo_id_seq', 1, false);


--
-- Name: trabajo_id_direccion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trabajo_id_direccion_seq', 1, false);


--
-- Name: trabajo_id_empresa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trabajo_id_empresa_seq', 1, false);


--
-- Name: trabajo_id_ocupacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trabajo_id_ocupacion_seq', 1, false);


--
-- Name: trabajo_id_persona_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trabajo_id_persona_seq', 1, false);


--
-- Name: trabajo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trabajo_id_seq', 1, false);


--
-- Name: ubicacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ubicacion_id_seq', 1, false);


--
-- Name: usuario_ayuda_favor_id_favor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_ayuda_favor_id_favor_seq', 1, false);


--
-- Name: usuario_ayuda_favor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_ayuda_favor_id_seq', 1, false);


--
-- Name: usuario_ayuda_favor_id_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_ayuda_favor_id_usuario_seq', 1, false);


--
-- Name: usuario_id_persona_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_id_persona_seq', 1, false);


--
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_id_seq', 16, true);


--
-- Name: vehiculo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vehiculo_id_seq', 1, false);


--
-- Name: vehiculo_id_tipo_vehiculo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vehiculo_id_tipo_vehiculo_seq', 1, false);


--
-- Name: vehiculo_persona_id_persona_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vehiculo_persona_id_persona_seq', 1, false);


--
-- Name: vehiculo_persona_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vehiculo_persona_id_seq', 1, false);


--
-- Name: vehiculo_persona_id_vehiculo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vehiculo_persona_id_vehiculo_seq', 1, false);


--
-- Name: calificacion calificacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calificacion
    ADD CONSTRAINT calificacion_pkey PRIMARY KEY (id);


--
-- Name: categoria_filtro categoria_filtro_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria_filtro
    ADD CONSTRAINT categoria_filtro_nombre_key UNIQUE (nombre);


--
-- Name: categoria_filtro categoria_filtro_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria_filtro
    ADD CONSTRAINT categoria_filtro_pkey PRIMARY KEY (id);


--
-- Name: ciudad ciudad_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ciudad
    ADD CONSTRAINT ciudad_nombre_key UNIQUE (nombre);


--
-- Name: ciudad ciudad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ciudad
    ADD CONSTRAINT ciudad_pkey PRIMARY KEY (id);


--
-- Name: direccion direccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.direccion
    ADD CONSTRAINT direccion_pkey PRIMARY KEY (id);


--
-- Name: empresa empresa_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT empresa_nombre_key UNIQUE (nombre);


--
-- Name: empresa empresa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT empresa_pkey PRIMARY KEY (id);


--
-- Name: favor favor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.favor
    ADD CONSTRAINT favor_pkey PRIMARY KEY (id);


--
-- Name: filtro_configuracion_usuario filtro_configuracion_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_configuracion_usuario
    ADD CONSTRAINT filtro_configuracion_usuario_pkey PRIMARY KEY (id);


--
-- Name: filtro_favor filtro_favor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_favor
    ADD CONSTRAINT filtro_favor_pkey PRIMARY KEY (id);


--
-- Name: filtro filtro_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro
    ADD CONSTRAINT filtro_nombre_key UNIQUE (nombre);


--
-- Name: filtro_persona filtro_persona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_persona
    ADD CONSTRAINT filtro_persona_pkey PRIMARY KEY (id);


--
-- Name: filtro filtro_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro
    ADD CONSTRAINT filtro_pkey PRIMARY KEY (id);


--
-- Name: nacionalidad nacionalidad_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nacionalidad
    ADD CONSTRAINT nacionalidad_nombre_key UNIQUE (nombre);


--
-- Name: nacionalidad nacionalidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nacionalidad
    ADD CONSTRAINT nacionalidad_pkey PRIMARY KEY (id);


--
-- Name: nivel_estudios nivel_estudios_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel_estudios
    ADD CONSTRAINT nivel_estudios_nombre_key UNIQUE (nombre);


--
-- Name: nivel_estudios nivel_estudios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel_estudios
    ADD CONSTRAINT nivel_estudios_pkey PRIMARY KEY (id);


--
-- Name: ocupacion ocupacion_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ocupacion
    ADD CONSTRAINT ocupacion_nombre_key UNIQUE (nombre);


--
-- Name: ocupacion ocupacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ocupacion
    ADD CONSTRAINT ocupacion_pkey PRIMARY KEY (id);


--
-- Name: pais pais_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pais
    ADD CONSTRAINT pais_nombre_key UNIQUE (nombre);


--
-- Name: pais pais_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pais
    ADD CONSTRAINT pais_pkey PRIMARY KEY (id);


--
-- Name: persona persona_identificación_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT "persona_identificación_key" UNIQUE (identificacion);


--
-- Name: persona persona_licencia_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_licencia_key UNIQUE (licencia);


--
-- Name: persona persona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_pkey PRIMARY KEY (id);


--
-- Name: profesion profesion_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profesion
    ADD CONSTRAINT profesion_nombre_key UNIQUE (nombre);


--
-- Name: profesion profesion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profesion
    ADD CONSTRAINT profesion_pkey PRIMARY KEY (id);


--
-- Name: tipo_configuracion tipo_configuracion_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_configuracion
    ADD CONSTRAINT tipo_configuracion_nombre_key UNIQUE (nombre);


--
-- Name: tipo_configuracion tipo_configuracion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_configuracion
    ADD CONSTRAINT tipo_configuracion_pkey PRIMARY KEY (id);


--
-- Name: tipo_licencia tipo_licencia_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_licencia
    ADD CONSTRAINT tipo_licencia_nombre_key UNIQUE (nombre);


--
-- Name: tipo_licencia tipo_licencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_licencia
    ADD CONSTRAINT tipo_licencia_pkey PRIMARY KEY (id);


--
-- Name: detalle_tipo_pago tipo_pago_favor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_tipo_pago
    ADD CONSTRAINT tipo_pago_favor_pkey PRIMARY KEY (id);


--
-- Name: tipo_pago tipo_pago_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_pago
    ADD CONSTRAINT tipo_pago_nombre_key UNIQUE (nombre);


--
-- Name: tipo_pago tipo_pago_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_pago
    ADD CONSTRAINT tipo_pago_pkey PRIMARY KEY (id);


--
-- Name: tipo_vehiculo tipo_vehiculo_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_vehiculo
    ADD CONSTRAINT tipo_vehiculo_nombre_key UNIQUE (nombre);


--
-- Name: tipo_vehiculo tipo_vehiculo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_vehiculo
    ADD CONSTRAINT tipo_vehiculo_pkey PRIMARY KEY (id);


--
-- Name: trabajo trabajo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo
    ADD CONSTRAINT trabajo_pkey PRIMARY KEY (id);


--
-- Name: ubicacion ubicacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion
    ADD CONSTRAINT ubicacion_pkey PRIMARY KEY (id);


--
-- Name: empresa uk2fqlxbcs4h827hio1qam0dhd3; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT uk2fqlxbcs4h827hio1qam0dhd3 UNIQUE (nombre);


--
-- Name: tipo_pago uk2jn8asd6licmlekochs3wr069; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_pago
    ADD CONSTRAINT uk2jn8asd6licmlekochs3wr069 UNIQUE (nombre);


--
-- Name: usuario uk2mlfr087gb1ce55f2j87o74t; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT uk2mlfr087gb1ce55f2j87o74t UNIQUE (correo);


--
-- Name: usuario uk33gathdlc33wn52w45op1r397; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT uk33gathdlc33wn52w45op1r397 UNIQUE (id_persona);


--
-- Name: vehiculo uk8k33brk0t0gtg3mvh7aup9asd; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehiculo
    ADD CONSTRAINT uk8k33brk0t0gtg3mvh7aup9asd UNIQUE (nombre);


--
-- Name: profesion uk9g13ixelirooj3ohc6jmk4elh; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profesion
    ADD CONSTRAINT uk9g13ixelirooj3ohc6jmk4elh UNIQUE (nombre);


--
-- Name: usuario uk_33gathdlc33wn52w45op1r397; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT uk_33gathdlc33wn52w45op1r397 UNIQUE (id_persona);


--
-- Name: nacionalidad ukai2qjhipbere5q56o9nn2k4oi; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nacionalidad
    ADD CONSTRAINT ukai2qjhipbere5q56o9nn2k4oi UNIQUE (nombre);


--
-- Name: tipo_vehiculo ukcvjjknqwylrwelqbudcg86aug; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_vehiculo
    ADD CONSTRAINT ukcvjjknqwylrwelqbudcg86aug UNIQUE (nombre);


--
-- Name: tipo_licencia ukdk1ut3f3y7xlp92x4wkpmxeqw; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_licencia
    ADD CONSTRAINT ukdk1ut3f3y7xlp92x4wkpmxeqw UNIQUE (nombre);


--
-- Name: ciudad ukgq6hfkl2jwepoa130me3x7iak; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ciudad
    ADD CONSTRAINT ukgq6hfkl2jwepoa130me3x7iak UNIQUE (nombre);


--
-- Name: filtro ukhtefuwbbsxlw16b20wufqwea0; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro
    ADD CONSTRAINT ukhtefuwbbsxlw16b20wufqwea0 UNIQUE (nombre);


--
-- Name: usuario uki02kr8ui5pqddyd7pkm3v4jbt; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT uki02kr8ui5pqddyd7pkm3v4jbt UNIQUE (usuario);


--
-- Name: ocupacion ukk5ah1k0qtb4pt5aokp0y53qc0; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ocupacion
    ADD CONSTRAINT ukk5ah1k0qtb4pt5aokp0y53qc0 UNIQUE (nombre);


--
-- Name: categoria_filtro ukkfre22hnhgjf8fg6at2nu6vbi; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria_filtro
    ADD CONSTRAINT ukkfre22hnhgjf8fg6at2nu6vbi UNIQUE (nombre);


--
-- Name: tipo_configuracion uklg2j2ii1muhc8gigjuey9lj95; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_configuracion
    ADD CONSTRAINT uklg2j2ii1muhc8gigjuey9lj95 UNIQUE (nombre);


--
-- Name: nivel_estudios ukome0q800iwkvkpqhgwboln8e2; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel_estudios
    ADD CONSTRAINT ukome0q800iwkvkpqhgwboln8e2 UNIQUE (nombre);


--
-- Name: persona ukox6qnkh1rllqubpew7pv0kblw; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT ukox6qnkh1rllqubpew7pv0kblw UNIQUE (licencia);


--
-- Name: persona uktr8fh0es1l6g7l2b688jykaqq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT uktr8fh0es1l6g7l2b688jykaqq UNIQUE (identificacion);


--
-- Name: usuario_ayuda_favor usuario_ayuda_favor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_ayuda_favor
    ADD CONSTRAINT usuario_ayuda_favor_pkey PRIMARY KEY (id);


--
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- Name: vehiculo vehiculo_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehiculo
    ADD CONSTRAINT vehiculo_nombre_key UNIQUE (nombre);


--
-- Name: vehiculo_persona vehiculo_persona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehiculo_persona
    ADD CONSTRAINT vehiculo_persona_pkey PRIMARY KEY (id);


--
-- Name: vehiculo vehiculo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehiculo
    ADD CONSTRAINT vehiculo_pkey PRIMARY KEY (id);


--
-- Name: usuario_correo_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX usuario_correo_uindex ON public.usuario USING btree (correo);


--
-- Name: usuario_id_persona_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX usuario_id_persona_uindex ON public.usuario USING btree (id_persona);


--
-- Name: usuario_usuario_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX usuario_usuario_uindex ON public.usuario USING btree (usuario);


--
-- Name: nacionalidad _id_pais_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nacionalidad
    ADD CONSTRAINT _id_pais_fk FOREIGN KEY (id_pais) REFERENCES public.pais(id);


--
-- Name: calificacion calificacion_id_favor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calificacion
    ADD CONSTRAINT calificacion_id_favor_fk FOREIGN KEY (id_favor) REFERENCES public.favor(id);


--
-- Name: calificacion calificacion_id_usuario_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calificacion
    ADD CONSTRAINT calificacion_id_usuario_fk FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);


--
-- Name: ciudad ciudad_pais_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ciudad
    ADD CONSTRAINT ciudad_pais_id_fk FOREIGN KEY (id_pais) REFERENCES public.pais(id);


--
-- Name: detalle_tipo_pago detalle_tipo_pago_id_tipo_pago_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_tipo_pago
    ADD CONSTRAINT detalle_tipo_pago_id_tipo_pago_fk FOREIGN KEY (id_tipo_pago) REFERENCES public.tipo_pago(id);


--
-- Name: direccion direccion_persona_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.direccion
    ADD CONSTRAINT direccion_persona_id_fk FOREIGN KEY (id_persona) REFERENCES public.persona(id);


--
-- Name: favor favor_id_detalle_tipo_pago_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.favor
    ADD CONSTRAINT favor_id_detalle_tipo_pago_fk FOREIGN KEY (id_detalle_tipo_pago) REFERENCES public.detalle_tipo_pago(id);


--
-- Name: favor favor_id_direccion_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.favor
    ADD CONSTRAINT favor_id_direccion_fk FOREIGN KEY (id_direccion_favor) REFERENCES public.direccion(id);


--
-- Name: favor favor_id_usuario_realiza_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.favor
    ADD CONSTRAINT favor_id_usuario_realiza_fk FOREIGN KEY (id_usuario_realiza) REFERENCES public.usuario(id);


--
-- Name: favor favor_id_usuario_solicita_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.favor
    ADD CONSTRAINT favor_id_usuario_solicita_fk FOREIGN KEY (id_usuario_solicita) REFERENCES public.usuario(id);


--
-- Name: filtro filtro_categoria_filtro_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro
    ADD CONSTRAINT filtro_categoria_filtro_id_fk FOREIGN KEY (id_categoria_filtro) REFERENCES public.categoria_filtro(id);


--
-- Name: filtro_configuracion_usuario filtro_configuracion_usuario_id_filtro_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_configuracion_usuario
    ADD CONSTRAINT filtro_configuracion_usuario_id_filtro_fk FOREIGN KEY (id_filtro) REFERENCES public.filtro(id);


--
-- Name: filtro_configuracion_usuario filtro_configuracion_usuario_id_tipo_configuracion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_configuracion_usuario
    ADD CONSTRAINT filtro_configuracion_usuario_id_tipo_configuracion FOREIGN KEY (id_tipo_configuracion) REFERENCES public.tipo_configuracion(id);


--
-- Name: filtro_configuracion_usuario filtro_configuracion_usuario_id_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_configuracion_usuario
    ADD CONSTRAINT filtro_configuracion_usuario_id_usuario FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);


--
-- Name: filtro_favor filtro_favor_id_favor; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_favor
    ADD CONSTRAINT filtro_favor_id_favor FOREIGN KEY (id_favor) REFERENCES public.favor(id);


--
-- Name: filtro_favor filtro_favor_id_filtro_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_favor
    ADD CONSTRAINT filtro_favor_id_filtro_fk FOREIGN KEY (id_filtro) REFERENCES public.filtro(id);


--
-- Name: filtro_persona filtro_persona_id_filtro_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_persona
    ADD CONSTRAINT filtro_persona_id_filtro_fk FOREIGN KEY (id_filtro) REFERENCES public.filtro(id);


--
-- Name: filtro_persona filtro_persona_id_persona_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filtro_persona
    ADD CONSTRAINT filtro_persona_id_persona_fk FOREIGN KEY (id_persona) REFERENCES public.persona(id);


--
-- Name: direccion id_ciudad_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.direccion
    ADD CONSTRAINT id_ciudad_fk FOREIGN KEY (id_ciudad) REFERENCES public.ciudad(id);


--
-- Name: profesion id_nivel_estudios_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profesion
    ADD CONSTRAINT id_nivel_estudios_fk FOREIGN KEY (id_nivel_estudios) REFERENCES public.nivel_estudios(id);


--
-- Name: direccion id_ubicacion_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.direccion
    ADD CONSTRAINT id_ubicacion_fk FOREIGN KEY (id_ubicacion) REFERENCES public.ubicacion(id);


--
-- Name: persona persona_id_nacionalidad_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_id_nacionalidad_fk FOREIGN KEY (id_nacionalidad) REFERENCES public.nacionalidad(id);


--
-- Name: persona persona_id_nivel_estudios_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_id_nivel_estudios_fk FOREIGN KEY (id_nivel_estudios) REFERENCES public.nivel_estudios(id);


--
-- Name: persona persona_id_ocupacion_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_id_ocupacion_fk FOREIGN KEY (id_ocupacion) REFERENCES public.ocupacion(id);


--
-- Name: persona persona_id_profesion_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_id_profesion_fk FOREIGN KEY (id_profesion) REFERENCES public.profesion(id);


--
-- Name: persona persona_id_tipo_lincencia_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_id_tipo_lincencia_fk FOREIGN KEY (id_tipo_licencia) REFERENCES public.tipo_licencia(id);


--
-- Name: trabajo trabajo_id_direccion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo
    ADD CONSTRAINT trabajo_id_direccion FOREIGN KEY (id_direccion) REFERENCES public.direccion(id);


--
-- Name: trabajo trabajo_id_empresa_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo
    ADD CONSTRAINT trabajo_id_empresa_fk FOREIGN KEY (id_empresa) REFERENCES public.empresa(id);


--
-- Name: trabajo trabajo_id_ocupacion_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo
    ADD CONSTRAINT trabajo_id_ocupacion_fk FOREIGN KEY (id_ocupacion) REFERENCES public.ocupacion(id);


--
-- Name: trabajo trabajo_id_persona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo
    ADD CONSTRAINT trabajo_id_persona FOREIGN KEY (id_persona) REFERENCES public.persona(id);


--
-- Name: usuario_ayuda_favor usuario_ayuda_favor_id_favor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_ayuda_favor
    ADD CONSTRAINT usuario_ayuda_favor_id_favor_fk FOREIGN KEY (id_favor) REFERENCES public.favor(id);


--
-- Name: usuario_ayuda_favor usuario_ayuda_favor_id_usuario_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_ayuda_favor
    ADD CONSTRAINT usuario_ayuda_favor_id_usuario_fk FOREIGN KEY (id_usuario) REFERENCES public.usuario(id);


--
-- Name: usuario usuario_persona_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_persona_id_fk FOREIGN KEY (id_persona) REFERENCES public.persona(id);


--
-- Name: vehiculo_persona vehiculo_persona_id_persona_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehiculo_persona
    ADD CONSTRAINT vehiculo_persona_id_persona_fk FOREIGN KEY (id_persona) REFERENCES public.persona(id);


--
-- Name: vehiculo_persona vehiculo_persona_id_vehiculo_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehiculo_persona
    ADD CONSTRAINT vehiculo_persona_id_vehiculo_fk FOREIGN KEY (id_vehiculo) REFERENCES public.vehiculo(id);


--
-- Name: vehiculo vehiculo_tipo_vehiculo_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehiculo
    ADD CONSTRAINT vehiculo_tipo_vehiculo_id_fk FOREIGN KEY (id_tipo_vehiculo) REFERENCES public.tipo_vehiculo(id);


--
-- PostgreSQL database dump complete
--

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           