create table if not exists categoria_filtro
(
	id bigserial not null
		constraint categoria_filtro_pkey
			primary key,
	nombre varchar(30)
)
;

alter table categoria_filtro owner to postgres
;

create table if not exists empresa
(
	id bigserial not null
		constraint empresa_pkey
			primary key,
	nombre varchar(50)
)
;

alter table empresa owner to postgres
;

create table if not exists filtro
(
	id bigserial not null
		constraint filtro_pkey
			primary key,
	nombre varchar(20),
	descripcion varchar(50),
	categoria_filtro bigint
		constraint filtro_categoria_filtro_id_fk
			references categoria_filtro
)
;

alter table filtro owner to postgres
;

create table if not exists nivel_estudios
(
	id bigserial not null
		constraint nivel_estudios_pkey
			primary key,
	nombre varchar(30)
)
;

alter table nivel_estudios owner to postgres
;

create table if not exists ocupacion
(
	id bigserial not null
		constraint ocupacion_pkey
			primary key,
	nombre varchar(30)
)
;

alter table ocupacion owner to postgres
;

create table if not exists pais
(
	id bigserial not null
		constraint pais_pkey
			primary key,
	nombre varchar(30)
)
;

alter table pais owner to postgres
;

create table if not exists ciudad
(
	id bigserial not null
		constraint ciudad_pkey
			primary key,
	nombre varchar(30),
	pais bigint
		constraint ciudad_pais_id_fk
			references pais
)
;

alter table ciudad owner to postgres
;

create table if not exists nacionalidad
(
	id bigserial not null
		constraint nacionalidad_pkey
			primary key,
	nombre varchar(30),
	pais bigint
		constraint "_id_pais_fk"
			references pais
)
;

alter table nacionalidad owner to postgres
;

create table if not exists profesion
(
	id bigserial not null
		constraint profesion_pkey
			primary key,
	nombre varchar(60),
	nivel_estudios bigint
		constraint id_nivel_estudios_fk
			references nivel_estudios
)
;

alter table profesion owner to postgres
;

create table if not exists tipo_configuracion
(
	id bigserial not null
		constraint tipo_configuracion_pkey
			primary key,
	nombre varchar(30)
)
;

alter table tipo_configuracion owner to postgres
;

create table if not exists tipo_licencia
(
	id bigserial not null
		constraint tipo_licencia_pkey
			primary key,
	nombre varchar(30)
)
;

alter table tipo_licencia owner to postgres
;

create table if not exists persona
(
	id bigserial not null
		constraint persona_pkey
			primary key,
	identificacion varchar(20)
		constraint "persona_identificación_key"
			unique,
	nombres_apellidos varchar(50),
	telefono varchar(10),
	licencia varchar(15),
	tipo_licencia bigint
		constraint persona_id_tipo_lincencia_fk
			references tipo_licencia,
	nacionalidad bigint
		constraint persona_id_nacionalidad_fk
			references nacionalidad,
	nivel_estudios bigint
		constraint persona_id_nivel_estudios_fk
			references nivel_estudios,
	profesion bigint
		constraint persona_id_profesion_fk
			references profesion,
	ocupacion bigint
		constraint persona_id_ocupacion_fk
			references ocupacion,
	informacion_adicional varchar(250)
)
;

alter table persona owner to postgres
;

create table if not exists filtro_persona
(
	id bigserial not null
		constraint filtro_persona_pkey
			primary key,
	filtro bigint
		constraint filtro_persona_id_filtro_fk
			references filtro,
	persona bigint
		constraint filtro_persona_id_persona_fk
			references persona,
	valor_string varchar(20),
	valor_int integer,
	tabla bigint,
	estado varchar(20)
)
;

alter table filtro_persona owner to postgres
;

create table if not exists tipo_pago
(
	id bigserial not null
		constraint tipo_pago_pkey
			primary key,
	nombre varchar(30)
)
;

alter table tipo_pago owner to postgres
;

create table if not exists detalle_tipo_pago
(
	id bigserial not null
		constraint tipo_pago_favor_pkey
			primary key,
	tipo_pago bigint
		constraint detalle_tipo_pago_id_tipo_pago_fk
			references tipo_pago,
	valor double precision,
	descripcion varchar(50)
)
;

alter table detalle_tipo_pago owner to postgres
;

create table if not exists tipo_vehiculo
(
	id bigserial not null
		constraint tipo_vehiculo_pkey
			primary key,
	nombre varchar(30)
)
;

alter table tipo_vehiculo owner to postgres
;

create table if not exists ubicacion
(
	id bigserial not null
		constraint ubicacion_pkey
			primary key,
	latitud double precision,
	longitud double precision
)
;

alter table ubicacion owner to postgres
;

create table if not exists direccion
(
	id bigserial not null
		constraint direccion_pkey
			primary key,
	calle_principal varchar(30),
	calle_secundaria varchar(30),
	barrio varchar(30),
	numero_casa varchar(20),
	referencia varchar(120),
	ciudad bigint
		constraint id_ciudad_fk
			references ciudad,
	ubicacion bigint
		constraint id_ubicacion_fk
			references ubicacion,
	persona bigint
		constraint direccion_persona_id_fk
			references persona
)
;

alter table direccion owner to postgres
;

create table if not exists trabajo
(
	id bigserial not null
		constraint trabajo_pkey
			primary key,
	cargo varchar(50),
	persona bigint
		constraint trabajo_id_persona
			references persona,
	ocupacion bigint
		constraint trabajo_id_ocupacion_fk
			references ocupacion,
	empresa bigint
		constraint trabajo_id_empresa_fk
			references empresa,
	direccion bigint
		constraint trabajo_id_direccion
			references direccion
)
;

alter table trabajo owner to postgres
;

create table if not exists usuario
(
	id bigserial not null
		constraint usuario_pkey
			primary key,
	usuario varchar(20),
	correo varchar(20),
	contrasenia varchar(12),
	foto bytea,
	calificacion integer,
	estado integer,
	persona bigint
		constraint usuario_persona_id_fk
			references persona,
	token varchar(250)
)
;

alter table usuario owner to postgres
;

create table if not exists favor
(
	id bigserial not null
		constraint favor_pkey
			primary key,
	titulo varchar(20),
	descripcion varchar(150),
	estado varchar(20),
	fecha_solicita timestamp,
	fecha_realiza timestamp,
	direccion_favor bigint
		constraint favor_id_direccion_fk
			references direccion,
	detalle_tipo_pago bigint
		constraint favor_id_detalle_tipo_pago_fk
			references detalle_tipo_pago,
	usuario_solicita bigint
		constraint favor_id_usuario_solicita_fk
			references usuario,
	usuario_realiza bigint
		constraint favor_id_usuario_realiza_fk
			references usuario
)
;

alter table favor owner to postgres
;

create table if not exists calificacion
(
	id bigserial not null
		constraint calificacion_pkey
			primary key,
	calificacion integer,
	usuario bigint
		constraint calificacion_id_usuario_fk
			references usuario,
	favor bigint
		constraint calificacion_id_favor_fk
			references favor
)
;

alter table calificacion owner to postgres
;

create table if not exists filtro_configuracion_usuario
(
	id bigserial not null
		constraint filtro_configuracion_usuario_pkey
			primary key,
	filtro bigint
		constraint filtro_configuracion_usuario_id_filtro_fk
			references filtro,
	tipo_configuracion bigint
		constraint filtro_configuracion_usuario_id_tipo_configuracion
			references tipo_configuracion,
	usuario bigint
		constraint filtro_configuracion_usuario_id_usuario
			references usuario,
	valor_string varchar(20),
	valor_int integer,
	tabla bigint,
	estado varchar(20)
)
;

alter table filtro_configuracion_usuario owner to postgres
;

create table if not exists filtro_favor
(
	id bigserial not null
		constraint filtro_favor_pkey
			primary key,
	filtro bigint
		constraint filtro_favor_id_filtro_fk
			references filtro,
	favor bigint
		constraint filtro_favor_id_favor
			references favor,
	valor_string varchar(20),
	valor_int integer,
	tabla bigint,
	estado varchar(20)
)
;

alter table filtro_favor owner to postgres
;

create table if not exists usuario_ayuda_favor
(
	id bigserial not null
		constraint usuario_ayuda_favor_pkey
			primary key,
	usuario bigint
		constraint usuario_ayuda_favor_id_usuario_fk
			references usuario,
	favor bigint
		constraint usuario_ayuda_favor_id_favor_fk
			references favor,
	estado_aceptacion varchar(20)
)
;

alter table usuario_ayuda_favor owner to postgres
;

create table if not exists vehiculo
(
	id bigserial not null
		constraint vehiculo_pkey
			primary key,
	nombre varchar(30),
	tipo_vehiculo bigint
		constraint vehiculo_tipo_vehiculo_id_fk
			references tipo_vehiculo,
	placa varchar(10)
)
;

alter table vehiculo owner to postgres
;

create table if not exists vehiculo_persona
(
	id bigserial not null
		constraint vehiculo_persona_pkey
			primary key,
	vehiculo bigint
		constraint vehiculo_persona_id_vehiculo_fk
			references vehiculo,
	persona bigint
		constraint vehiculo_persona_id_persona_fk
			references persona
)
;

alter table vehiculo_persona owner to postgres
;

