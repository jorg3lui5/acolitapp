import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cabecera',
  templateUrl: './cabecera.page.html',
  styleUrls: ['./cabecera.page.scss'],
})
export class CabeceraPage implements OnInit {

  @Input() titulo:string;

  constructor() { }

  ngOnInit() {
    console.log(this.titulo);
  }

}
