import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FCM } from '@ionic-native/fcm/ngx';
import { LoginPageModule } from './login/login.module'
import { HttpClientModule } from '@angular/common/http';
import { EnvioNotificacionesService } from './services/envio-notificaciones/envio-notificaciones.service';
import { IonicStorageModule } from '@ionic/storage';
import { HTTP } from '@ionic-native/http/ngx';
import { CalificacionService } from './services/calificacion.service';
import { CategoriaFiltroService } from './services/categoriaFiltro.service';
import { CiudadService } from './services/ciudad.service';
import { DetalleTipoPagoService } from './services/detalleTipoPago.service';
import { DireccionService } from './services/direccion.service';
import { EmpresaService } from './services/empresa.service';
import { EnvioNotificacionService } from './services/envioNotificacion.service';
import { FavorService } from './services/favor.service';
import { FiltroService } from './services/filtro.service';
import { FiltroConfiguracionUsuarioService } from './services/filtroConfiguracionUsuario.service';
import { FiltroFavorService } from './services/filtroFavor.service';
import { FiltroPersonaService } from './services/filtroPersona.service';
import { NacionalidadService } from './services/nacionalidad.service';
import { NivelEstudiosService } from './services/nivelEstudios.service';
import { OcupacionService } from './services/ocupacion.service';
import { PaisService } from './services/pais.service';
import { PersonaService } from './services/persona.service';
import { ProfesionService } from './services/profesion.service';
import { TipoConfiguracionService } from './services/tipoConfiguracion.service';
import { TipoLicenciaService } from './services/tipoLicencia.service';
import { TipoPagoService } from './services/tipoPago.service';
import { TipoVehiculoService } from './services/tipoVehiculo.service';
import { TrabajoService } from './services/trabajo.service';
import { UbicacionService } from './services/ubicacion.service';
import { UsuarioService } from './services/usuario.service';
import { UsuarioAyudaFavorService } from './services/usuarioAyudaFavor.service';
import { VehiculoService } from './services/vehiculo.service';
import { VehiculoPersonaService } from './services/vehiculoPersona.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents: [

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    LoginPageModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    
  ],
  providers: [
    FCM,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    EnvioNotificacionesService,
    CalificacionService,
    CategoriaFiltroService,
    CiudadService,
    DetalleTipoPagoService,
    DireccionService,
    EmpresaService,
    EnvioNotificacionService,
    FavorService,
    FiltroService,
    FiltroConfiguracionUsuarioService,
    FiltroFavorService,
    FiltroPersonaService,
    NacionalidadService,
    NivelEstudiosService,
    OcupacionService,
    PaisService,
    PersonaService,
    ProfesionService,
    TipoConfiguracionService,
    TipoLicenciaService,
    TipoPagoService,
    TipoVehiculoService,
    TrabajoService,
    UbicacionService,
    UsuarioService,
    UsuarioAyudaFavorService,
    VehiculoService,
    VehiculoPersonaService,
    HTTP
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
