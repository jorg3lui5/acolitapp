import { Persona } from './persona';
import { DetalleTipoPago } from './detalleTipoPago';

export class TipoPago {
    id: number;
    nombre: string;
    detalleTipoPagoList:DetalleTipoPago[];
    
}