import { Persona } from './persona';
import { NivelEstudios } from './nivelEstudios';

export class Profesion {
    id: number;
    nombre: string;
    personaList: Persona[];
    nivelEstudios: NivelEstudios;

    
}