import { Persona } from './persona';

export class TipoLicencia {
    id: number;
    nombre: string;
    personaList: Persona[];
    
}