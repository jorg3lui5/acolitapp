import { Favor } from './favor';
import { Usuario } from './usuario';

export class Calificacion {
    id: number;
    calificacion: number;
    favor: Favor;
    usuario:Usuario;

    
}