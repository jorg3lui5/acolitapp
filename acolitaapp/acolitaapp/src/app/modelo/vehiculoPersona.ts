import { Persona } from './persona';
import { Vehiculo } from './vehiculo';

export class VehiculoPersona {
    id: number;
    persona: Persona;
    vehiculo: Vehiculo;
    
}