import { Persona } from './persona';
import { Filtro } from './filtro';

export class CategoriaFiltro {
    id: number;
    nombre: string;
    filtroList: Filtro[];    
}