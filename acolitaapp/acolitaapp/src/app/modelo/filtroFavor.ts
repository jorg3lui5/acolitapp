import { Persona } from './persona';
import { Favor } from './favor';
import { Filtro } from './filtro';

export class FiltroFavor {
    id: number;
    valorString: String;
    valorInt: number;
    tabla: number;
    estado: string;
    favor: Favor;
    filtro: Filtro;

    
}