import { Persona } from './persona';
import { Vehiculo } from './vehiculo';

export class TipoVehiculo {
    id: number;
    nombre: string;
    vehiculoList: Vehiculo[];
    
}