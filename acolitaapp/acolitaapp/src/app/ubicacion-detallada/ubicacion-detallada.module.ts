import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UbicacionDetalladaPageRoutingModule } from './ubicacion-detallada-routing.module';

import { UbicacionDetalladaPage } from './ubicacion-detallada.page';
import { CabeceraPageModule } from '../cabecera/cabecera.module'
import { PiePaginaPageModule } from '../pie-pagina/pie-pagina.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UbicacionDetalladaPageRoutingModule,
    CabeceraPageModule,
    PiePaginaPageModule
  ],
  declarations: [
    UbicacionDetalladaPage
  ],
  exports: [
    UbicacionDetalladaPage
  ],
})
export class UbicacionDetalladaPageModule {}
