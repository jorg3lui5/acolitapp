import { Component, OnInit } from '@angular/core';
import { Constantes } from '../compartido/constantes';
import { Router } from '@angular/router';
import { EnvioNotificacionesService } from '../services/envio-notificaciones/envio-notificaciones.service';
import { ActivatedRoute } from '@angular/router';
import { Usuario } from '../modelo/usuario';
import { ToastController } from '@ionic/angular';
import { UsuarioService } from '../services/usuario.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constantes: Constantes = new Constantes;
  usuarios:any;
  price: any = '';

  usuario: Usuario = new Usuario();

  constructor(
    private route: Router,
    public _envioNotificacionesService: EnvioNotificacionesService,
    public _usuarioService: UsuarioService,
    private router: ActivatedRoute,
    public toastController: ToastController,
    private storage: Storage
  ) { 
    this.price = this.router.snapshot.params['price'];
  }

  ngOnInit() {

    this._envioNotificacionesService.enviarNotificacion()
    .subscribe(
      (data)=> {
        this.usuarios=data;
      },
      (error)=>{
        console.log(error);
      }
    )
  }

  loguear(usuario, contrasenia) {
    if(usuario && contrasenia){
      this._usuarioService.loguear(usuario, contrasenia).subscribe(
        (data)=> {
          if(data && data.status=="OK"){
            if(data.objeto){
              this.storage.set('usuario', usuario);
              this.route.navigate(['/favores']);
            }
            else{
              this.mostrarMensaje(data.message);
            }
          }
          else{
            this.mostrarMensaje(data.message);
          }
        },
        (error)=>{
          console.log(error);
        }
      )
    }
    else{
      this.mostrarMensaje("Ingrese el usuario y contraseña");
    }
  }

  abrirRegistro() {
    this.route.navigate(['/registro']);
  }

  async mostrarMensaje(mensaje) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000
    });
    toast.present();
  }

}
