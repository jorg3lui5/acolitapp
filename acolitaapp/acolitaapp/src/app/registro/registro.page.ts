import { Component, OnInit } from '@angular/core';
import { Constantes } from '../compartido/constantes';
import { DatosPersonalesPage } from '../datos-personales/datos-personales.page';
import { NavController, ToastController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { Router } from '@angular/router';
import { Persona } from '../modelo/persona';
import { Usuario } from '../modelo/usuario';
import { UsuarioService } from '../services/usuario.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  constantes: Constantes = new Constantes;

  usuario: Usuario = new Usuario();

  contrasenia: string;

  constructor(
    private route: Router,
    public _usuarioService: UsuarioService,
    public toastController: ToastController,
    public navCtrl: NavController,
    private storage: Storage
    ) 
  {
      this.usuario.persona=new Persona();
  }

  ngOnInit() {
  }

  crearCuenta() {
    if(this.usuario.usuario && this.usuario.persona.identificacion && this.usuario.correo && this.usuario.contrasenia && this.contrasenia)
    {
      if(this.usuario.contrasenia==this.contrasenia){  
        this.cargarToken();
        this._usuarioService.crear(this.usuario).subscribe(
          (data)=> {
            if(data && data.status=="OK"){
              if(data.objeto){
                this.usuario=data.objeto;
                let parametros ={
                  nombreUsuario: JSON.stringify(this.usuario.usuario)
                }
                this.storage.set('usuario', this.usuario.usuario);
                // this.route.navigate(['/datos-personales']);
                this.navCtrl.navigateForward("/datos-personales",{queryParams: parametros});
              }
              else{
                this.mostrarMensaje("No se pudo crear el usuario");
              }
            }
            else{
              this.mostrarMensaje(data.message);
            }
          },
          (error)=>{
            console.log(error);
            this.mostrarMensaje(error);
          }
        )
      }
      else{
        this.mostrarMensaje("Las contraseñas no coinciden");
      }
    }
    else{
      this.mostrarMensaje("Llene todos los campos");
    }
  }

  cancelar() {
    this.route.navigate(['/login']);
  }

  async mostrarMensaje(mensaje) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000
    });
    toast.present();
  }
  
  cargarToken(){
    this.storage.get('token').then((val:string) => {
      this.usuario.token=val;
    });
  }
}
