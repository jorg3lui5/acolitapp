import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FavoresPage } from './favores.page';

describe('FavoresPage', () => {
  let component: FavoresPage;
  let fixture: ComponentFixture<FavoresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoresPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FavoresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
