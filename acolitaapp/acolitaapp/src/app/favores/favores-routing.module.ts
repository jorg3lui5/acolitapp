import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FavoresPage } from './favores.page';

const routes: Routes = [
  {
    path: '',
    component: FavoresPage,
    children: [
      {
        path: 'favores-solicitados',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../favores-solicitados/favores-solicitados.module').then(m => m.FavoresSolicitadosPageModule)
          }
        ]
      },
      {
        path: 'favores-realizados',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../favores-realizados/favores-realizados.module').then(m => m.FavoresRealizadosPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/favores/favores-solicitados',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/favores/favores-solicitados',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FavoresPageRoutingModule {}
