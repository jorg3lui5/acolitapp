import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PiePaginaPageRoutingModule } from './pie-pagina-routing.module';

import { PiePaginaPage } from './pie-pagina.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PiePaginaPageRoutingModule
  ],
  declarations: [
    PiePaginaPage
  ],
  exports: [
    PiePaginaPage
  ],
})
export class PiePaginaPageModule {}
