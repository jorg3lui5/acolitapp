import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PiePaginaPage } from './pie-pagina.page';

describe('PiePaginaPage', () => {
  let component: PiePaginaPage;
  let fixture: ComponentFixture<PiePaginaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PiePaginaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PiePaginaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
