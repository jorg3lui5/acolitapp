import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'cabecera',
    loadChildren: () => import('./cabecera/cabecera.module').then( m => m.CabeceraPageModule)
  },
  {
    path: 'menu-lateral',
    loadChildren: () => import('./menu-lateral/menu-lateral.module').then( m => m.MenuLateralPageModule)
  },
  {
    path: 'pie-pagina',
    loadChildren: () => import('./pie-pagina/pie-pagina.module').then( m => m.PiePaginaPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'login/:price',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registro',
    loadChildren: () => import('./registro/registro.module').then( m => m.RegistroPageModule)
  },
  {
    path: 'datos-personales',
    loadChildren: () => import('./datos-personales/datos-personales.module').then( m => m.DatosPersonalesPageModule)
  },
  {
    path: 'favores',
    loadChildren: () => import('./favores/favores.module').then( m => m.FavoresPageModule)
  },
  {
    path: 'solicitud',
    loadChildren: () => import('./solicitud/solicitud.module').then( m => m.SolicitudPageModule)
  },
  {
    path: 'notificaciones',
    loadChildren: () => import('./notificaciones/notificaciones.module').then( m => m.NotificacionesPageModule)
  },
  {
    path: 'favor-recibido',
    loadChildren: () => import('./favor-recibido/favor-recibido.module').then( m => m.FavorRecibidoPageModule)
  },
  {
    path: 'personas-disponibles',
    loadChildren: () => import('./personas-disponibles/personas-disponibles.module').then( m => m.PersonasDisponiblesPageModule)
  },
  {
    path: 'informacion-personal',
    loadChildren: () => import('./informacion-personal/informacion-personal.module').then( m => m.InformacionPersonalPageModule)
  },
  {
    path: 'ajustes-notificacion',
    loadChildren: () => import('./ajustes-notificacion/ajustes-notificacion.module').then( m => m.AjustesNotificacionPageModule)
  },
  {
    path: 'ajuste-favores',
    loadChildren: () => import('./ajuste-favores/ajuste-favores.module').then( m => m.AjusteFavoresPageModule)
  },
  {
    path: 'filtros',
    loadChildren: () => import('./filtros/filtros.module').then( m => m.FiltrosPageModule)
  },
  {
    path: 'ubicacion-actual',
    loadChildren: () => import('./ubicacion-actual/ubicacion-actual.module').then( m => m.UbicacionActualPageModule)
  },
  {
    path: 'ubicacion-detallada',
    loadChildren: () => import('./ubicacion-detallada/ubicacion-detallada.module').then( m => m.UbicacionDetalladaPageModule)
  },
  // {
  //   path: 'favores-realizados',
  //   loadChildren: () => import('./favores-realizados/favores-realizados.module').then( m => m.FavoresRealizadosPageModule)
  // },
  // {
  //   path: 'favores-solicitados',
  //   loadChildren: () => import('./favores-solicitados/favores-solicitados.module').then( m => m.FavoresSolicitadosPageModule)
  // },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
