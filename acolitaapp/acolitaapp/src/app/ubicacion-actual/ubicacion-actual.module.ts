import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UbicacionActualPageRoutingModule } from './ubicacion-actual-routing.module';

import { UbicacionActualPage } from './ubicacion-actual.page';
import { CabeceraPageModule } from '../cabecera/cabecera.module'
import { PiePaginaPageModule } from '../pie-pagina/pie-pagina.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UbicacionActualPageRoutingModule,
    CabeceraPageModule,
    PiePaginaPageModule
  ],
  declarations: [
    UbicacionActualPage
  ],
  exports: [
    UbicacionActualPage
  ],
})
export class UbicacionActualPageModule {}
