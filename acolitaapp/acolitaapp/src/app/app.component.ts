import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Constantes } from './compartido/constantes';
import { FCM } from '@ionic-native/fcm/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constantes: Constantes = new Constantes;

  public appPages = [
    {
      title: this.constantes._cuenta,
      url: '/registro',
      icon: 'person'
    },
    {
      title: this.constantes._notificaciones,
      url: '/notificaciones',
      icon: 'notifications'
    },
    {
      title: this.constantes._favores,
      url: '/favores',
      icon: 'hand'
    },
    {
      title: this.constantes._cerrarSesion,
      url: '/login',
      icon: 'log-out'
    }
  ];
  
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private fcm: FCM,
    private router: Router,
    private storage: Storage
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.fcm.getToken().then(token => {
        this.storage.set('token', token);
      });

      this.fcm.onTokenRefresh().subscribe(token => {
        console.log(token);
        this.storage.set('token', token);
      });

      this.fcm.onNotification().subscribe(data => {
        console.log(data);
        if (data.wasTapped) {
          console.log('Received in background');
          this.router.navigate([data.landing_page, data.price]);
        } else {
          console.log('Received in foreground');
          this.router.navigate([data.landing_page, data.price]);
        }
      });
    });

    this.storage.get('token').then((val) => {
      console.log(val);
    });
  }
}
