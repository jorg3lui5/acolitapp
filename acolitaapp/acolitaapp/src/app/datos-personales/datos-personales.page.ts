import { Component, OnInit } from '@angular/core';
import { Constantes } from '../compartido/constantes';
import { Usuario } from '../modelo/usuario';
import { UsuarioService } from '../services/usuario.service';
import { Direccion } from '../modelo/direccion';
import { Ciudad } from '../modelo/ciudad';
import { Pais } from '../modelo/pais';
import { PaisService } from '../services/pais.service';
import { CiudadService } from '../services/ciudad.service';
import { NacionalidadService } from '../services/nacionalidad.service';
import { Nacionalidad } from '../modelo/nacionalidad';
import { NivelEstudios } from '../modelo/nivelEstudios';
import { NivelEstudiosService } from '../services/nivelEstudios.service';
import { ProfesionService } from '../services/profesion.service';
import { Profesion } from '../modelo/profesion';
import { Ocupacion } from '../modelo/ocupacion';
import { OcupacionService } from '../services/ocupacion.service';
import { TipoVehiculo } from '../modelo/tipoVehiculo';
import { Persona } from '../modelo/persona';
import { ToastController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { TipoLicenciaService } from '../services/tipoLicencia.service';
import { TipoVehiculoService } from '../services/tipoVehiculo.service';
import { TipoLicencia } from '../modelo/tipoLicencia';
import { Vehiculo } from '../modelo/vehiculo';
import { VehiculoPersona } from '../modelo/vehiculoPersona';
import { PersonaService } from '../services/persona.service';

@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.page.html',
  styleUrls: ['./datos-personales.page.scss'],
})
export class DatosPersonalesPage implements OnInit {

  constantes: Constantes = new Constantes;
  nombreUsuario: string;
  paises: Pais[];
  ciudades: Ciudad[];
  nivelesEstudios: NivelEstudios[];
  profesiones: Profesion[];
  ocupaciones: Ocupacion[];
  tiposLicencia: TipoLicencia[];
  tiposVehiculos: TipoVehiculo[]

  pais: Pais;
  ciudad: Ciudad;
  tipoVehiculo: TipoVehiculo;
  placaVehiculo: string;
  persona: Persona=new Persona();


  constructor(
    private _usuarioService: UsuarioService,
    private _paisService: PaisService,
    private _ciudadService: CiudadService,
    private _nacionalidadService: NacionalidadService,
    private _nivelEstudiosService: NivelEstudiosService,
    private _profesionService: ProfesionService,
    private _ocupacionService: OcupacionService,
    public toastController: ToastController,
    public activatedRoute : ActivatedRoute,
    public _tipoLicenciaService:TipoLicenciaService,
    public _tipoVehiculoService:TipoVehiculoService,
    public _personaService: PersonaService,
    public navCtrl: NavController,
  )
  {
    // this.usuario=navParams.get('usuario');
    this.activatedRoute.queryParams.subscribe((res)=>{
      this.nombreUsuario=JSON.parse(res.nombreUsuario);
    });
  }

  ngOnInit() {
    if(this.nombreUsuario){
      this.recuperarPersonaPorNombreUsuario(this.nombreUsuario);
      this.recuperarNivelesEstudio();
      this.recuperarOcupaciones();
      this.recuperarPaises();
      this.recuperarTiposLicencia();
      this.recuperarTiposVehiculo();
    }
  }

  seleccionarPais(){
    this.ciudad=null;
    this.recuperarCiudadesPorIdPais(this.pais.id);
    this.recuperarNacionalidadPorIdPais(this.pais.id);
  }
  seleccionarNivelEstudios()
  {
    if(this.persona.nivelEstudios && this.persona.nivelEstudios.nombre=='Profesional'){
      this.recuperarProfesiones();
    }
  }

  finalizar(){

    if(this.persona.direccionList && this.persona.direccionList.length>0){
     this.persona.direccionList[0].ciudad=this.ciudad;
    }
    else{
        let direccion: Direccion=new Direccion();
        direccion.ciudad=this.ciudad;
        this.persona.direccionList=[];
        this.persona.direccionList.push(direccion);
    }
    if(this.persona.vehiculoPersonaList && this.persona.vehiculoPersonaList.length>0 && this.persona.vehiculoPersonaList[0].vehiculo){
      this.persona.vehiculoPersonaList[0].vehiculo.tipoVehiculo=this.tipoVehiculo;
      this.persona.vehiculoPersonaList[0].vehiculo.placa=this.placaVehiculo;
    }
    else{
      let vehiculo:Vehiculo=new Vehiculo();
      vehiculo.tipoVehiculo=this.tipoVehiculo;
      vehiculo.placa=this.placaVehiculo;
      let vehiculoPersona:VehiculoPersona=new VehiculoPersona();
      vehiculoPersona.vehiculo=vehiculo;
      this.persona.vehiculoPersonaList=[];
      this.persona.vehiculoPersonaList.push(vehiculoPersona);
    }
    this.actualizarPersona(this.persona);
  }

  actualizarPersona(persona) {
    if(persona.nombresApellidos)
    {
      this._personaService.actualizar(persona).subscribe(
        (data)=> {
          if(data && data.status=="OK"){
            if(data.objeto){
              this.persona=data.objeto;
              this.mostrarMensaje("Usuario registrado satisfactoriamente");
              this.navCtrl.navigateForward("/favores")
            }
            else{
              this.mostrarMensaje("No se pudo actualizar los datos de la persona");
            }
          }
          else{
            this.mostrarMensaje(data.message);
          }
        },
        (error)=>{
          console.log(error);
          this.mostrarMensaje(error);
        }
      )
    }
    else{
      this.mostrarMensaje("Los nombres y apellidos son obligatorios");
    }
  }

  recuperarPersonaPorNombreUsuario(nombreUsuario){
    this._usuarioService.recuperarPorUsuario(nombreUsuario).subscribe(
      (data)=> {
        if(data && data.status=="OK"){
          if(data.objeto){
            this.persona=data.objeto;
            if(this.persona.direccionList && this.persona.direccionList.length>0 && this.persona.direccionList[0].ciudad){
             this.pais=this.persona.direccionList[0].ciudad.pais;
             this.ciudad=this.persona.direccionList[0].ciudad;
            }
            if(this.persona.vehiculoPersonaList && this.persona.vehiculoPersonaList.length>0 && this.persona.vehiculoPersonaList[0].vehiculo){
              this.tipoVehiculo=this.persona.vehiculoPersonaList[0].vehiculo.tipoVehiculo;
              this.placaVehiculo=this.persona.vehiculoPersonaList[0].vehiculo.placa;
            }
          }
          else{
            this.persona=new Persona();
          }
        }
        else{
          this.mostrarMensaje(data.message);
        }
      },
      (error)=>{
        console.log(error);
        this.mostrarMensaje(error);
      }
    )
  }

  recuperarPaises(){
    this._paisService.listarTodos().subscribe(
      (data)=> {
        if(data && data.status=="OK"){
            this.paises=data.objeto;
        }
        else{
          this.mostrarMensaje(data.message);
        }
      },
      (error)=>{
        console.log(error);
        this.mostrarMensaje(error);
      }
    )
  }

  recuperarNivelesEstudio(){
    this._nivelEstudiosService.listarTodos().subscribe(
      (data)=> {
        if(data && data.status=="OK"){
            this.nivelesEstudios=data.objeto;
        }
        else{
          this.mostrarMensaje(data.message);
        }
      },
      (error)=>{
        console.log(error);
        this.mostrarMensaje(error);
      }
    )
  }
  recuperarProfesiones(){
    this._profesionService.listarTodos().subscribe(
      (data)=> {
        if(data && data.status=="OK"){
            this.profesiones=data.objeto;
        }
        else{
          this.mostrarMensaje(data.message);
        }
      },
      (error)=>{
        console.log(error);
        this.mostrarMensaje(error);
      }
    )
  }
  recuperarTiposLicencia(){
    this._tipoLicenciaService.listarTodos().subscribe(
      (data)=> {
        if(data && data.status=="OK"){
            this.tiposLicencia=data.objeto;
        }
        else{
          this.mostrarMensaje(data.message);
        }
      },
      (error)=>{
        console.log(error);
        this.mostrarMensaje(error);
      }
    )
  }
  recuperarTiposVehiculo(){
    this._tipoVehiculoService.listarTodos().subscribe(
      (data)=> {
        if(data && data.status=="OK"){
            this.tiposVehiculos=data.objeto;
        }
        else{
          this.mostrarMensaje(data.message);
        }
      },
      (error)=>{
        console.log(error);
        this.mostrarMensaje(error);
      }
    )
  }
  recuperarOcupaciones(){
    this._ocupacionService.listarTodos().subscribe(
      (data)=> {
        if(data && data.status=="OK"){
            this.ocupaciones=data.objeto;
        }
        else{
          this.mostrarMensaje(data.message);
        }
      },
      (error)=>{
        console.log(error);
        this.mostrarMensaje(error);
      }
    )
  }
  recuperarCiudadesPorIdPais(idPais){
    this._ciudadService.listarPorIdPais(this.pais.id).subscribe(
      (data)=> {
        if(data && data.status=="OK"){
            this.ciudades=data.objeto;
        }
        else{
          this.mostrarMensaje(data.message);
        }
      },
      (error)=>{
        console.log(error);
        this.mostrarMensaje(error);
      }
    )
  }

  recuperarNacionalidadPorIdPais(idPais){
    this._nacionalidadService.recuperarPorIdPais(idPais).subscribe(
      (data)=> {
        if(data && data.status=="OK"){
            this.persona.nacionalidad=data.objeto;
        }
        else{
          this.mostrarMensaje(data.message);
        }
      },
      (error)=>{
        console.log(error);
        this.mostrarMensaje(error);
      }
    )
  }

  compareWithFn = (o1, o2) => {
    return o1 && o2 ? o1.nombre === o2.nombre : o1 === o2;
  };

  compareWith = this.compareWithFn;

  async mostrarMensaje(mensaje) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000
    });
    toast.present();
  }
}

