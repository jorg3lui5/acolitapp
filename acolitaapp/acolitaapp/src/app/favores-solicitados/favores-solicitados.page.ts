import { Component, OnInit } from '@angular/core';
import { Constantes } from '../compartido/constantes';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-favores-solicitados',
  templateUrl: './favores-solicitados.page.html',
  styleUrls: ['./favores-solicitados.page.scss'],
})
export class FavoresSolicitadosPage implements OnInit {

  item: any;
    constantes: Constantes = new Constantes;

  constructor(
    public navCtrl: NavController,
  ) 
  { 

  }

  ngOnInit() {
  }

  unread(item){

  }

  nuevaSolicitud(){
    this.navCtrl.navigateForward("/solicitud");
  }
}
