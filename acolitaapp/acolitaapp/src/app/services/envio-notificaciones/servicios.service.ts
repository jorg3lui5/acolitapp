import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { HTTP } from '@ionic-native/http/ngx';
import { RespuestaDTO } from 'src/app/modelo/dto/RespuestaDTO';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { Persona } from 'src/app/modelo/persona';
import { Usuario } from 'src/app/modelo/usuario';


@Injectable({
  providedIn: 'root'
})
export class ServiciosService {
  
  urlBase: string =environment.urlBase;
  
  constructor(private http: HttpClient) {

  }

  // loguear(usuario,contrasenia){
  //   this.http.get(this.urlBase+'usuario/loguear/'+usuario+"/"+contrasenia)
  //   .subscribe(
  //     (data:RespuestaDTO)=> {
  //       if(data && data.status=="OK"){
  //         return data.message;
  //       }
  //       else{
  //         return data.message;
  //       }
  //       //this.route.navigate(['/favores']);
  //     },
  //     (error)=>{
  //       console.log(error);
  //     }
  //   )
  // }

  loguear(usuario,contrasenia): Observable<RespuestaDTO> {
    let apiURL = this.urlBase+'usuario/loguear/'+usuario+"/"+contrasenia;
    return this.http.get<RespuestaDTO>(apiURL)
  }

  crearPersonaUsuario(persona:Persona): Observable<RespuestaDTO> {
    let apiURL = this.urlBase+'persona/crear/';
    return this.http.post<RespuestaDTO>(apiURL,persona)
  }

  crearUsuarioPersona(usuario:Usuario): Observable<RespuestaDTO> {
    let apiURL = this.urlBase+'usuario/crear/';
    return this.http.post<RespuestaDTO>(apiURL,usuario)
  }
}
