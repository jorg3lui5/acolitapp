import { TestBed } from '@angular/core/testing';

import { EnvioNotificacionesService } from './envio-notificaciones.service';

describe('EnvioNotificacionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnvioNotificacionesService = TestBed.get(EnvioNotificacionesService);
    expect(service).toBeTruthy();
  });
});
