import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FavorRecibidoPageRoutingModule } from './favor-recibido-routing.module';

import { FavorRecibidoPage } from './favor-recibido.page';
import { CabeceraPageModule } from '../cabecera/cabecera.module'
import { PiePaginaPageModule } from '../pie-pagina/pie-pagina.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FavorRecibidoPageRoutingModule,
    CabeceraPageModule,
    PiePaginaPageModule
  ],
  declarations: [
    FavorRecibidoPage
  ],
  exports: [
    FavorRecibidoPage
  ],
})
export class FavorRecibidoPageModule {}
